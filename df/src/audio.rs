// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use serde::Deserialize;
use serde_json as json;

use hmi2mid::Hmi2mid;

pub struct Audio {
  // wav sfx files
  pub sound_fire           : Vec <u8>,
  pub sound_gui_tick       : Vec <u8>,
  pub sound_footstep_soft1 : Vec <u8>,
  pub sound_footstep_soft2 : Vec <u8>,
  pub sound_corpse_fall    : Vec <u8>,
  pub sound_swing_med      : Vec <u8>,
  pub sound_hit_melee1     : Vec <u8>,
  pub sound_hit_melee2     : Vec <u8>,
  pub sound_hit_melee3     : Vec <u8>,
  // midi song files
  pub song_title_menu      : Vec <u8>,
  pub song_deathmatch      : Vec <u8>
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
pub struct SoundHeader {
  pub wave_bytes_size : u32
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
pub struct SongHeader {
  pub hmi_bytes_size : u32
}

impl Audio {
  pub fn load (df_stream : &mut &[u8]) -> Self {
    fn parse_wave (df_stream : &mut &[u8]) -> (SoundHeader, Vec <u8>) {
      let mut deserializer = json::Deserializer::from_slice (df_stream)
        .into_iter::<json::Value>();
      let header = json::from_value::<SoundHeader> (
        deserializer.next().unwrap().unwrap()).unwrap();
      *df_stream = &(*df_stream)[deserializer.byte_offset()..];
      let wave_bytes_size = header.wave_bytes_size as usize;
      let wave_file = df_stream[0..wave_bytes_size].iter().cloned().collect();
      *df_stream = &(*df_stream)[wave_bytes_size..];
      (header, wave_file)
    }

    fn parse_hmi (df_stream : &mut &[u8]) -> (SongHeader, Vec <u8>) {
      let mut deserializer = json::Deserializer::from_slice (df_stream)
        .into_iter::<json::Value>();
      let header = json::from_value::<SongHeader> (
        deserializer.next().unwrap().unwrap()).unwrap();
      *df_stream = &(*df_stream)[deserializer.byte_offset()..];
      let hmi_bytes_size = header.hmi_bytes_size as usize;
      let hmi_file = df_stream[0..hmi_bytes_size].iter().cloned().collect();
      *df_stream = &(*df_stream)[hmi_bytes_size..];
      (header, hmi_file)
    }

    //
    // DAGGER.SND: wav sfx
    //
    // fire: record 420 ("242")
    let (_, sound_fire) = parse_wave (df_stream);
    // tick: record 360 ("203")
    let (_, sound_gui_tick) = parse_wave (df_stream);
    // footstep soft 1: record 331 ("75")
    let (_, sound_footstep_soft1) = parse_wave (df_stream);
    // footstep soft 2: record 310 ("54")
    let (_, sound_footstep_soft2) = parse_wave (df_stream);
    // corpse fall: record 15 ("17")
    let (_, sound_corpse_fall) = parse_wave (df_stream);
    // swing med: record 347 ("91")
    let (_, sound_swing_med) = parse_wave (df_stream);
    // hit melee 1: record 352 ("96")
    let (_, sound_hit_melee1) = parse_wave (df_stream);
    // hit melee 2: record 111 ("381")
    let (_, sound_hit_melee2) = parse_wave (df_stream);
    // hit melee 3: record 108 ("378")
    let (_, sound_hit_melee3) = parse_wave (df_stream);

    //
    // MIDI.BSA: midi songs
    //
    let mut hmi2mid = Hmi2mid::new();
    // title music: "GDUNGN4.HMI"
    let (_, song_title_menu_hmi) = parse_hmi (df_stream);
    let song_title_menu = hmi2mid.hmi_rip (song_title_menu_hmi).unwrap();
    // pvp music: "30.HMI"
    let (_, song_deathmatch_hmi) = parse_hmi (df_stream);
    let song_deathmatch = hmi2mid.hmi_rip (song_deathmatch_hmi).unwrap();

    Audio {
      sound_fire,
      sound_gui_tick,
      sound_footstep_soft1,
      sound_footstep_soft2,
      sound_corpse_fall,
      sound_swing_med,
      sound_hit_melee1,
      sound_hit_melee2,
      sound_hit_melee3,
      song_title_menu,
      song_deathmatch
    }
  }
}
