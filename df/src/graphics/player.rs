// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use lazy_static::lazy_static;

use super::parse_image;
use super::BYTES_PER_PIXEL as BPP;

const FRAME_SIZE       : usize = 192;
const ATLAS_WIDTH      : usize = 5*FRAME_SIZE;    // 5 angles
const ATLAS_HEIGHT     : usize = 12*FRAME_SIZE;   // 12 frames
const ATLAS_BYTES_SIZE : usize = BPP*ATLAS_WIDTH*ATLAS_HEIGHT;

lazy_static!{
  pub static ref OFFSETS_MAGE_THIEF_F : Vec <(i16, i16)> = vec![
    // 0..20: walk cycle
    (3, 11),
    (-4, 12),
    (-3, 13),
    (4, 13),
    (3, 13),
    // 20..50: attack frames
    (-10, 32),
    (-5, 33),
    (-9, 32),
    (-8, 31),
    (-3, 31),
    // 50..55: hit frame
    (2, 17),
    (-4, 18),
    (-7, 16),
    (-6, 15),
    (-5, 15),
    // 55..60: standing frame
    (2, 10),
    (-4, 11),
    (-5, 11),
    (6, 10),
    (6, 10),
  ];
  pub static ref OFFSETS_FIGHTER_LIGHT_M : Vec <(i16, i16)> = vec![
    // 0..20: walk cycle
    (-4, 11),
    (-13, 12),
    (-10, 15),
    (4, 15),
    (2, 15),
    // 20..50: attack frames
    (-23, 19),
    (-17, 20),
    (-18, 22),
    (-8, 24),
    (-4, 25),
    // 50..55: hit frame
    (-6, 16),
    (-11, 16),
    (-11, 15),
    (-12, 16),
    (-7, 17),
    // 55..60: standing frame
    (-2, -3),
    (-9, -2),
    (-16, -1),
    (-5, -2),
    (6, -1),
  ];
}

/// Read frames into a player sprite sheet ("atlas")
pub fn read_frames (
  df_stream : &mut &[u8], offsets : &[(i16, i16)], frame_count : usize
) -> ((u32, u32), Vec <u8>) {
  const SIZE : usize = FRAME_SIZE;

  let mut buffer = Vec::with_capacity (ATLAS_BYTES_SIZE);
  buffer.resize (ATLAS_BYTES_SIZE, 0);

  for i in 0..frame_count {
    let (header, data) = parse_image (df_stream);

    let width    = header.width as usize;
    let height   = header.height as usize;
    let offset_x = header.offset_x as i16;
    let offset_y = header.offset_y as i16;

    if i < 20 {
      // walk cycle frames: rows 1-4
      let row    = 1 + i % 4;
      let column = i / 4;
      let (pad_right, pad_top) = (
        (offset_x / 2 + offsets[0 + column].0) as usize,
        (offset_y / 2 + offsets[0 + column].1) as usize
      );
      let base_byte = ATLAS_BYTES_SIZE - BPP*ATLAS_WIDTH*(1 + row)*SIZE;
      for j in 0..height {
        let row_byte = base_byte + BPP*ATLAS_WIDTH*(j+pad_top);
        let byte     = row_byte + BPP*(pad_right + column*SIZE);
        buffer[byte..byte+BPP*width]
          .copy_from_slice(&data[BPP*j*width..BPP*(j+1)*width]);
      }

    } else if i < 50 {
      // attack frames: rows 5-10
      let row    = 5 + (i-20) % 6;
      let column = (i-20) / 6;
      let (pad_right, pad_top) = (
        (offset_x / 2 + offsets[5 + column].0) as usize,
        (offset_y / 2 + offsets[5 + column].1) as usize
      );
      let base_byte = ATLAS_BYTES_SIZE - BPP*ATLAS_WIDTH*(1 + row)*SIZE;
      for j in 0..height {
        let row_byte = base_byte + BPP*ATLAS_WIDTH*(j+pad_top);
        let byte     = row_byte + BPP*(pad_right + column*SIZE);
        buffer[byte..byte+BPP*width]
          .copy_from_slice(&data[BPP*j*width..BPP*(j+1)*width]);
      }

    } else if i < 55 {
      // hit frame: row 11
      let row    = 11;
      let column = i-50;
      let (pad_right, pad_top) = (
        (offset_x / 2 + offsets[10 + column].0) as usize,
        (offset_y / 2 + offsets[10 + column].1) as usize
      );
      let base_byte = ATLAS_BYTES_SIZE - BPP*ATLAS_WIDTH*(1 + row)*SIZE;
      for j in 0..height {
        let row_byte = base_byte + BPP*ATLAS_WIDTH*(j+pad_top);
        let byte     = row_byte + BPP*(pad_right + column*SIZE);
        buffer[byte..byte+BPP*width]
          .copy_from_slice(&data[BPP*j*width..BPP*(j+1)*width]);
      }

    } else if i < 60 {
      // standing frame: row 0
      let row    = 0;
      let column = i-55;
      let (pad_right, pad_top) = (
        (offset_x / 2 + offsets[15 + column].0) as usize,
        (offset_y / 2 + offsets[15 + column].1) as usize
      );
      let base_byte = ATLAS_BYTES_SIZE - BPP*ATLAS_WIDTH*(1 + row)*SIZE;
      for j in 0..height {
        let row_byte = base_byte + BPP*ATLAS_WIDTH*(j+pad_top);
        let byte     = row_byte + BPP*(pad_right + column*SIZE);
        buffer[byte..byte+BPP*width]
          .copy_from_slice(&data[BPP*j*width..BPP*(j+1)*width]);
      }
    }
  }
  ( (ATLAS_WIDTH as u32, ATLAS_HEIGHT as u32), buffer )
}
