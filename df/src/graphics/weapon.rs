// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use super::parse_image;
use super::BYTES_PER_PIXEL as BPP;

const FRAME_WIDTH      : usize = 320;
const FRAME_HEIGHT     : usize = 200;
const ATLAS_WIDTH      : usize = 5*FRAME_WIDTH;    // 5 frames
const ATLAS_HEIGHT     : usize = 7*FRAME_HEIGHT;   // 1 static + 6 animations
const ATLAS_BYTES_SIZE : usize = BPP*ATLAS_WIDTH*ATLAS_HEIGHT;

/// Read frames into a weapon graphic sheet ("atlas")
pub fn read_frames (df_stream : &mut &[u8]) -> ((u32, u32), Vec <u8>) {
  let mut buffer = Vec::with_capacity (ATLAS_BYTES_SIZE);
  buffer.resize (ATLAS_BYTES_SIZE, 0);

  { // frame 0: neutral position
    let (header, data) = parse_image (df_stream);
    let width    = header.width    as usize;
    let height   = header.height   as usize;
    let offset_x = header.offset_x as usize;
    //let offset_y = header.offset_y as usize;

    let base_byte = ATLAS_BYTES_SIZE - BPP*ATLAS_WIDTH*FRAME_HEIGHT;
    for j in 0..height {
      let row_byte = base_byte + j*BPP*ATLAS_WIDTH;
      let byte     = row_byte + BPP*offset_x;
      buffer[byte..byte+BPP*width]
        .copy_from_slice(&data[j*BPP*width..(j+1)*BPP*width]);
    }
  }

  for i in 0..30 {
    let (header, data) = parse_image (df_stream);
    let width    = header.width    as usize;
    let height   = header.height   as usize;
    let offset_x = header.offset_x as usize
      + (FRAME_WIDTH - width - header.offset_x as usize);
    //let offset_y = header.offset_y as usize;

    let row    = 1 + i / 5;
    let column = i % 5;

    // correct offset of row 5
    let offset_x = if row == 5 {
      offset_x - 45
    } else {
      offset_x
    };

    let base_byte = ATLAS_BYTES_SIZE - BPP*ATLAS_WIDTH*(1 + row)*FRAME_HEIGHT;
    for j in 0..height {
      let row_byte = base_byte + j*BPP*ATLAS_WIDTH;
      let byte     = row_byte + BPP*(offset_x + column*FRAME_WIDTH);
      buffer[byte..byte+BPP*width]
        .copy_from_slice(&data[j*BPP*width..(j+1)*BPP*width]);
    }
  }

  // debug
  /*
  let mut b = buffer.clone();
  b.reverse();
  b.chunks_mut(4).for_each(<[_]>::reverse);
  image::save_buffer("WEAPON.png", &b,
    ATLAS_WIDTH as u32, ATLAS_HEIGHT as u32, image::ColorType::BGRA(8)
  ).unwrap();
  */

  ( (ATLAS_WIDTH as u32, ATLAS_HEIGHT as u32), buffer )
}
