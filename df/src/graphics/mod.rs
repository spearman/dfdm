// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use serde_json as json;
use serde::Deserialize;

mod player;
mod weapon;

pub (crate) const BYTES_PER_PIXEL : usize = 4;

/// Glium RawImage2d::from_raw_rgba takes a Vec<T> argument, so instead of
/// working with slices we just allocate a byte vec for each payload. The
/// alternative would be using `unsafe` to transmute the slice into a
/// `[(u8,u8,u8,u8)]` and using the `Texture2dDataSink::from_raw` method which
/// takes a `Cow<[(u8,u8,u8,u8)]>`.
pub struct Graphics {
  pub image_court                  : ((u32, u32), Vec <u8>),
  pub image_sky                    : ((u32, u32), Vec <u8>),
  /// Copy of sky with modified colors
  pub image_sky_sepia              : ((u32, u32), Vec <u8>),
  pub image_cursor_sword           : ((u32, u32), Vec <u8>),
  pub image_blood                  : ((u32, u32), Vec <u8>),
  pub image_corpse                 : ((u32, u32), Vec <u8>),
  pub image_weapon_sword           : ((u32, u32), Vec <u8>),
  pub image_player_mage_thief_f    : ((u32, u32), Vec <u8>),
  pub image_player_fighter_light_m : ((u32, u32), Vec <u8>)
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
pub struct ImageHeader {
  pub width    : u16,
  pub height   : u16,
  pub offset_x : u16,
  pub offset_y : u16
}

pub (crate) fn parse_image (df_stream : &mut &[u8]) -> (ImageHeader, Vec <u8>) {
  let mut deserializer = json::Deserializer::from_slice (df_stream)
    .into_iter::<json::Value>();
  let header = json::from_value::<ImageHeader> (
    deserializer.next().unwrap().unwrap()).unwrap();
  *df_stream = &(*df_stream)[deserializer.byte_offset()..];
  let image_size_bytes = header.image_size_bytes();
  let pixel_data = df_stream[0..image_size_bytes].iter().cloned().collect();
  *df_stream = &(*df_stream)[image_size_bytes..];
  (header, pixel_data)
}

impl Graphics {
  pub fn load (df_stream : &mut &[u8]) -> Self {
    // court: CORT01I0.IMG
    let image_court = {
      let (header, data) = parse_image (df_stream);
      (header.image_dimensions(), data)
    };

    // sky: DANK02I0.IMG
    let (image_sky, image_sky_sepia) = {
      let (mut header, mut pixel_data) = parse_image (df_stream);
      // image is taller than 200 pixels: skip rows on top and bottom
      debug_assert_eq!(header.height, 215);
      header.height = 200;
      let width = header.width as usize;
      let pixel_data : Vec <u8> = {
        // 7 rows from top
        let start = 7 * width * BYTES_PER_PIXEL;
        // 8 rows from bottom
        let end = pixel_data.len() - 8 * width as usize * BYTES_PER_PIXEL;
        pixel_data.drain(start..end).collect()
      };
      // process image with sepia tone
      let mut pixel_data_sepia = pixel_data.clone();
      pixel_data_sepia.as_mut_slice().chunks_mut (4).for_each (
        |pixel| {
          pixel[1] = (pixel[1] as f64 * 0.589) as u8;
          pixel[2] = (pixel[2] as f64 * 0.179) as u8;
        }
      );
      let dimensions = header.image_dimensions();
      ( (dimensions, pixel_data), (dimensions, pixel_data_sepia) )
    };

    // sword cursor: CUST08I0.IMG
    let image_cursor_sword = {
      let (header, mut data) = parse_image (df_stream);
      // flip horizontally
      const BPP : usize = BYTES_PER_PIXEL;
      let width  = header.width  as usize;
      data.chunks_mut (BPP*width).for_each (<[_]>::reverse);
      data.chunks_mut (BPP).for_each (<[_]>::reverse);
      (header.image_dimensions(), data)
    };

    // blood: TEXTURE.380 record 0
    let image_blood = {
      const BLOOD_FRAME_COUNT : usize = 5;
      const ROW_BYTE_SIZE     : usize = 64 * BYTES_PER_PIXEL;
      const FRAME_BYTE_SIZE   : usize = 64 * ROW_BYTE_SIZE;
      // frame[0]
      let (mut header, mut pixel_data) = parse_image (df_stream);
      // image is shorter than 64 pixels: add space at top and bottom
      debug_assert_eq!(header.height, 57);
      debug_assert_eq!(header.width, 64);
      let mut pixel_data = {
        let mut v = Vec::with_capacity (FRAME_BYTE_SIZE * BLOOD_FRAME_COUNT);
        // 4 rows on top
        v.resize_with (4 * ROW_BYTE_SIZE, Default::default);
        v.append (&mut pixel_data);
        // 3 rows on bottom
        v.resize_with (v.len() + 3 * ROW_BYTE_SIZE, Default::default);
        debug_assert_eq!(v.len(), FRAME_BYTE_SIZE);
        v
      };
      // frames[1..5]
      for _ in 1..5 {
        let (header_frame, mut frame_data) = parse_image (df_stream);
        debug_assert_eq!(header, header_frame);
        pixel_data.resize_with (pixel_data.len() + 4 * ROW_BYTE_SIZE,
          Default::default);
        pixel_data.append(&mut frame_data);
        pixel_data.resize_with (pixel_data.len() + 3 * ROW_BYTE_SIZE,
          Default::default);
      }
      debug_assert_eq!(pixel_data.len(), BLOOD_FRAME_COUNT * FRAME_BYTE_SIZE);
      header.height = BLOOD_FRAME_COUNT as u16 * 64;
      (header.image_dimensions(), pixel_data)
    };

    // corpse: TEXTURE.380 record 1
    let image_corpse = {
      let (header, data) = parse_image (df_stream);
      (header.image_dimensions(), data)
    };

    // corpse: WEAPON04.CIF
    let image_weapon_sword = weapon::read_frames (df_stream);

    // player mage-thief (F): TEXTURE.489
    // 100 frames
    let image_player_mage_thief_f =
      player::read_frames (df_stream, &player::OFFSETS_MAGE_THIEF_F, 100);

    // player fighter-light (M): TEXTURE.482
    // 80 frames
    let image_player_fighter_light_m =
      player::read_frames (df_stream, &player::OFFSETS_FIGHTER_LIGHT_M, 80);

    Graphics {
      image_court,
      image_sky,
      image_sky_sepia,
      image_cursor_sword,
      image_blood,
      image_corpse,
      image_weapon_sword,
      image_player_mage_thief_f,
      image_player_fighter_light_m
    }
  }
}

impl ImageHeader {
  /// Return image (width,height) as a pair of `u32` (this is the type used by
  /// glium image creation functions).
  pub fn image_dimensions (&self) -> (u32, u32) {
    ( self.width  as u32, self.height as u32 )
  }
  /// Total byte size of this image's pixel data.
  pub fn image_size_bytes (&self) -> usize {
    self.width as usize * self.height as usize * BYTES_PER_PIXEL
  }
}
