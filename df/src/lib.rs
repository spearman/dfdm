// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

extern crate lazy_static;
extern crate log;
extern crate serde;
extern crate serde_json;

extern crate hmi2mid;

use log::{info, error};

pub mod graphics;
pub mod audio;

pub use self::graphics::Graphics;
pub use self::audio::Audio;

/// Processes the byte stream generated by the DaggerFile dotnet application.
///
/// DaggerFile validates the specified ARENA2 installation folder and loads
/// textures and audio in a predefined order:
///
/// - TODO
pub struct Resources {
  pub graphics : Graphics,
  pub audio    : Audio
}

impl Resources {
  pub fn load (maybe_arena2_path : Option <std::path::PathBuf>) -> Self {
    let output = {
      let daggerfile_exe_path = {
        let paths = [
          std::path::PathBuf::from ("./DaggerFile/DaggerFile"),
          std::path::PathBuf::from ("./DaggerFile/DaggerFile.exe")
        ];
        paths.iter().find (|p| p.exists()).unwrap_or_else (
          || {
            error!("Could not find DaggerFile executable");
            panic!("dfdm client: fatal error")
        }).canonicalize().unwrap()
      };
      let mut command = std::process::Command::new (daggerfile_exe_path);
      command.current_dir("./DaggerFile/");
      if let Some (arena2_path) = maybe_arena2_path {
        command.arg (arena2_path);
      }
      info!("Running {:?}...", command);
      command.output().expect ("Running DaggerFile failed")
    };
    if !output.status.success() {
      error!("DaggerFile exited with {:?}", output.status);
      eprintln!("DaggerFile stderr: {}",
        std::str::from_utf8 (&output.stderr).unwrap());
      std::process::exit(-1);
    }
    let stdout    = output.stdout;
    let mut bytes = stdout.as_slice();
    let graphics  = Graphics::load (&mut bytes);
    let audio     = Audio::load    (&mut bytes);
    info!("Finished loading DaggerFile resources");
    Resources { graphics, audio }
  }
}
