# Daggerfall Death Match

> DFDM: a Daggerfall-themed deathmatch prototype

Download pre-built binaries (v0.0.4):

- [Windows (x64)](https://gitlab.com/spearman/dfdm-dist/blob/master/dfdm-v0.0.4_win-x64.zip)
- [Linux (x64)](https://gitlab.com/spearman/dfdm-dist/blob/master/dfdm-v0.0.4_linux-x64.tar.gz)

This is an experimental deathmatch game created to learn multiplayer network
programming and to test a multi-threaded client architecture written in Rust.

As this is a very early prototype, it is not a complete game. Currently
implemented:

- Two player sprite sheets, one weapon (sword), and a number of sfx and MIDI
  music loaded at runtime from an existing Daggerfall installation (see below).
- Practice mode: fight some bots with some very simple random walking &
  attacking sequences.
- Multiplayer mode: connect to a server and fight other players.

Video:

[![YouTube thumbnail](http://img.youtube.com/vi/5smhDRGDkXM/0.jpg)](http://www.youtube.com/watch?v=5smhDRGDkXM "DFDM client")

There are known bugs and unfinished portions of code that will cause crashes, so
don't expect it to be stable. See [Limitations and bugs](#limitations-and-bugs)
below. As of this release (2019-03-19) further development is on hold.

Running a pre-built distribution requires a full installation of Daggerfall
which can be downloaded for free
[here](https://elderscrolls.bethesda.net/en/daggerfall) and installed using
DOSBox by following the included instructions.
The default configuration looks for the ARENA2 directory in the relative path
`../Daggerfall/ARENA2`. This can be changed by editing the
`./data/client-config.yaml` file or providing the command line option
`-p <ARENA2_PATH>`.

Loading Daggerfall resources at runtime is provided by the companion
[DaggerFile](https://gitlab.com/spearman/DaggerFile) program, which
utilizes code from the
[Daggerfall Unity](https://github.com/interkarma/daggerfall-unity) project.


## Building

**Build and run client**

Requires the DaggerFile program in local path `./DaggerFile/DaggerFile`.

Debug:

```
$ cargo run --bin client
```

Release:

```
$ cargo run --release --bin client
```

When running source builds with `cargo run`, command-line arguments can be
supplied following `--`:

```
$ cargo run --bin client -- -p "/path/to/ARENA2"
```

Dotfiles showing some aspects of the program architecture can be output by
defining the `DFDM_DOTFILES` environment variable:

```
$ DFDM_DOTFILES=1 cargo run --bin client
```

and graphs can be generated using the included makefile (requires Graphviz):

```
$ make -f MakefileDot client
$ make -f MakefileDot splash
$ make -f MakefileDot title_menu
$ make -f MakefileDot practice
$ make -f MakefileDot multi_setup
$ make -f MakefileDot multi_run
```

Example output can be seen [here](https://spearman.github.io/client/client.svg).

The client application relies on the following paths being present:

- `./data/` -- data and configuration directory
- `./DaggerFile/` -- Daggerfall resource loading application; this can be built
  as a self-contained application from the source
  [repository](https://gitlab.com/spearman/DaggerFile) for a target dotnet
  [runtime](https://docs.microsoft.com/en-us/dotnet/core/rid-catalog) (e.g.
  `linux-x64`):
  ```
  $ dotnet publish -c Release -r linux-x64
  ```
  and then copying the contents of the output
  `/path/to/repo/bin/Release/dotnetapp2.2/linux-x64/publish/` to
  `./DaggerFile/`

Required shared libraries can be found in the `./dist/` subdirectories for each
release target.


**Build and run server**

Debug:

```
$ cargo run --bin server
```

Release:

```
$ cargo run --release --bin server
```

A dotfile showing the server program state machine can be output by defining the
`DFDM_DOTFILES` environment variable:

```
$ DFDM_DOTFILES=1 cargo run --bin server
```


**Publish**

Copy the appropriate release binaries into a directory containing the shared
libraries in `./dist/<platform>/`, and the `./data/` and `DaggerFile`
directories (see above for building `DaggerFile`).


## Options

**Client**

the following command-line options will override the configuration in
`./data/client-config.yaml`:

`-l <LOG_LEVEL>` -- one of `Trace`, `Debug`, `Info`, `Warn`, `Error`

`-p <ARENA2_PATH>` -- a path to the `ARENA2` installation directory


**Server**

the following command-line options will override the configuration in
`./data/server-config.yaml`:

`-l <LOG_LEVEL>` -- one of `Trace`, `Debug`, `Info`, `Warn`, `Error`

`-p <PORT>` -- UDP port number

`-h <HOSTNAME>` -- server address, e.g. `192.168.0.2` (LAN), or external IP
  address for internet games


## Features

- Multithreaded client architecture with decoupled rendering, simulation, and
  networking
- Linear physics sim with continuous collision detection
- Real-time combat with weapon arcs based on melee attack type
- Multiplayer server support for up to 16 players
- Renderer utilizing Opengl 3.3 core profile


## Limitations and bugs

- The settings cannot yet be edited in game. Client settings can be changed
  in `./data/client-config.yaml`.
- There is currently no level geometry or lighting.
- The network code does very basic synchronization on user commands and
  currently makes no attempt to detect desynchs or handle ping fluctuations or
  dropouts.
- The physics simulation is not very mature and does not support stable stacking
  of more than 2 objects. This can cause the resolver to enter into an infinite
  loop. In debug builds there may also be crashes if certain assumptions are
  violated.
- There are bugs with FMOD when closing the program. On Linux sometimes a
  'Channel stolen' error will be seen, and on Windows the program always
  terminates with a segfault, even on normal exit.
- While there are two player sprites, there is no class selection for
  multiplayer mode, and all players will be the 'male light fighter' skin with
  longsword.
- There is no scoreboard or in-game chat.


## License

GPL 3.0
