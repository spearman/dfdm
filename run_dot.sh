#!/bin/bash

set -e

while getopts "v" opt; do
  case $opt in
    v)
      set -x
      ;;
    *)
      echo "Valid options:"
      echo "  -v    verbose"
      exit 1
      ;;
  esac
done

DFDM_DOTFILES=1 cargo run --bin client
make -f MakefileDot client
make -f MakefileDot splash
make -f MakefileDot title_menu
make -f MakefileDot practice
make -f MakefileDot multi_setup
make -f MakefileDot multi_run

exit
