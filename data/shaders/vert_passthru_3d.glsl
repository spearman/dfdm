/*
Simple "no-op" 3D vertex shader.

It is acceptable to input vertices with extra attributes, but it is a runtime
error if the `in_position` attribute is not a `vec3`.
*/

#version 330

/*  Built-in vertex shader inputs:
in int gl_VertexId;
in int gl_InstanceId;
*/

// inputs
in vec3 in_position;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

void main () {
  gl_Position = vec4 (in_position, 1.0);
}
