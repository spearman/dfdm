
/*
Passes through position and `var_color`
*/

#version 330

/*  Built-in vertex shader inputs:
in int gl_VertexId;
in int gl_InstanceId;
*/

// inputs
in vec2 in_position;
in vec4 in_color;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

// outputs
/*layout (location = 0)*/ out vec4 var_color;

void main () {
  gl_Position = vec4 (in_position, 0.0, 1.0);
  var_color   = in_color;
}
