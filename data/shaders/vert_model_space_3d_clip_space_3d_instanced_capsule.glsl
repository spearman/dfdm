/*
Draw a Z-axis aligned capsule inscribed inside an AABB defined by position and
scale.
*/

#version 330

// uniforms
uniform mat4 uni_projection_mat_perspective;
uniform mat4 uni_transform_mat_view;

/*  Built-in vertex shader inputs:
in int gl_VertexID;
in int gl_InstanceID;
*/

// inputs
// per instance
in vec3 in_position;
in vec3 in_scale;
// per vertex
in vec3 inst_position;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

// outputs
/* none */

const int VERTICES_PER_HEMISPHERE = 73;

mat3 mat_scale = mat3 (
  vec3 (in_scale.x, 0.0, 0.0),
  vec3 (0.0, in_scale.y, 0.0),  // should be equal to x for spherical caps
  vec3 (0.0, 0.0, in_scale.x)); // always uses x for the radius

// main
void main () {
  // the total capsule height should be equal to 2.0 * in_scale.z
  float height              = 2.0 * in_scale.z;
  float radius              = in_scale.x;
  float cylinder_length     = height - 2.0 * radius;
  vec3 hemisphere_translate = vec3 (0.0, 0.0, 0.5 * cylinder_length);
  if (VERTICES_PER_HEMISPHERE <= gl_VertexID) {
    hemisphere_translate *= -1.0;
  }
  vec3 hemisphere_position  = (mat_scale * inst_position) + hemisphere_translate;

  // write outputs
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * vec4 (in_position + hemisphere_position, 1.0);
}
