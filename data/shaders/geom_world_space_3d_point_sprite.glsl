/*
Draws an animated point sprite in world space.
*/

#version 330

///////////////////////////////////////////////////////////////////////////////
//  uniforms                                                                 //
///////////////////////////////////////////////////////////////////////////////
uniform mat4 uni_projection_mat_perspective;
uniform mat4 uni_transform_mat_view;
uniform sampler2D uni_sampler_2d;

///////////////////////////////////////////////////////////////////////////////
//  inputs                                                                   //
///////////////////////////////////////////////////////////////////////////////
// input primitive
layout (points) in;

/*  Built-in geometry shader inputs:
in int gl_PrimitiveIDIn;
in int gl_InvocationID;   // GLSL 4.0+
in gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_in[];
*/

// inputs
/*none*/

///////////////////////////////////////////////////////////////////////////////
//  outputs                                                                  //
///////////////////////////////////////////////////////////////////////////////
/*  Built-in geometry shader outputs:
out int gl_PrimitiveID;
out int gl_Layer;
out int gl_ViewportIndex;
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
*/

// output primitive
layout (triangle_strip, max_vertices = 4) out;

// outputs
/*layout (location = 0)*/      out vec2 var_uv;

///////////////////////////////////////////////////////////////////////////////
//  constants                                                                //
///////////////////////////////////////////////////////////////////////////////

// TODO: if this pixel per unit scale is used in other shaders maybe it should
// be a uniform or configurable
const float pixels_per_unit = 56.0; // 56 pixels per 1m
// texture dimensions (texels)
/*const*/ ivec2 texture_dims = textureSize (uni_sampler_2d, 0); // lod 0
// billboard size from center to edges in each dimension
/*const*/ float size_x = texture_dims.x / (2.0 * pixels_per_unit);
/*const*/ float size_y = texture_dims.y / (2.0 * pixels_per_unit);
// billboard center in view space
/*const*/ vec3  center_view_space
  = vec3 (uni_transform_mat_view * gl_in[0].gl_Position);

///////////////////////////////////////////////////////////////////////////////
//  main                                                                     //
///////////////////////////////////////////////////////////////////////////////

void main () {
  // lower left
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space + vec3 (-size_x, -size_y, 0.0), 1.0);
  var_uv             = vec2 (0.0, 0.0);
  EmitVertex();
  // lower right
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space + vec3 (size_x, -size_y, 0.0), 1.0);
  var_uv             = vec2 (1.0, 0.0);
  EmitVertex();
  // upper left
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space + vec3 (-size_x, size_y, 0.0), 1.0);
  var_uv             = vec2 (0.0, 1.0);
  EmitVertex();
  // upper right
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space + vec3 (size_x, size_y, 0.0), 1.0);
  var_uv             = vec2 (1.0, 1.0);
  EmitVertex();
}
