/*
// TODO: Draws a tile.
*/

#version 330

// uniforms
uniform mat4  uni_projection_mat_ortho;
uniform int   uni_gui_scale;
uniform ivec2 uni_tile_dimensions;

// input primitive
layout (points) in;

/*  Built-in geometry shader inputs:
in int gl_PrimitiveIDIn;
in int gl_InvocationID;   // GLSL 4.0+
in gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_in[];
*/

// inputs
/*layout (location = 1)*/ in uint vert_tile[];

/*  Built-in geometry shader outputs:
out int gl_PrimitiveID;
out int gl_Layer;
out int gl_ViewportIndex;
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
*/

// output primitive
layout (triangle_strip, max_vertices = 4) out;

// outputs
/*layout (location = 0)*/ out vec2 var_uv;

// main
void main () {
  vec2 scaled_tile_dimensions = vec2 (uni_gui_scale * uni_tile_dimensions);

  int tile = int (vert_tile[0]);
  float uv_size = 1.0 / 16.0;
  vec2  uv_base = vec2 (
    uv_size * float (tile % 16),
    uv_size * float (15 - (tile / 16)));

  // lower-left corner
  var_uv      = uv_base;
  gl_Position = uni_projection_mat_ortho *
    (gl_in[0].gl_Position + vec4 (0.0, -scaled_tile_dimensions.y, 0.0, 0.0));
  EmitVertex();
  // upper-left corner
  var_uv      = uv_base + vec2 (0.0, uv_size);
  gl_Position = uni_projection_mat_ortho *
    gl_in[0].gl_Position;
  EmitVertex();
  // lower-right corner
  var_uv      = uv_base + vec2 (uv_size, 0.0);
  gl_Position = uni_projection_mat_ortho *
    (gl_in[0].gl_Position
      + vec4 (scaled_tile_dimensions.x, -scaled_tile_dimensions.y, 0.0, 0.0));
  EmitVertex();
  // upper-right corner
  var_uv      = uv_base + vec2 (uv_size, uv_size);
  gl_Position = uni_projection_mat_ortho *
    (gl_in[0].gl_Position + vec4 (scaled_tile_dimensions.x, 0.0, 0.0, 0.0));
  EmitVertex();
}
