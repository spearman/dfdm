/*
Draws an oriented, vertically aligned billboard sprite in world space.
*/

#version 330

////////////////////////////////////////////////////////////////////////////////
//  uniforms                                                                  //
////////////////////////////////////////////////////////////////////////////////
uniform mat4 uni_projection_mat_perspective;
uniform mat4 uni_transform_mat_view;
uniform sampler2DArray uni_sampler_2d_array;

////////////////////////////////////////////////////////////////////////////////
//  inputs                                                                    //
////////////////////////////////////////////////////////////////////////////////
// input primitive
layout (points) in;

/*  Built-in geometry shader inputs:
in int gl_PrimitiveIDIn;
in int gl_InvocationID;   // GLSL 4.0+
in gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_in[];
*/

// inputs
/*layout (location = 0)*/ in mat3 orientation[];
/*layout (location = 3)*/ in uint texture_index[];
/*layout (location = 4)*/ in uint frame[];

////////////////////////////////////////////////////////////////////////////////
//  outputs                                                                   //
////////////////////////////////////////////////////////////////////////////////
/*  Built-in geometry shader outputs:
out int gl_PrimitiveID;
out int gl_Layer;
out int gl_ViewportIndex;
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
*/

// output primitive
layout (triangle_strip, max_vertices = 4) out;

// outputs
/*layout (location = 0)*/      out vec2 var_uv;
/*layout (location = 1)*/ flat out uint flat_texture_index;

///////////////////////////////////////////////////////////////////////////////
//  functions                                                                //
///////////////////////////////////////////////////////////////////////////////

const uint VIEW_FRONT       = 0u;
const uint VIEW_FRONT_RIGHT = 1u;
const uint VIEW_RIGHT       = 2u;
const uint VIEW_BACK_RIGHT  = 3u;
const uint VIEW_BACK        = 4u;
const uint VIEW_BACK_LEFT   = 5u;
const uint VIEW_LEFT        = 6u;
const uint VIEW_FRONT_LEFT  = 7u;

uint compute_view_angle (
  const vec3 center_view_space, const mat3 object_orient_view_space
) {
  // vector from billboard center to view position (origin in view space)
  const float FRAC_PI_2 = 1.57079632679489661923132169163975144;
  const float FRAC_PI_8 = 0.39269908169872415480783042290993786;
  const vec3 view_vec   = (-center_view_space);
  /*const*/ float x = dot (view_vec, object_orient_view_space[1]); // X basis vector
  /*const*/ float y = dot (view_vec, object_orient_view_space[0]); // Y basis vector
  float view_angle;
  if (x == 0.0) {
    if (y == 0.0) {
      view_angle = 0.0;
    } else if (0.0 < y) {
      view_angle = FRAC_PI_2;
    } else {  // assert (y < 0.0)
      view_angle = -FRAC_PI_2;
    }
  } else {  // assert (x != 0.0)
    view_angle = atan (y,x);
  }
  if        (-FRAC_PI_8     <= view_angle && view_angle < FRAC_PI_8) {
    return VIEW_FRONT;
  } else if (FRAC_PI_8      <= view_angle && view_angle < 3.0*FRAC_PI_8) {
    return VIEW_FRONT_RIGHT;
  } else if (3.0*FRAC_PI_8  <= view_angle && view_angle < 5.0*FRAC_PI_8) {
    return VIEW_RIGHT;
  } else if (5.0*FRAC_PI_8  <= view_angle && view_angle < 7.0*FRAC_PI_8) {
    return VIEW_BACK_RIGHT;
  } else if (7.0*FRAC_PI_8  <= view_angle || view_angle < -7.0*FRAC_PI_8) {
    return VIEW_BACK;
  } else if (-7.0*FRAC_PI_8 <= view_angle && view_angle < -5.0*FRAC_PI_8) {
    return VIEW_BACK_LEFT;
  } else if (-5.0*FRAC_PI_8 <= view_angle && view_angle < -3.0*FRAC_PI_8) {
    return VIEW_LEFT;
  } else if (-3.0*FRAC_PI_8 <= view_angle && view_angle < -FRAC_PI_8) {
    return VIEW_FRONT_LEFT;
  } else {
    return VIEW_FRONT;	// unreachable
  }
}

float compute_uv_x_base (const uint view_angle, const uint uv_columns) {
  const float uv_width = 1.0/float (uv_columns);
  switch (view_angle) {
    case VIEW_FRONT:        return 0.0;
    case VIEW_FRONT_RIGHT:  return 1.0 * uv_width; // needs flipping
    case VIEW_RIGHT:        return 2.0 * uv_width; // needs flipping
    case VIEW_BACK_RIGHT:   return 3.0 * uv_width; // needs flipping
    case VIEW_BACK:         return 4.0 * uv_width;
    case VIEW_BACK_LEFT:    return 3.0 * uv_width;
    case VIEW_LEFT:         return 2.0 * uv_width;
    case VIEW_FRONT_LEFT:   return 1.0 * uv_width;
    default:                return 0.0;  // unreachable
  }
}

// because the sprites have only left-facing views we need to flip the
// right-facing views to make them appear in the right direction
vec2 compute_uv_x_flip (const uint view_angle) {
  switch (view_angle) {
    case VIEW_FRONT_RIGHT:
    case VIEW_RIGHT:
    case VIEW_BACK_RIGHT: return vec2 (1.0, 0.0);
    default:              return vec2 (0.0, 1.0);
  }
}


///////////////////////////////////////////////////////////////////////////////
//  constants                                                                //
///////////////////////////////////////////////////////////////////////////////

// TODO: if this pixel per unit scale is used in other shaders maybe it should
// be a uniform or configurable
const float pixels_per_unit = 56.0; // 56 pixels per 1m
const uint  uv_columns      = 5u;  // 5 viewing angles for each pose
// row     0: neutral standing
// rows 1- 4: walk cycle
// rows 5-10: melee attack cycle
const uint  uv_rows         = 12u; // TODO: more animations
const float uv_width        = 1.0 / float (uv_columns);
const float uv_height       = 1.0 / float (uv_rows);
// texture dimensions (texels)
/*const*/ ivec3 texture_dims = textureSize (uni_sampler_2d_array, 0); // lod 0
// billboard size from center to edges in each dimension
/*const*/ float size_x = texture_dims.x / (float (uv_columns) * 2.0 * pixels_per_unit);
/*const*/ float size_y = texture_dims.y / (float (uv_rows) * 2.0 * pixels_per_unit);
// billboard is aligned to vertical world space axis (Z) so we calculate
// the top and bottom centers in world space and transform to view space
// billboard center in view space
/*const*/ vec4  center_top    =
  gl_in[0].gl_Position + vec4 (0.0, 0.0,  size_y, 0.0);
/*const*/ vec4  center_bottom =
  gl_in[0].gl_Position + vec4 (0.0, 0.0, -size_y, 0.0);

/*const*/ vec3  center_view_space_top
  = vec3 (uni_transform_mat_view * center_top);
/*const*/ vec3  center_view_space_bottom
  = vec3 (uni_transform_mat_view * center_bottom);
/*const*/ vec3  center_view_space
  = vec3 (uni_transform_mat_view * gl_in[0].gl_Position);
/*const*/ mat3  orient_view_space
  = mat3 (uni_transform_mat_view) * orientation[0];
// compute the viewing angle:
/*const*/ uint view_angle = compute_view_angle (center_view_space, orient_view_space);
/*const*/ float uv_x_base = compute_uv_x_base (view_angle, uv_columns);
/*const*/ vec2  uv_x_flip = compute_uv_x_flip  (view_angle);
/*const*/ float uv_y_base = 1.0 - float (frame[0]+1u) * uv_height;

////////////////////////////////////////////////////////////////////////////////
//  main                                                                      //
////////////////////////////////////////////////////////////////////////////////

void main () {
  // lower left
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space_bottom + vec3 (-size_x, 0.0, 0.0), 1.0);
  var_uv             = vec2 (uv_x_base + uv_x_flip[0]*uv_width, uv_y_base);
  flat_texture_index = texture_index[0];
  EmitVertex();
  // lower right
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space_bottom + vec3 (size_x, 0.0, 0.0), 1.0);
  var_uv             = vec2 (uv_x_base + uv_x_flip[1]*uv_width, uv_y_base);
  flat_texture_index = texture_index[0];
  EmitVertex();
  // upper left
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space_top + vec3 (-size_x, 0.0, 0.0), 1.0);
  var_uv             = vec2 (
    uv_x_base + uv_x_flip[0]*uv_width,
    uv_y_base + uv_height);
  flat_texture_index = texture_index[0];
  EmitVertex();
  // upper right
  gl_Position = uni_projection_mat_perspective
    * vec4 (center_view_space_top + vec3 (size_x, 0.0, 0.0), 1.0);
  var_uv             = vec2 (
    uv_x_base + uv_x_flip[1]*uv_width,
    uv_y_base + uv_height);
  flat_texture_index = texture_index[0];
  EmitVertex();
}
