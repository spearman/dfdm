/*
Draws X,Y,Z basis vectors at input world position.
*/

#version 330

// uniforms
uniform mat4 uni_projection_mat_perspective;
uniform mat4 uni_transform_mat_view;

// input primitive
layout (points) in;

/*  Built-in geometry shader inputs:
in int gl_PrimitiveIDIn;
in int gl_InvocationID;   // GLSL 4.0+
in gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_in[];
*/

// inputs
/*layout (location = 0)*/ in mat3 orientation[];

/*  Built-in geometry shader outputs:
out int gl_PrimitiveID;
out int gl_Layer;
out int gl_ViewportIndex;
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
*/

// output primitive
layout (line_strip, max_vertices = 6) out;

// outputs
/*layout (location = 0)*/ out vec4 var_color;

// constants
/*const*/ mat4 world_to_clip_mat
  = uni_projection_mat_perspective * uni_transform_mat_view;

// variables
vec4 color;
vec4 basis;

// main
void main () {
  // X axis -- red
  color = vec4 (1.0, 0.0, 0.0, 1.0);
  basis = vec4 (orientation[0][0], 0.0);
  var_color   = color;
  gl_Position = world_to_clip_mat * gl_in[0].gl_Position;
  EmitVertex();
  var_color   = color;
  gl_Position = world_to_clip_mat * (gl_in[0].gl_Position + basis);
  EmitVertex();
  EndPrimitive();
  // Y axis -- green
  color = vec4 (0.0, 1.0, 0.0, 1.0);
  basis = vec4 (orientation[0][1], 0.0);
  var_color   = color;
  gl_Position = world_to_clip_mat * gl_in[0].gl_Position;
  EmitVertex();
  var_color   = color;
  gl_Position = world_to_clip_mat * (gl_in[0].gl_Position + basis);
  EmitVertex();
  EndPrimitive();
  // Z axis -- blue
  color = vec4 (0.0, 0.0, 1.0, 1.0);
  basis = vec4 (orientation[0][2], 0.0);
  var_color   = color;
  gl_Position = world_to_clip_mat * gl_in[0].gl_Position;
  EmitVertex();
  var_color   = color;
  gl_Position = world_to_clip_mat * (gl_in[0].gl_Position + basis);
  EmitVertex();
  EndPrimitive();
}
