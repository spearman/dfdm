/*
3D sprites are defined by a center position and with an index into the sprite 2D
texture array, and a frame number.
*/

#version 330

/*  Built-in vertex shader inputs:
in int gl_VertexID;
in int gl_InstanceID;
*/

// inputs
in vec3 in_position;
in uint in_texture_index;
in uint in_frame;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

// outputs
/*layout (location = 0)*/ out uint texture_index;
/*layout (location = 1)*/ out uint frame;

// main
void main () {
  gl_Position   = vec4 (in_position, 1.0);
  texture_index = in_texture_index;
  frame         = in_frame;
}
