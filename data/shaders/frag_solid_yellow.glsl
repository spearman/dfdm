/*
Write solid yellow (1,1,0,1) to all fragments.
*/

#version 330

/*  Built-in fragment shader inputs:
in vec4   gl_FragCoord;
in bool   gl_FrontFacing;
in vec2   gl_PointCoord;
in float  gl_ClipDistance[];
in int    gl_PrimitiveID;
in int    gl_SampleID;        // GLSL 4.0+
in vec2   gl_SamplePosition;  // GLSL 4.0+
in int    gl_SampleMaskIn[];  // GLSL 4.0+
in int    gl_Layer;           // GLSL 4.3+
in int    gl_ViewportIndex;   // GLSL 4.3+
*/

/*  Built-in fragment shader outputs:
out float gl_FragDepth;
out int   gl_SampleMask[];
*/

// outputs
out vec4 out_color;

void main () {
  out_color = vec4 (1.0, 1.0, 0.0, 1.0);
}
