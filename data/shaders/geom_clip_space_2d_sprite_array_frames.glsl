/*
Draw a frame of an animated sprite with the given center position and
dimensions. The UV coordinates of the texture are selected based on the value
in the row and column uniforms, row and column coordinates for the input
vertex.  Also passes on a texture array index to select from the appropriate
texture.
*/

#version 330

// input primitive
layout (points) in;

// uniforms
uniform uint uni_row_count;
uniform uint uni_column_count;

/*  Built-in geometry shader inputs:
in int gl_PrimitiveIDIn;
in int gl_InvocationID;   // GLSL 4.0+
in gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_in[];
*/

// inputs
/*layout (location = 0)*/ in vec2 dimensions[];
/*layout (location = 1)*/ in uint texture_index[];
/*layout (location = 2)*/ in uint row[];
/*layout (location = 3)*/ in uint column[];

/*  Built-in geometry shader outputs:
out int gl_PrimitiveID;
out int gl_Layer;
out int gl_ViewportIndex;
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
*/

// output primitive
layout (triangle_strip, max_vertices = 4) out;

// outputs
/*layout (location = 0)*/      out vec2 var_uv;
/*layout (location = 1)*/ flat out uint flat_texture_index;

//
// constants
//
/*const*/ vec2 dimensions_div2   = dimensions[0] * 0.5;
// texture coordinates are 0,0 at lower-left and 1,1 at upper-right:
//
//    0,1  1,1
//    0,0  1,0
//
// we want the base point to be the lower-left
/*const*/ float uv_column_width = 1.0/float (uni_column_count);
/*const*/ float uv_row_height   = 1.0/float (uni_row_count);
/*const*/ vec2  uv_base         = vec2 (
  uv_column_width * column[0],
  uv_row_height * (uni_row_count - row[0] - 1u)
);

// main
void main () {
  // lower-left corner
  gl_Position = gl_in[0].gl_Position + vec4 (-dimensions_div2, 0.0, 0.0);
  var_uv      = vec2 (uv_base);
  flat_texture_index  = texture_index[0];
  EmitVertex();
  // upper-left corner
  gl_Position = gl_in[0].gl_Position
    + vec4 (-dimensions_div2.x, dimensions_div2.y, 0.0, 0.0);
  var_uv      = vec2 (uv_base + vec2 (0.0, uv_row_height));
  flat_texture_index  = texture_index[0];
  EmitVertex();
  // lower-right corner
  gl_Position = gl_in[0].gl_Position
    + vec4 (dimensions_div2.x, -dimensions_div2.y, 0.0, 0.0);
  var_uv      = vec2 (uv_base + vec2 (uv_column_width, 0.0));
  flat_texture_index  = texture_index[0];
  EmitVertex();
  // upper-right corner
  gl_Position = gl_in[0].gl_Position + vec4 (dimensions_div2, 0.0, 0.0);
  var_uv      = vec2 (uv_base + vec2 (uv_column_width, uv_row_height));
  flat_texture_index  = texture_index[0];
  EmitVertex();
}
