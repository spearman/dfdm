/*
2D sprites are defined by a center position and dimensions which are both
output unmodified. This shader also passes on a texture array index and a
colorize flag and color to eventually be used by the fragment shader.
*/

#version 330

/*  Built-in vertex shader inputs:
in int gl_VertexID;
in int gl_InstanceID;
*/

// inputs
in vec2 in_position;
in vec2 in_dimensions;
in uint in_texture_index;
in uint in_colorize_flag;
in vec3 in_colorize_color;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

// outputs
/*layout (location = 0)*/ out vec2 dimensions;
/*layout (location = 1)*/ out uint texture_index;
/*layout (location = 2)*/ out uint colorize_flag;
/*layout (location = 3)*/ out vec3 colorize_color;

// main
void main () {
  gl_Position    = vec4 (in_position, 0.0, 1.0);
  dimensions     = in_dimensions;
  texture_index  = in_texture_index;
  colorize_flag  = in_colorize_flag;
  colorize_color = in_colorize_color;
}
