/*
Sample from a 2D texture according to input UV coordinates.
*/

#version 330

// uniforms
uniform sampler2D uni_sampler_2d;

/*  Built-in fragment shader inputs:
in vec4   gl_FragCoord;
in bool   gl_FrontFacing;
in vec2   gl_PointCoord;
in float  gl_ClipDistance[];
in int    gl_PrimitiveID;
in int    gl_SampleID;        // GLSL 4.0+
in vec2   gl_SamplePosition;  // GLSL 4.0+
in int    gl_SampleMaskIn[];  // GLSL 4.0+
in int    gl_Layer;           // GLSL 4.3+
in int    gl_ViewportIndex;   // GLSL 4.3+
*/

// inputs
/*layout (location = 0)*/ in vec2 var_uv;

/*  Built-in fragment shader outputs:
out float gl_FragDepth;
out int   gl_SampleMask[];
*/

// outputs
out vec4 out_frag_color;

// main
void main() {
  /*const*/ vec4 tex_color = texture (uni_sampler_2d, var_uv);

  if (0.0 < tex_color.a) {
    out_frag_color = tex_color;
  } else {
    discard;
  }
}
