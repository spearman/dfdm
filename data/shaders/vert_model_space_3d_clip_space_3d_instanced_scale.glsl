/*
Draws model space vertices with instanced world position.
*/

#version 330

// uniforms
uniform mat4 uni_projection_mat_perspective;
uniform mat4 uni_transform_mat_view;

/*  Built-in vertex shader inputs:
in int gl_VertexID;
in int gl_InstanceID;
*/

// inputs
// per instance
in vec3 in_position;
in vec3 in_scale;
// per vertex
in vec3 inst_position;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

// outputs
/* none */

mat3 mat_scale = mat3 (
  vec3 (in_scale.x, 0.0, 0.0),
  vec3 (0.0, in_scale.y, 0.0),
  vec3 (0.0, 0.0, in_scale.z));

// main
void main () {
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * vec4 (in_position + (mat_scale * inst_position), 1.0);
}
