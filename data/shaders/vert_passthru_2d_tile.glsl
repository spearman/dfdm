/*
2D tiles are defined by a row and a column position which are transformed into
screen (pixel) coordinates where the upper half of the screen are increasing
rows and the right half of the screen are increasing columns. The input tile
number is output unmodified.
*/

#version 330

uniform int   uni_gui_scale;
uniform ivec2 uni_viewport_dimensions;
uniform ivec2 uni_tile_dimensions;

/*  Built-in vertex shader inputs:
in int gl_VertexID;
in int gl_InstanceID;
*/

// inputs
in int  in_row;
in int  in_column;
in uint in_tile;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

// outputs
/*layout (location = 0)*/ out uint vert_tile;

// main
void main () {
  vec2 viewport_half_dimensions = vec2 (uni_viewport_dimensions / 2);
  vec2 scaled_tile_dimensions   = vec2 (uni_gui_scale * uni_tile_dimensions);
  vec2 pixel_coord              = viewport_half_dimensions + vec2 (
    float(in_column) * scaled_tile_dimensions.x,
    float(in_row)    * scaled_tile_dimensions.y
  );
  gl_Position = vec4 (pixel_coord, 0.0, 1.0);
  vert_tile = in_tile;
}
