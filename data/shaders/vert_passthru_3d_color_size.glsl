/*
No-op vertex shader passing on a 3D point with color and size attributes.
*/

#version 330

// uniforms
/* none */

/*  Built-in vertex shader inputs:
in int gl_VertexID;
in int gl_InstanceID;
*/

// inputs
in vec3  in_position;
in vec4  in_color;
in float in_size;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

// outputs
/*layout (location = 0)*/ out vec4  color;
/*layout (location = 1)*/ out float size;

// main
void main () {
  gl_Position = vec4 (in_position, 1.0);
  color       = in_color;
  size        = in_size;
}
