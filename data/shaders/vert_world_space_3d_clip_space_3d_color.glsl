/*
Vertices in 3D world space with a color attribute.
*/

#version 330

// uniforms
uniform mat4 uni_projection_mat_perspective;
uniform mat4 uni_transform_mat_view;

/*  Built-in vertex shader inputs:
in int gl_VertexID;
in int gl_InstanceID;
*/

// inputs
in vec3 in_position;
in vec4 in_color;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

// outputs
/*layout (location = 0)*/ out vec4 var_color;

// main
void main () {
  var_color   = in_color;
  gl_Position = uni_projection_mat_perspective * uni_transform_mat_view
    * vec4 (in_position, 1.0);
}
