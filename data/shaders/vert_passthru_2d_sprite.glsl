/*
2D sprites are defined by a center position and dimensions which are both
output unmodified.
*/

#version 330

/*  Built-in vertex shader inputs:
in int gl_VertexID;
in int gl_InstanceID;
*/

// inputs
in vec2 in_position;
in vec2 in_dimensions;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

// outputs
/*layout (location = 0)*/ out vec2 dimensions;

// main
void main () {
  dimensions = in_dimensions;
  gl_Position = vec4 (in_position, 0.0, 1.0);
}
