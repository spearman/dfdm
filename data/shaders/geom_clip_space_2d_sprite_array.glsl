/*
Draws a textured sprite given as a center position and dimensions in clip space
coordinates. Also passes on a texture array index to the fragment shader.
*/

#version 330


// input primitive
layout (points) in;

/*  Built-in geometry shader inputs:
in int gl_PrimitiveIDIn;
in int gl_InvocationID;   // GLSL 4.0+
in gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_in[];
*/

// inputs
/*layout (location = 0)*/ in vec2 dimensions[];
/*layout (location = 1)*/ in uint texture_index[];

/*  Built-in geometry shader outputs:
out int gl_PrimitiveID;
out int gl_Layer;
out int gl_ViewportIndex;
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
*/

// output primitive
layout (triangle_strip, max_vertices = 4) out;

// outputs
/*layout (location = 0)*/      out vec2 var_uv;
/*layout (location = 1)*/ flat out uint flat_texture_index;

// constants
/*const*/ vec2 dimensions_div2 = dimensions[0] * 0.5;

// main
void main () {
  // lower-left corner
  gl_Position = gl_in[0].gl_Position + vec4 (-dimensions_div2, 0.0, 0.0);
  var_uv      = vec2 (0.0, 0.0);
  flat_texture_index = texture_index[0];
  EmitVertex();
  // upper-left corner
  gl_Position = gl_in[0].gl_Position
    + vec4 (-dimensions_div2.x, dimensions_div2.y, 0.0, 0.0);
  var_uv      = vec2 (0.0, 1.0);
  flat_texture_index = texture_index[0];
  EmitVertex();
  // lower-right corner
  gl_Position = gl_in[0].gl_Position
    + vec4 (dimensions_div2.x, -dimensions_div2.y, 0.0, 0.0);
  var_uv      = vec2 (1.0, 0.0);
  flat_texture_index = texture_index[0];
  EmitVertex();
  // upper-right corner
  gl_Position = gl_in[0].gl_Position + vec4 (dimensions_div2, 0.0, 0.0);
  var_uv      = vec2 (1.0, 1.0);
  flat_texture_index = texture_index[0];
  EmitVertex();
}
