/*
"No-op" 3D vertex shader that also passes on an orientation matrix for each
vertex.

It is acceptable to input vertices with extra attributes, but it is a runtime
error if the `in_position` attribute is not a `vec3`.
*/

#version 330

/*  Built-in vertex shader inputs:
in int gl_VertexId;
in int gl_InstanceId;
*/

// inputs
in vec3 in_position;
in mat3 in_orientation;

/*  Built-in vertex shader outputs:
out gl_PerVertex {
  vec4  gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
}
*/

// outputs
/*layout (location = 0)*/ out mat3 orientation;

void main () {
  gl_Position = vec4 (in_position, 1.0);
  orientation = in_orientation;
}
