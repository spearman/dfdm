// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use std::time;

use crate::simulation;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Timer {
  t_base          : time::Instant,
  step            : u64,
  /// Simulation step interval
  step_ms         : u16,
  /// Simulation step interval
  step_ns         : u64,
  step_next_ts    : TimestampNs,
  /// This is an internal variable used by the `step_begin()`, `step_end()`
  /// interface
  step_begin_ts   : TimestampNs,
  /// This is an internal variable used by the `step_begin()`, `step_end()`
  /// interface
  step_end_ts     : TimestampNs,
  /// Last elapsed step time as measured from `step_begin_ts` to `step_end_ts`
  step_elapsed_ns : u64,
  /// Average step *duration* (not time between steps)
  step_average_ns : u64
}

/// A timestamp measured as a nanosecond duration since the base initialization
/// time.
#[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
pub struct TimestampNs (u64);

impl Timer {
  /// Creates a new timer which can return nanosecond timestamps representing
  /// duration since this timer was started.
  pub fn start (step : u64, step_ms : u16) -> Self {
    let step_ns         = 1_000_000 * step_ms  as u64;
    let step_next_ts    = TimestampNs (step_ns);
    let step_begin_ts   = TimestampNs (0);
    let step_end_ts     = TimestampNs (0);
    let step_elapsed_ns = 0;
    let step_average_ns = 0;
    let t_base          = time::Instant::now();
    Timer {
      t_base,
      step,
      step_ms,
      step_ns,
      step_next_ts,
      step_begin_ts,
      step_end_ts,
      step_elapsed_ns,
      step_average_ns
    }
  }

  /// Returns the current step
  #[inline]
  pub fn step_current (&self) -> u64 {
    self.step
  }

  /// Returns the step time in milliseconds
  #[inline]
  pub fn step_ms (&self) -> u16 {
    self.step_ms
  }

  /// Returns the step time in nanoseconds
  #[inline]
  pub fn step_ns (&self) -> u64 {
    self.step_ns
  }

  /// Returns the current average step length in nanoseconds
  #[inline]
  pub fn average_step_ns (&self) -> u64 {
    self.step_average_ns
  }

  /// Returns the last step begin timestamp
  #[inline]
  pub fn last_begin_ts (&self) -> TimestampNs {
    self.step_begin_ts
  }

  /// Returns the last step end timestamp
  #[inline]
  pub fn last_end_ts (&self) -> TimestampNs {
    self.step_end_ts
  }

  /// Returns the last step duration as nanoseconds
  #[inline]
  pub fn last_elapsed_ns (&self) -> u64 {
    self.step_elapsed_ns
  }

  /// Returns the timestamp of the next step
  #[inline]
  pub fn next_step_ts (&self) -> TimestampNs {
    self.step_next_ts
  }

  /// Returns `Ok(timestamp)` if the returned time was after the next step time,
  /// or `Err(timestamp)` if the current time was before the next step time.
  #[inline]
  pub fn is_step_time (&self) -> Result <TimestampNs, TimestampNs>  {
    let ts_now = self.timestamp_now();
    if ts_now < self.step_next_ts {
      Err (ts_now)
    } else {
      Ok (ts_now)
    }
  }

  #[inline]
  pub fn timestamp_now (&self) -> TimestampNs {
    let t_now = self.t_base.elapsed().as_nanos();
    debug_assert!(t_now <= std::u64::MAX as u128);
    TimestampNs (t_now as u64)
  }

  /// Saves a timestamp to begin timing the current step
  #[inline]
  pub fn step_begin (&mut self) {
    self.step_begin_ts = self.timestamp_now();
  }
  /// Computes the elapsed step time, updates the average step length, and
  /// increments the step counter
  #[inline]
  pub fn step_end (&mut self) {
    self.step_end_ts     = self.timestamp_now();
    self.step_elapsed_ns = self.step_end_ts.0 - self.step_begin_ts.0;
    self.step_average_ns = if self.step_average_ns > 0 {
      (self.step_average_ns + self.step_elapsed_ns) / 2
    } else {
      self.step_elapsed_ns
    };
    self.step_next_ts += self.step_ns;
    self.step += 1;
  }

}

impl std::ops::Deref for TimestampNs {
  type Target = u64;
  fn deref (&self) -> &u64 {
    &self.0
  }
}
impl std::ops::DerefMut for TimestampNs {
  fn deref_mut (&mut self) -> &mut u64 {
    &mut self.0
  }
}
impl std::ops::Add <u64> for TimestampNs {
  type Output = Self;
  fn add (self, rhs : u64) -> Self {
    TimestampNs (*self + rhs)
  }
}
impl std::ops::AddAssign <u64> for TimestampNs {
  fn add_assign (&mut self, rhs : u64) {
    self.0 += rhs;
  }
}
impl std::ops::Sub <u64> for TimestampNs {
  type Output = Self;
  fn sub (self, rhs : u64) -> Self {
    TimestampNs (*self - rhs)
  }
}

impl Default for Timer {
  fn default() -> Self {
    Timer::start (0, simulation::STATIC_CONFIG.step_ms)
  }
}
