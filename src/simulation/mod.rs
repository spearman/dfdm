// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use serde_yaml;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};

use rs_utils::show;

pub mod history;
pub mod timer;
pub mod world;

pub use self::history::History;
pub use self::timer::Timer;
pub use self::world::World;

////////////////////////////////////////////////////////////////////////////////
//  statics                                                                   //
////////////////////////////////////////////////////////////////////////////////

lazy_static!{
  /// Static configuration to be loaded from `./data/simulation-config.yaml`
  pub static ref STATIC_CONFIG : Config = {
    use std::fs::File;
    let config_file = File::open ("data/simulation-config.yaml").unwrap();
    serde_yaml::from_reader::<File, Config> (config_file).unwrap()
  };
}

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Config {
  pub step_ms                : u16,
  pub world_gravity          : f64,
  pub player_friction        : f64,
  pub player_restitution     : f64,
  pub player_acceleration    : f64,
  pub player_min_velocity    : f64,
  pub player_max_velocity    : f64,
  pub player_turn_speed      : f64,
  /// Attack duration in number of simulation steps.
  pub player_attack_duration : u16
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Simulation {
  pub config  : Config,
  /// Circular buffer of world states and commands
  pub history : History,
  #[serde(skip)]
  pub timer   : Timer
}

////////////////////////////////////////////////////////////////////////////////
//  enums                                                                     //
////////////////////////////////////////////////////////////////////////////////

/// TODO: either ordering or idempotency should be explicitly enforced
/// to allow determinate simulation
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum Command {
  Player (world::player::Id, world::player::Command)
}

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

pub fn report_sizes() {
  use std::mem::size_of;
  println!("simulation report sizes...");
  show!(size_of::<Simulation>());
  show!(size_of::<Command>());
  World::report_sizes();
  println!("...simulation report sizes");
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

impl Simulation {
  #[inline]
  pub fn new (
    config               : Config,
    history_size         : usize,
    history_future_steps : usize,
    init                 : (u64, World, Vec <Command>)
  ) -> Self {
    let step    = init.0;
    let history = History::new (
      history_size,
      history_future_steps,
      init);
    let timer = Timer::start (step, config.step_ms);
    Simulation { config, history, timer }
  }
  #[inline]
  pub fn step_current (&self) -> u64 {
    self.history.current_step()
  }
  #[inline]
  pub fn world_current (&self) -> &World {
    &self.history.current().1
  }
  #[inline]
  pub fn commands_current (&self) -> &Vec <Command> {
    &self.history.current().2
  }
  /// Returns optional because if the current step is also the oldest step there
  /// is no previous step
  #[inline]
  pub fn commands_previous (&self) -> Option <&Vec <Command>> {
    let current_step = self.step_current();
    if current_step > 0 {
      self.history.get_step (current_step-1).map (|step| &step.2)
    } else {
      None
    }
  }
  /*
  #[inline]
  pub fn world_current_mut (&mut self) -> &mut World {
    &mut self.history.current_mut().1
  }
  */
  #[inline]
  pub fn schedule_command (&mut self, step : u64, command : Command) {
    self.history.insert_command (step, command, &self.config)
  }
  #[inline]
  pub fn schedule_command_current (&mut self, command : Command) {
    let current_step = self.history.current_step();
    self.schedule_command (current_step, command)
  }
  #[inline]
  pub fn step (&mut self) {
    log::trace!("Simulation step...");
    self.timer.step_begin();
    self.history.step (&self.config);
    self.timer.step_end();
    log::trace!("  elapsed time: {:?}", std::time::Duration::from_nanos (
      self.timer.last_elapsed_ns()));
    log::trace!("...Simulation step");
  }
}

impl Default for Simulation {
  fn default() -> Self {
    Simulation {
      config:  STATIC_CONFIG.clone(),
      history: History::default(),
      timer:   Timer::default()
    }
  }
}
