// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use std;
use log;
use linear_sim;
use serde::{Deserialize, Serialize};
use vec_map::VecMap;

use crate::simulation::{self, Command};

pub mod player;

pub use self::player::Player;

////////////////////////////////////////////////////////////////////////////////
//  constants                                                                 //
////////////////////////////////////////////////////////////////////////////////

pub const MAX_PLAYER_ID        : u8  = SystemObjectKeysDynamic::Player16 as u8;
const INITIAL_PLAYER_VEC_SIZE  : u16 = 16;

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

//
//  struct World
//
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct World {
  pub step         : u64,
  pub players      : VecMap <Player>,
  /// Corpses are created as dynamic objects with increasing indices starting
  /// from MAX_PLAYER_ID+1
  pub corpse_count : u32,
  /// Physical simulation
  pub system       :
    linear_sim::System <linear_sim::integrator::SemiImplicitEuler>
}

////////////////////////////////////////////////////////////////////////////////
//  enums                                                                     //
////////////////////////////////////////////////////////////////////////////////

/// Dynamic object keys in the physical simulation.
///
/// *Note*: `Player1` has discriminant '`0`', `Player2` has discriminant '`1`',
/// and so on.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum SystemObjectKeysDynamic {
  Player1 = 0,
  Player2,
  Player3,
  Player4,
  Player5,
  Player6,
  Player7,
  Player8,
  Player9,
  Player10,
  Player11,
  Player12,
  Player13,
  Player14,
  Player15,
  Player16
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum SystemObjectKeysStatic {
  Ground = 0
}

#[derive(Debug, PartialEq)]
pub enum NewPlayerError {
  CreateObjectError (Vec <linear_sim::event::CreateObjectResult>)
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

//
//  impl World
//
impl World {

  /// Create a default `World` on step `0`
  pub fn create (config : &simulation::Config) -> Self {
    use linear_sim::geometry::shape;
    use linear_sim::*;
    let step    = 0;
    let players = VecMap::with_capacity (INITIAL_PLAYER_VEC_SIZE as usize);
    let corpse_count = 0;
    let system  = {
      let mut system = System::<integrator::SemiImplicitEuler>::new();
      // create gravity force
      let gravity_acceleration = {
        let step_secs = config.step_ms as f64 / 1000.0;
        [ 0.0, 0.0, config.world_gravity * step_secs * step_secs ]
      };
      let gravity = force::Gravity { acceleration: gravity_acceleration.into() };
      assert!(system.handle_event (event::Input::SetGravity (gravity)).is_empty());
      // create static object: ground orthant
      let ground_object = {
        let position = component::Position ([0.0, 0.0, 0.0].into());
        let bound    = component::Bound    (shape::Unbounded::from (
          shape::Orthant::from (math::SignedAxis3::PosZ)).into());
        let material = component::MATERIAL_STONE;
        object::Variant::Static (object::Static { position, bound, material })
      };
      assert!(system.handle_event (event::Input::CreateObject (
        ground_object, object::Key::from (SystemObjectKeysStatic::Ground as u32))
      ).is_empty());
      system
    };
    World { step, players, corpse_count, system }
  }

  #[inline]
  pub fn player_new (&mut self,
    player_id : player::Id, player : Player, config : &simulation::Config
  ) -> Result <(), NewPlayerError> {
    use linear_sim::*;
    debug_assert!(*player_id <= SystemObjectKeysDynamic::Player16 as u8);
    // enter initial data in collision pipeline
    let dynamic_key          = object::Key::from (player_id.0 as u32);
    let dynamic_object       = player.object (config);
    let create_object_output = self.system.handle_event (
      event::Input::CreateObject (dynamic_object.into(), dynamic_key));
    let errors : Vec <event::CreateObjectResult> = create_object_output
      .into_iter().filter_map (|event| match event {
        event::Output::CreateObjectResult (
          intersection @ event::CreateObjectResult::Intersection (..)
        ) => Some (intersection),
        _ => unreachable!("only create object result events should be returned")
      }
    ).collect();
    if !errors.is_empty() {
      Err (NewPlayerError::CreateObjectError (errors))
    } else {
      assert!(self.players.insert (player_id.into(), player).is_none());
      Ok (())
    }
  }

  #[inline]
  pub fn player_delete (&mut self, player_id : player::Id) {
    use linear_sim::*;
    self.players.remove (player_id.into()).unwrap();
    let object_id = {
      let kind = object::Kind::Dynamic;
      let key  = object::Key::from (player_id.0 as u32);
      object::Identifier { kind, key }
    };
    self.system.handle_event (event::Input::DestroyObject (object_id));
  }

  #[inline]
  pub fn player_move (&mut self,
    player_id : player::Id, direction : player::MoveState
  ) {
    let player = &mut self.players[player_id.0 as usize];
    player.move_state.move_direction (direction);
  }

  #[inline]
  pub fn player_turn (&mut self,
    player_id : player::Id, direction : player::TurnState
  ) {
    let player = &mut self.players[player_id.0 as usize];
    player.turn_state.turn_direction (direction);
  }

  #[inline]
  pub fn player_rotate (&mut self,
    player_id   : player::Id,
    delta_yaw   : cgmath::Rad <f64>,
    delta_pitch : cgmath::Rad <f64>
  ) {
    let player = &mut self.players[player_id.0 as usize];
    player.rotate_yaw   (delta_yaw);
    player.rotate_pitch (delta_pitch);
    player.compute_orientation();
  }

  #[inline]
  pub fn player_set_orient (&mut self,
    player_id : player::Id,
    abs_yaw   : Option <cgmath::Rad <f64>>,
    abs_pitch : Option <cgmath::Rad <f64>>
  ) {
    let player = &mut self.players[player_id.0 as usize];
    let mut modified = false;
    abs_yaw.map   (|yaw| {
       player.yaw = yaw;
       modified   = true;
    });
    abs_pitch.map (|pitch| {
      player.pitch = pitch;
      modified     = true;
    });
    if modified {
      player.compute_orientation();
    }
  }

  #[inline]
  pub fn player_jump (&mut self, player_id : player::Id,) {
    let player = &mut self.players[player_id.0 as usize];
    player.jump();
  }

  #[inline]
  pub fn player_walk (&mut self, player_id : player::Id) {
    let player = &mut self.players[player_id.0 as usize];
    player.walk = true;
  }

  #[inline]
  pub fn player_run (&mut self, player_id : player::Id) {
    let player = &mut self.players[player_id.0 as usize];
    player.walk = false;
  }

  #[inline]
  pub fn player_attack (&mut self,
    player_id   : player::Id,
    attack_kind : player::AttackKind
  ) {
    let player = &mut self.players[player_id.0 as usize];
    player.attack (self.step, attack_kind);
  }

  pub fn step (&mut self,
    step : u64, commands : &Vec <Command>, config : &simulation::Config
  ) {
    log::trace!("World step...");

    use linear_sim::*;
    // step system
    let system_events = self.system.handle_event (event::Input::Step);
    for event in system_events.into_iter() {
      match event {
        event::Output::CollisionResolve (event::CollisionResolve {
          /*toi,*/ object_id_a, object_id_b, contact,
          ..
        }) => {
          // resting contact: set player jump state to standing
          // TODO: resting contact
          //if *toi == 0.0
          use std::f64::consts::FRAC_1_SQRT_2;
          debug_assert_eq!(object_id_a.kind, object::Kind::Dynamic);
          let key_a = object_id_a.key;
          if *key_a <= MAX_PLAYER_ID as u32 {
            if contact.contact.constraint.normal().z > FRAC_1_SQRT_2 {
              self.players[*key_a as usize].jump_state =
                player::JumpState::Standing;
            }
          }
          match object_id_b.kind {
            object::Kind::Dynamic => {
              let key_b = object_id_b.key;
              if *key_b <= MAX_PLAYER_ID as u32 {
                if contact.contact.constraint.normal().z < -FRAC_1_SQRT_2 {
                  self.players[*key_b as usize].jump_state =
                    player::JumpState::Standing;
                }
              }
            }
            object::Kind::Static => {}
            _ => unreachable!()
          }
        }
        _ => unreachable!("unexpected system step output event")
      }
    }

    for command in &commands[..] {
      match command {
        Command::Player (id, player::Command::Spawn (player)) => {
          // a hack to resolve intersections
          // a better method will probably be needed if level geometry is added
          let mut player = player.clone();
          loop {
            match self.player_new  (*id, player.clone(), config) {
              Ok(()) => break,
              Err (NewPlayerError::CreateObjectError (events)) => {
                let mut intersection = false;
                for event in events {
                  match event {
                    linear_sim::event::CreateObjectResult::Intersection (_, _) => {
                      intersection = true;
                    }
                  }
                }
                if intersection {
                    player.position.z += 2.0;
                }
              }
            }
          }
        }
        Command::Player (id, player::Command::Quit) => self.player_delete (*id),
        Command::Player (id, player::Command::Move (direction)) =>
          self.player_move (*id, direction.clone()),
        Command::Player (id, player::Command::Turn (direction)) =>
          self.player_turn (*id, direction.clone()),
        Command::Player (id, player::Command::Rotate (delta_yaw, delta_pitch)) =>
          self.player_rotate (*id, *delta_yaw, *delta_pitch),
        Command::Player (id, player::Command::SetOrient (abs_yaw, abs_pitch)) =>
          self.player_set_orient (*id, *abs_yaw, *abs_pitch),
        Command::Player (id, player::Command::Jump) => self.player_jump (*id),
        Command::Player (id, player::Command::Walk) => self.player_walk (*id),
        Command::Player (id, player::Command::Run)  => self.player_run  (*id),
        Command::Player (id, player::Command::Attack (attack_kind)) =>
          self.player_attack (*id, attack_kind.clone())
      }
    }

    // step players
    let mut player_actions    : Vec <(player::Id, player::Action)> = Vec::new();
    let mut linear_sim_events : Vec <linear_sim::event::Input>     = Vec::new();
    for (id, player) in self.players.iter_mut() {
      debug_assert!(id < MAX_PLAYER_ID as usize);
      let player_id = player::Id (id as u8);
      {
        // new position & velocity
        let object   = &self.system.objects_dynamic()[id];
        let component::Position (ref position) = &object.position;
        let velocity = &object.derivatives.velocity;
        player.position = *position;
        player.velocity = *velocity;
      }
      // player update
      let object_key = object::Key::from_raw (id as u32);
      let results    = player.step (object_key, step, config);
      for result in results.into_iter() {
        match result {
          player::StepResult::Action (action) =>
            player_actions.push ((player_id, action)),
          player::StepResult::LinearSimEvent (event)  =>
            linear_sim_events.push (event)
        }
      }
    }

    // process actions
    // NB: for player actions to be valid, players should not be destroyed
    // during the player.step phase
    for (player_id, action) in player_actions.into_iter() {
      let player_index = player_id.0 as usize;
      match action {
        player::Action::MeleeAttack { hit_segment, /*kind*/.. } => {
          for (id, player) in self.players.iter_mut() {
            if id != player_index {
              use linear_sim::object::Bounded;
              let object = &self.system.objects_dynamic()[id];
              if let Some (hit_point) = object.hit_test (&hit_segment) {
                player.wounds.push (player::Wound (hit_point));
                player.health = player.health
                  .saturating_sub (player::BASE_WEAPON_DAMAGE);
                // TODO: weapon-specific damage
                // TODO: knockback
              }
            }
          }
        }
      }
    }

    for event in linear_sim_events.into_iter() {
      assert!(self.system.handle_event (event).is_empty());
    }

    // handle player deaths and respawn
    for (id, player) in self.players.iter_mut() {
      if player.health == 0 {
        let mut corpse_object = self.system.objects_dynamic()[id].clone();
        let player_key        = object::Key::from (id as u32);
        let player_object_id  = object::Identifier {
          kind: object::Kind::Dynamic, key: player_key
        };
        // destroy old player object
        assert!(self.system.handle_event (
          event::Input::DestroyObject (player_object_id)
        ).is_empty());
        // respawn
        // TODO: better spawn point handling
        player.position     = [0.0, 0.0, 10.0].into();
        player.yaw          = cgmath::Rad (0.0).into();
        player.pitch        = cgmath::Rad (0.0).into();
        player.compute_orientation();
        player.velocity     = [0.0, 0.0, 0.0].into();
        player.acceleration = [0.0, 0.0, 0.0].into();
        player.health       = player::BASE_PLAYER_HEALTH;
        player.attack_state = None;
        player.wounds.clear();
        // a hack to resolve intersections
        // a better method will probably be needed if level geometry is added
        loop {
          // create new player object
          let new_player_object = player.object (config);
          let create_result = self.system.handle_event (
            event::Input::CreateObject (new_player_object.into(), player_key));
          let mut intersection = false;
          for event in create_result.into_iter() {
            match event {
              event::Output::CreateObjectResult (
                event::CreateObjectResult::Intersection (_, _)
              ) => {
                intersection = true;
              }
              _ => unreachable!("only create object result events should be returned")
            }
          }
          if intersection {
              player.position.z += 2.0;
          } else {
            break
          }
        }
        // modify the corpse object bounding volume and create it
        corpse_object.bound = component::Bound (
          geometry::shape::Variant::Bounded (
            geometry::shape::Capsule::unchecked (
              player::CORPSE_RADIUS, player::CORPSE_HALF_HEIGHT).into()));
        let corpse_key =
          object::Key::from (MAX_PLAYER_ID as u32 + self.corpse_count + 1);
        // NB: this should never fail since we are spawning the corpse inside
        // the volume that was occupied by the player as long as the corpse
        // radius is smaller than the original player radius AND as long as the
        // new player was not respawned in the same spot
        assert!(self.system.handle_event (
          event::Input::CreateObject (corpse_object.into(), corpse_key)
        ).is_empty());
        self.corpse_count += 1;
      }
    }

    self.step += 1;

    log::trace!("...World step");
  }

  pub fn report_sizes() {
    println!("World report sizes...");
    println!("  size of World: {}", std::mem::size_of::<World>());
    Player::report_sizes();
    println!("...World report sizes");
  }

} // end impl World
