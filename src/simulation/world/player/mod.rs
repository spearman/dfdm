// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use {std, cgmath};
use linear_sim::{self, geometry};
use serde::{Deserialize, Serialize};

use crate::simulation;

pub mod class;

pub use self::class::Class;

////////////////////////////////////////////////////////////////////////////////
//  constants                                                                 //
////////////////////////////////////////////////////////////////////////////////

pub const BASE_WEAPON_DAMAGE : u16 =   4;
pub const BASE_PLAYER_HEALTH : u16 =  30;
pub const CORPSE_RADIUS      : f64 = 0.2;
pub const CORPSE_HALF_HEIGHT : f64 = 0.2;
/// Melee attack length must be longer than this value (default 30 steps)
// TODO: enforce limit or make it unnecessary
pub const MIN_MELEE_ATTACK_STEPS : u16 = 10;

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

//
//  struct Player
//
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct Player {
  pub class           : Class,
  pub position        : cgmath::Point3  <f64>,
  pub orientation     : cgmath::Basis3  <f64>,
  /// Restricted to $[0, 2\pi)$
  pub yaw             : cgmath::Rad     <f64>,
  /// Restricted to $[-pi/2,pi/2]$
  pub pitch           : cgmath::Rad     <f64>,
  pub velocity        : cgmath::Vector3 <f64>,
  pub acceleration    : cgmath::Vector3 <f64>,
  pub walk            : bool,
  pub jump            : bool,
  pub move_state      : MoveState,
  pub turn_state      : TurnState,
  pub jump_state      : JumpState,
  pub attack_state    : Option <AttackState>,
  /// Attack length measured in simulation steps.
  pub attack_duration : u16,
  pub health          : u16,
  pub wounds          : Vec <Wound>
}

//
//  struct Id
//
#[derive(Serialize, Deserialize, Clone,Copy,Debug,Eq,Ord,PartialEq,PartialOrd)]
pub struct Id (pub u8);

//
//  struct MoveState
//
/// Represents combination of forward/backward, right/left, up/down as ternary
/// states (in a right-handed coordinate system).
///
/// Stored internally by a signed byte-size counter. Therefore a series of
/// increments (resp. decrements) must be matched by an equal number of
/// decrements (resp increments) to return to a neutral state. Note because of
/// the size of the counter, a maximum of 127 "activations" may be made in any
/// one direction. This should be sufficient to allow for multiple keyboard
/// input keys to be mapped to the same movement action.
///
/// &#9888; Wrapping increment/decrement is not currently prevented.

// TODO: introduce an actual ternary type ?
#[derive(Serialize, Deserialize, Clone,Debug,Default,Eq,PartialEq)]
pub struct MoveState {
  forward_backward : i8,   // forward == 1, backward == -1
  right_left       : i8,   // right == 1, left == -1
  up_down          : i8    // up == 1, down == -1
}

//
//  struct TurnState
//
/// Represents signum state for turning right/left (yaw) and up/down (pitch).
///
/// See documentation of `MoveState` for a description of usage.
// TODO: introduce an actual ternary type ?
#[derive(Serialize, Deserialize, Clone,Debug,Default,Eq,PartialEq)]
pub struct TurnState {
  right_left : i8,  // right == 1, left == -1
  up_down    : i8   // up == 1, down == -1
}

//
//  struct AttackState
//
#[derive(Serialize, Deserialize, Clone,Debug,Eq,PartialEq)]
pub struct AttackState {
  pub attack_kind : AttackKind,
  pub step_start  : u64,
  pub step_end    : u64
}

//
//  struct Id
//
#[derive(Serialize, Deserialize, Clone,Debug,PartialEq)]
pub struct Wound (pub cgmath::Point3 <f64>);

////////////////////////////////////////////////////////////////////////////////
//  enums                                                                     //
////////////////////////////////////////////////////////////////////////////////

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum Command {
  Spawn (Player),
  Quit,
  Move      (MoveState),
  Turn      (TurnState),
  /// `(delta_yaw, delta_pitch)`
  Rotate    (cgmath::Rad <f64>, cgmath::Rad <f64>),
  /// `(abs_yaw, abs_pitch)`
  SetOrient (Option <cgmath::Rad <f64>>, Option <cgmath::Rad <f64>>),
  Jump,
  Walk,
  Run,
  // TODO: crouch
  //Crouch,
  Attack    (AttackKind)
}

#[derive(Serialize, Deserialize, Clone,Debug,Eq,PartialEq)]
pub enum JumpState {
  Standing,
  Jumping
}

//
//  enum AttackKind
//
#[derive(Serialize, Deserialize, Clone,Debug,Eq,PartialEq)]
pub enum AttackKind {
  Melee   (MeleeKind),
  Missile (MissileKind),
  Spell   (SpellKind)
}

//
//  enum MeleeKind
//
#[derive(Serialize, Deserialize, Clone,Debug,Eq,PartialEq)]
pub enum MeleeKind {
  /// weapon graphic row 1
  Chop,
  /// weapon graphic row 2
  SlashLowLeft,
  /// weapon graphic row 3
  SlashHighLeft,
  /// weapon graphic row 4
  SlashHighRight,
  /// weapon graphic row 5
  SlashLowRight,
  /// weapon graphic row 6
  Stab
}

//
//  enum MissileKind
//
#[derive(Serialize, Deserialize, Clone,Debug,Eq,PartialEq)]
pub enum MissileKind {
  Bow
}

//
//  enum SpellKind
//
#[derive(Serialize, Deserialize, Clone,Debug,Eq,PartialEq)]
pub enum SpellKind {
  Fire,
  Frost,
  Magic,
  Poison,
  Shock
}

//
//  enum Melee
//
#[derive(Serialize, Deserialize, Clone,Debug,Eq,PartialEq)]
pub enum Melee {
  HandToHand,
  Staff,
  Dagger,
  ShortSword,
  LongSword,
  GreatSword,
  Axe,
  Hammer
}

#[derive(Clone, Debug, PartialEq)]
pub enum Action {
  MeleeAttack {
    kind        : MeleeKind,
    hit_segment : geometry::Segment3 <f64>
  }
}

/// The result of updating a player to the next step.
#[derive(Debug, PartialEq)]
pub enum StepResult {
  /// A player action which can affect the world
  Action         (Action),
  /// Event to be handled by the linear simulation subsystem
  LinearSimEvent (linear_sim::event::Input)
}

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

/// Compute the eye point from a player position and class
#[inline]
pub fn eye (mut position : cgmath::Point3 <f64>, class : Class)
  -> cgmath::Point3 <f64>
{
  position.z += (7.0/16.0) * class.data().height;
  position
}

/// Compute the eye point from a player position and class.
///
/// f32 version for use by renderer code.
#[inline]
pub fn eye_f32 (mut position : cgmath::Point3 <f32>, class : Class)
  -> cgmath::Point3 <f32>
{
  position.z += (7.0/16.0) * class.data().height as f32;
  position
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

//
//  impl Player
//
impl Player {
  /// Create a player in default position and orientation: at the origin
  /// looking down the positive $Y$-axis.
  pub fn create (config : &simulation::Config) -> Self {
    use cgmath::{
      EuclideanSpace,
      One,
      Zero};
    let class           = Class::MageNormalF;
    let position        = cgmath::Point3::<f64>::origin();
    let orientation     = cgmath::Basis3::<f64>::one();
    let yaw             = cgmath::Rad (0.0);
    let pitch           = cgmath::Rad (0.0);
    let velocity        = cgmath::Vector3::<f64>::zero();
    let acceleration    = cgmath::Vector3::<f64>::zero();
    let walk            = false;
    let jump            = false;
    let move_state      = MoveState::default();
    let turn_state      = TurnState::default();
    let jump_state      = JumpState::default();
    let attack_state    = None;
    let attack_duration = config.player_attack_duration;
    let wounds          = Vec::new();
    let health          = BASE_PLAYER_HEALTH; // TODO: class specific health
    Player {
      class,
      position,
      orientation,
      yaw,
      pitch,
      velocity,
      acceleration,
      walk,
      jump,
      move_state,
      turn_state,
      jump_state,
      attack_state,
      attack_duration,
      wounds,
      health
    }
  }

  /// Create a player with given position and heading (degrees where the
  /// positive $Y$ direction is $0^\degree$).
  pub fn with_position_heading (
    position : cgmath::Point3 <f64>,
    heading  : cgmath::Deg <f64>,
    config   : &simulation::Config
  ) -> Self {
    let mut player  = Self::create (config);
    player.position = position;
    player.yaw      = heading.into();
    player.compute_orientation();
    player
  }

  pub fn object (&self, config : &simulation::Config) -> linear_sim::object::Dynamic {
    use linear_sim::*;
    let class::Data { width, height, mass, .. } = self.class.data();
    debug_assert!(height >= width);
    let position    = component::Position  (self.position);
    let mass        = component::Mass::new (*mass);
    let derivatives = component::Derivatives::zero();
    let bound       = component::Bound (geometry::shape::Variant::Bounded (
      geometry::shape::Capsule::noisy (0.5 * *width, 0.5 * (*height - *width))
        .into()));
    let material    = component::Material {
      restitution: config.player_restitution,
      friction:    config.player_friction
    };
    object::Dynamic { position, mass, derivatives, bound, material }
  }

  /// Compute the player view position
  #[inline]
  pub fn eye (&self) -> cgmath::Point3 <f64> {
    eye (self.position, self.class)
  }

  pub fn step (&mut self,
    object_key : linear_sim::object::Key,
    step       : u64,
    config     : &simulation::Config
  ) -> Vec <StepResult> {
    use cgmath::{
      InnerSpace,
      //Rotation,
      //Rotation3,
      Zero};
    use linear_sim::*;

    let mut out = Vec::new();

    // clear wounds
    self.wounds.clear();

    // step time as a fractional second
    let step_secs  = config.step_ms as f64 / 1000.0;
    let class_data = self.class.data();

    // new orientation based on turn state
    let delta_yaw   = self.turn_state.right_left() as f64
      * config.player_turn_speed;
    let delta_pitch = self.turn_state.up_down() as f64
      * config.player_turn_speed;
    if delta_yaw != 0.0 || delta_pitch != 0.0 {
      if delta_yaw != 0.0 {
        // restricts yaw to [0.0, 2*PI)
        self.rotate_yaw (cgmath::Rad (delta_yaw));
      }
      if delta_pitch != 0.0 {
        // restricts pitch to [-PI/2, PI/2]
        self.rotate_pitch (cgmath::Rad (delta_pitch));
      }
      self.compute_orientation();
    }
    // XY basis used in later computations
    let xy_basis = cgmath::Matrix3::from_angle_z (-self.yaw);
    debug_assert_eq!(xy_basis.z, cgmath::vec3 (0.0, 0.0, 1.0));
    debug_assert_eq!(xy_basis.x.z, 0.0);
    debug_assert_eq!(xy_basis.y.z, 0.0);
    // end new orientation

    // NOTE: to calculate the required velocity V for a jump height H
    // under gravity acceleration G:
    //   V = sqrt (2 * G * H)
    // TODO: class specific
    let impulse_jump = {
      let magnitude = if !self.jump {
        0.0
      } else {
        self.jump = false;
        if self.walk {
          // jump height of ~1.0m with g == -19.6 m/s2
          6.26 * step_secs
        } else {
          10.0 * step_secs
          // jump height of ~4.0m with g == -19.6 m/s2
          //12.52 * step_secs
          // jump height of ~2.0m with g == -9.8 m/s2
          //6.3 * step_secs
          // initial velocity equal to max velocity
          // config.player_max_velocity * step_secs
        }
      };
      cgmath::vec3 (0.0, 0.0, magnitude)
    };

    // new acceleration
    self.acceleration = cgmath::Vector3::<f64>::zero();
    // movement acceleration: only if not "jumping"
    // TODO: air control for thief classes ?
    /*
    match self.jump_state {
      JumpState::Standing => {
      }
      JumpState::Jumping  => {
      }

    }
    */
    // calculate max velocities above which acceleration will not be applied
    // - strafe max is 80% as fast as forward max
    let strafe_velocity_percent = 0.80;
    // - walk velocity
    let (max_velocity_forward, max_velocity_strafe) = if self.walk {
      // 2.8m/s == double average walking speed of ~1.4m/s
      ( 2.8 * step_secs, 2.8 * step_secs * strafe_velocity_percent)
    } else {
      // max velocity
      ( config.player_max_velocity * step_secs,
        config.player_max_velocity * step_secs
          * strafe_velocity_percent
      )
    };

    // apply movement acceleration: will not be applied in directions
    // that exceed the maximum
    // TODO: this velocity check will prevent all acceleration when player is
    // moving faster than max forward velocity; we would like to only prevent
    // acceleration in directions that exceed maximum velocity in that
    // direction, however strafe-running while turning can allow the player to
    // reach very high velocities so this enforces a kind of maximum velocity
    let impulse_move = if self.velocity.magnitude() <
      ( max_velocity_forward * max_velocity_forward +
        max_velocity_strafe * max_velocity_strafe ).sqrt()
    {
      let local_velocity_x = xy_basis.x.dot (self.velocity);
      let local_velocity_y = xy_basis.y.dot (self.velocity);
      let move_vec         = self.move_state.clone().to_vec();
      let move_x = if
        (local_velocity_x < -max_velocity_strafe && move_vec.x < 0.0)
        || (max_velocity_strafe < local_velocity_x && 0.0 < move_vec.x)
      {
        cgmath::Vector3::zero()
      } else {
        move_vec.x * xy_basis.x
      };
      let move_y = if
        (local_velocity_y < -max_velocity_forward && move_vec.y < 0.0)
        || (max_velocity_forward < local_velocity_y && 0.0 < move_vec.y)
      {
        cgmath::Vector3::zero()
      } else {
        move_vec.y * xy_basis.y
      };
      // convert from m/s^2
      step_secs * step_secs * config.player_acceleration *
        ( move_x + move_y + move_vec.z * xy_basis.z)
    } else {
      cgmath::Vector3::zero()
    };
    let impulse = impulse_jump + impulse_move;
    if impulse.magnitude2() > 0.0 {
      let impulse_event = event::Input::ModifyObject (
        event::ObjectModify::Dynamic (
          object_key, event::ObjectModifyDynamic::ApplyImpulse (impulse))
      );
      out.push (StepResult::LinearSimEvent (impulse_event));
    } // end apply movement acceleration
    // end new acceleration

    // update attack state
    self.attack_state = if let Some (attack_state) = self.attack_state.take() {
      debug_assert!(attack_state.step_start <= step);
      debug_assert!(step <= attack_state.step_end);
      if attack_state.step_end == step {
        None
      } else {
        match attack_state.clone().attack_kind {
          AttackKind::Melee (kind) => {
            let attack_duration = attack_state.step_end
              - attack_state.step_start;
            debug_assert_eq!(self.attack_duration as u64, attack_duration);
            // skip 1/4th of the start and end of the attack as "warm up" and
            // "cool down" period where no damage is dealt
            let skip = attack_duration / 4;
            let damage_start = attack_state.step_start + skip;
            let damage_end = attack_state.step_end - skip;
            if step >= damage_start && step <= damage_end {
              let damage_step     = (step - damage_start) as f64;
              let damage_duration = (damage_end - damage_start) as f64;
              // normalized [0.0-1.0] value
              let hit_frac = geometry::math::Normalized::new (
                damage_step / damage_duration).unwrap();
              // compute hit segment for specific melee kind
              let position = {
                let mut eye = self.eye();
                eye.z -= 1.0/8.0 * self.class.data().height; // shoulder-height
                eye
              };
              let hit_segment = kind.hit_segment (&class_data.weapon,
                position, self.orientation.as_ref(), hit_frac);
              out.push (
                StepResult::Action (Action::MeleeAttack { kind, hit_segment }))
            }
          }
          _ => unimplemented!("TODO: missile and spell attacks")
        }
        Some (attack_state)
      }
    } else {
      None
    };

    out
  } // end do_step

  /// Modify yaw and clamp to $[0, 2\pi)$.
  #[inline]
  pub fn rotate_yaw (&mut self, delta_yaw : cgmath::Rad <f64>) {
    use std::f64::consts::PI;
    self.yaw += delta_yaw;
    if self.yaw < cgmath::Rad (0.0) {
      self.yaw += cgmath::Rad (2.0*PI);
    }
    if cgmath::Rad (2.0*PI) <= self.yaw {
      self.yaw -= cgmath::Rad (2.0*PI);
    }
  }

  /// Modify pitch and clamp to $[-pi/2,pi/2]$
  #[inline]
  pub fn rotate_pitch (&mut self, delta_pitch : cgmath::Rad <f64>) {
    use std::f64::consts::FRAC_PI_2;
    self.pitch += delta_pitch;
    if self.pitch < cgmath::Rad (-FRAC_PI_2) {
      self.pitch = cgmath::Rad (-FRAC_PI_2)
    }
    if cgmath::Rad (FRAC_PI_2) < self.pitch {
      self.pitch = cgmath::Rad (FRAC_PI_2)
    }
  }

  /// Computes orientation basis from `yaw` and `pitch`
  #[inline]
  pub fn compute_orientation (&mut self) {
    use cgmath::Rotation3;
    let heading = cgmath::Basis3::from_angle_z (-self.yaw);
    self.orientation =
      cgmath::Basis3::from_axis_angle (heading.as_ref()[0], self.pitch)
        * heading;
  }

  /// Report static player information (e.g. byte size).
  pub fn report_sizes() {
    println!("Player report sizes...");
    println!("  size of Player: {}", std::mem::size_of::<Player>());
    println!("...Player report sizes");
  }

  pub fn jump (&mut self) {
    // TODO: crouch jump
    match self.jump_state {
      JumpState::Standing => {
        self.jump_state = JumpState::Jumping;
        self.jump       = true;   // applies an impulse on player step
      }
      _ => {}
    }
  }

  pub fn attack (&mut self, step : u64, attack_kind : AttackKind) {
    if self.attack_state.is_none() {
      let step_start = step;
      let step_end   = step + self.attack_duration as u64;
      self.attack_state = Some (
        AttackState {
          attack_kind,
          step_start,
          step_end
        }
      );
    }
  }
} // end impl Player

//
//  impl MoveState
//
impl MoveState {
  /// Assigns left-right state to X-component, forward-backward state to
  /// Y-component, and up-down state to Z-component.
  pub fn to_vec (self) -> cgmath::Vector3 <f64> {
    cgmath::vec3 (
      self.right_left()        as f64,
      self.forward_backward()  as f64,
      self.up_down()           as f64)
  }

  /// Returns the signum (-1, 0, 1) of the forward/backward state where forward
  /// == 1 and backward == -1.
  #[inline]
  pub fn forward_backward (&self) -> i8 {
    self.forward_backward.signum()
  }
  /// Returns the signum (-1, 0, 1) of the right/left state where right == 1
  /// and left == -1.
  #[inline]
  pub fn right_left (&self) -> i8 {
    self.right_left.signum()
  }
  /// Returns the signum (-1, 0, 1) of the up/down state where up == 1 and down
  /// == -1.
  #[inline]
  pub fn up_down (&self) -> i8 {
    self.up_down.signum()
  }
  #[inline]
  pub fn move_forward (mut self) -> Self {
    self.forward_backward += 1;
    self
  }
  #[inline]
  pub fn move_backward (mut self) -> Self {
    self.forward_backward -= 1;
    self
  }
  #[inline]
  pub fn move_right (mut self) -> Self {
    self.right_left += 1;
    self
  }
  #[inline]
  pub fn move_left (mut self) -> Self {
    self.right_left -= 1;
    self
  }
  #[inline]
  pub fn move_up (mut self) -> Self {
    self.up_down += 1;
    self
  }
  #[inline]
  pub fn move_down (mut self) -> Self {
    self.up_down -= 1;
    self
  }
  /// Applies another `MoveState` as a "direction" to modify the current
  /// movestate
  #[inline]
  pub fn move_direction (&mut self, direction : MoveState) {
    self.forward_backward += direction.forward_backward;
    self.right_left       += direction.right_left;
    self.up_down          += direction.up_down;
  }
} // end impl MoveState

//
//  impl TurnState
//
impl TurnState {
  /// Returns the signum (-1, 0, 1) of the right/left state where right == 1
  /// and left == -1.
  #[inline]
  pub fn right_left (&self) -> i8 {
    self.right_left.signum()
  }
  /// Returns the signum (-1, 0, 1) of the up/down state where up == 1 and down
  /// == -1.
  #[inline]
  pub fn up_down (&self) -> i8 {
    self.up_down.signum()
  }

  #[inline]
  pub fn turn_right (mut self) -> Self {
    self.right_left += 1;
    self
  }
  #[inline]
  pub fn turn_left (mut self) -> Self {
    self.right_left -= 1;
    self
  }
  #[inline]
  pub fn turn_up (mut self) -> Self {
    self.up_down += 1;
    self
  }
  #[inline]
  pub fn turn_down (mut self) -> Self {
    self.up_down -= 1;
    self
  }
  /// Applies another `TurnState` as a "direction" to modify the current
  /// turnstate
  #[inline]
  pub fn turn_direction (&mut self, direction : TurnState) {
    self.right_left += direction.right_left;
    self.up_down    += direction.up_down;
  }
} // end impl TurnState

//
//  impl JumpState
//
impl Default for JumpState {
  fn default() -> Self {
    JumpState::Standing
  }
}

//
//  impl AttackKind
//
impl From <MeleeKind> for AttackKind {
  fn from (melee_kind : MeleeKind) -> Self {
    AttackKind::Melee (melee_kind)
  }
}

impl From <MissileKind> for AttackKind {
  fn from (missile_kind : MissileKind) -> Self {
    AttackKind::Missile (missile_kind)
  }
}

impl From <SpellKind> for AttackKind {
  fn from (spell_kind : SpellKind) -> Self {
    AttackKind::Spell (spell_kind)
  }
}

impl MeleeKind {
  pub fn hit_segment (&self,
    weapon   : &Melee,
    position : cgmath::Point3 <f64>,
    orient   : &cgmath::Matrix3 <f64>,
    fraction : geometry::math::Normalized <f64>
  ) -> geometry::Segment3 <f64> {
    use std::f64::consts::{FRAC_PI_3, FRAC_PI_4, FRAC_PI_6};
    use cgmath::{Rotation, Rotation3};
    let frac    = *fraction;
    let reach   = weapon.reach();
    let right   = &orient[0];
    let forward = &orient[1];
    let up      = &orient[2];
    let hitvec  = reach * match self {
      MeleeKind::Stab => {
        // [0.25, 1.0]
        let len = 0.25 + frac * 0.75;
        // increase reach by 20%
        1.2 * len * forward
      }
      MeleeKind::Chop => {
        // [pi/6, (5*pi)/6]
        let angle  = FRAC_PI_6 + frac * 2.0 * FRAC_PI_3;
        let rotate = cgmath::Quaternion::from_axis_angle (
          *right, cgmath::Rad (-angle));
        rotate.rotate_vector (*up)
      }
      MeleeKind::SlashHighLeft => {
        // [pi/6, (5*pi)/6]
        let angle  = FRAC_PI_6 + frac * 2.0 * FRAC_PI_3;
        let rotate = cgmath::Quaternion::from_axis_angle (
          *up, cgmath::Rad (angle));
        rotate.rotate_vector (*right)
      }
      MeleeKind::SlashHighRight => {
        // [pi/6, (5*pi)/6]
        let angle  = FRAC_PI_6 + frac * 2.0 * FRAC_PI_3;
        let rotate = cgmath::Quaternion::from_axis_angle (
          *up, cgmath::Rad (-angle));
        rotate.rotate_vector (-*right)
      }
      MeleeKind::SlashLowLeft => {
        // [pi/6, (5*pi)/6]
        let angle  = FRAC_PI_6 + frac * 2.0 * FRAC_PI_3;
        let tilt   = cgmath::Matrix3::from_axis_angle (
          *forward, cgmath::Rad (-FRAC_PI_4));
        let tilted = tilt * orient;
        let rotate = cgmath::Quaternion::from_axis_angle (
          tilted[2], cgmath::Rad (angle));
        rotate.rotate_vector (tilted[0])
      }
      MeleeKind::SlashLowRight => {
        // [pi/6, (5*pi)/6]
        let angle  = FRAC_PI_6 + frac * 2.0 * FRAC_PI_3;
        let tilt   = cgmath::Matrix3::from_axis_angle (
          *forward, cgmath::Rad (FRAC_PI_4));
        let tilted = tilt * orient;
        let rotate = cgmath::Quaternion::from_axis_angle (
          tilted[2], cgmath::Rad (-angle));
        rotate.rotate_vector (-tilted[0])
      }
    };
    geometry::Segment3::unchecked (position, position + hitvec)
  }
}
//
//  impl Id
//
impl std::ops::Deref for Id {
  type Target = u8;
  fn deref (&self) -> &u8 {
    &self.0
  }
}
impl From <Id> for usize {
  fn from (player_id : Id) -> Self {
    let Id (id) = player_id;
    id as usize
  }
}
// end impl Id

//
//  impl Melee
//
impl Melee {
  pub fn reach (&self) -> f64 {
    match self {
      Melee::HandToHand => 1.00,
      Melee::Staff      => 2.30,
      Melee::Dagger     => 1.35,
      Melee::ShortSword => 1.70,
      Melee::LongSword  => 2.05,
      Melee::GreatSword => 2.60,
      Melee::Axe        => 1.95,
      Melee::Hammer     => 1.90
    }
  }
}
// end impl Melee
