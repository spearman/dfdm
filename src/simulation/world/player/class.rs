// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use vec_map::VecMap;

use enum_unitary::enum_unitary;

use crate::simulation::world::player;

lazy_static! {
  static ref DATA : VecMap <Data> = {
    let mut v = VecMap::with_capacity (Class::count());
    {
      let class = Class::FighterLightM as usize;
      let data  = Data {
        height: 1.90,   // 1.90m
        width:  0.70,   // 0.70m
        mass:   80.0,   // 77kg
        weapon: player::Melee::GreatSword // 2.6m
      };
      debug_assert!(data.verify());
      assert!(v.insert (class, data).is_none());
    } {
      let class = Class::MageThiefF as usize;
      let data  = Data {
        height: 1.84,  // 1.84m
        width:  0.50,  // 0.50m
        mass:   60.0,  // 60kg
        weapon: player::Melee::Dagger     // 1.35m
      };
      debug_assert!(data.verify());
      assert!(v.insert (class, data).is_none());
    }

    // TODO: remaining classes

    v
  };
}

enum_unitary! {
  #[derive(Copy, Debug, PartialEq, Deserialize, Serialize)]
  pub enum Class {
    // TODO: re-order these once all textures are uploaded
    MageThiefF,
    FighterLightM,

    MageNormalF,
    MageNormalM,
    MageFighterF,
    MageFighterM,
    //MageThiefF,
    MageThiefM,
    FighterLightF,
    //FighterLightM,
    FighterMediumF,
    FighterMediumM,
    FighterHeavyF,
    FighterHeavyM,
    ThiefLightF,
    ThiefLightM,
    ThiefHeavyF,
    ThiefHeavyM
  }
}

/// Per-class data
pub struct Data {
  /// Standing-height: note this must be equal to or greater than the width
  /// since the minimum bounding volume of a player is a sphere of width
  /// diameter and width height
  pub height     : f64,
  /// Width used to calculate bounding cylinder radius: note this must be equal
  /// to or less than the height since the minimum bounding volume of a player
  /// is a sphere of width diameter and width height
  pub width      : f64,
  pub mass       : f64,
  // TODO: ranged weapons and spells
  pub weapon     : player::Melee
}

impl Class {
  pub fn data (&self) -> &'static Data {
    DATA.get (*self as usize).as_ref().unwrap()
  }
}

impl Data {
  /// Verifies that the data is valid:
  ///
  /// - `height >= width`
  pub fn verify (&self) -> bool {
    self.height >= self.width
  }
}
