// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

use crate::simulation::{Config, Command, World};

#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct History {
  pub tracebuff     : Vec <(u64, World, Vec <Command>)>,
  pub oldest_index  : usize,
  pub current_index : usize,
  pub size          : usize,
  /// How many steps to clear in front of the current step to collect future
  /// commands
  pub future_steps  : usize
}

impl History {
  pub fn new (
    size         : usize,
    future_steps : usize,
    init         : (u64, World, Vec <Command>)
  ) -> Self {
    let (step, world, commands) = init;
    let tracebuff    = {
      let mut tracebuff = Vec::with_capacity (size);
      tracebuff.push ((step, world.clone(), commands));
      for i in 1..size as u64 {
        tracebuff.push ((step+i, world.clone(), Vec::new()));
      }
      debug_assert_eq!(tracebuff.len(), size);
      tracebuff
    };
    let oldest_index  = 0;
    let current_index = 0;
    History { tracebuff, oldest_index, current_index, size, future_steps }
  }

  #[inline]
  pub fn current_step (&self) -> u64 {
    self.tracebuff[self.current_index].0
  }
  #[inline]
  pub fn oldest_step (&self) -> u64 {
    self.tracebuff[self.oldest_index].0
  }
  #[inline]
  pub fn current (&self) -> &(u64, World, Vec <Command>) {
    &self.tracebuff[self.current_index]
  }
  #[inline]
  pub fn current_mut (&mut self) -> &mut (u64, World, Vec <Command>) {
    &mut self.tracebuff[self.current_index]
  }
  #[inline]
  pub fn oldest (&self) -> &(u64, World, Vec <Command>) {
    &self.tracebuff[self.oldest_index]
  }
  #[inline]
  pub fn oldest_mut (&mut self) -> &mut (u64, World, Vec <Command>) {
    &mut self.tracebuff[self.oldest_index]
  }
  pub fn get_step (&self, step : u64) -> Option <&(u64, World, Vec <Command>)> {
    let step_current = self.current_step();
    let step_oldest  = self.oldest_step();
    if step < step_oldest || step_current < step {
      return None
    }
    let offset = (step_current - step) as usize;
    debug_assert!(offset < self.size);
    let index  = (self.current_index + (self.size - offset)) % self.size;
    Some (&self.tracebuff[index])
  }
  pub fn get_step_mut (&mut self, step : u64)
    -> Option <&mut (u64, World, Vec <Command>)>
  {
    let step_current = self.current_step();
    let step_oldest  = self.oldest_step();
    let future_steps = self.future_steps as u64;
    if step < step_oldest || step_current + future_steps < step {
      return None
    }
    let offset = step_current as isize - step as isize;
    debug_assert!(offset < self.size as isize);
    let index  = (self.current_index +
      (self.size as isize - offset) as usize) % self.size;
    Some (&mut self.tracebuff[index])
  }
  pub fn step (&mut self, config : &Config) {
    self.do_step (self.current_step(), config);
    debug_assert!((self.current_step() - self.oldest_step()) as usize
      <= self.size - self.future_steps - 1);
    if (self.current_step() - self.oldest_step()) as usize
      == self.size - self.future_steps - 1
    {
      {
        let next_step    = self.current_step() + 1;
        let future_steps = self.future_steps as u64;
        let (ref mut step, _, ref mut commands) = self.oldest_mut();
        *step = next_step + future_steps;
        commands.clear();
      }
      self.oldest_index = (self.oldest_index + 1) % self.size;
    }
    self.current_index = (self.current_index + 1) % self.size;
  }
  pub fn insert_command (&mut self,
    step : u64, command : Command, config : &Config
  ) {
    debug_assert!(step <= self.current_step() + self.future_steps as u64);
    debug_assert!(self.oldest_step() <= step);
    let (_, _, ref mut commands) = self.get_step_mut (step).unwrap();
    commands.push (command);
    // re-synchronize world state up to current step
    if step < self.current_step() {
      for i in step..self.current_step() {
        self.do_step (i, config);
      }
    }
  }
  fn do_step (&mut self, step : u64, config : &Config) {
    let next = {
      let (step, world, commands) = self.get_step (step).unwrap();
      let mut next = world.clone();
      next.step (*step, &commands, config);
      next
    };
    let (_, ref mut world, _) = self.get_step_mut (step+1).unwrap();
    *world = next;
  }
}
