// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

pub const STATIC_CONFIG_YAML_PATH : &str = "data/server-config.yaml";

/// Static configuration to be loaded from `STATIC_CONFIG_YAML_PATH`
#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  /// Trace, Debug, Info, Warn, Error
  pub log_level            : String,
  /// The address for the ENet host (e.g. "127.0.0.1" if testing on localhost)
  pub hostname             : String,
  /// UDP port (should typically be in the range 1025-49151)
  pub port                 : u16,
  pub players_max          :  u8,
  pub spectators_max       :  u8,
  /// Update average ping every nth ping
  pub ping_rtt_window_size : u16,
  /// Max allowed client ping
  pub max_ping_ms          : u16,
  pub step_ms              : u16,
  pub tick_ms              : u16,
  /// Print system debug info per ms
  pub debug_ms             : u64,
  /// Print world state per steps
  pub debug_step           : u64,
  pub history_size         : usize,
  pub history_future_steps : usize
}
