// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use std::time;
use vec_map::VecMap;

use macro_machines::def_machine;

use dfdm::{client, server, simulation, Simulation, VERSION};
use dfdm::simulation::world::player;

use crate::{
  Timer,
  CONNECTIONS_MAX,
  ENET_ADDRESS,
  STATIC_CONFIG
};

def_machine! {
  Server (
    enet_host            : enet::Host = {
      let enet = enet::initialize().unwrap();
      enet.server_host_create (
        ENET_ADDRESS.clone(),
        // allow 1 extra connection to allow replying with "server full" to
        // further connection attempts
        *CONNECTIONS_MAX+1,
        Some (2),   // two channels, 0 and 1
        None, None
      ).unwrap()
    },
    timer                : Timer      =
      Timer::start (STATIC_CONFIG.tick_ms, STATIC_CONFIG.step_ms),
    simulation           : Simulation,
    main_iter            : u64,
    connection_count     : u64,
    player_clients       : VecMap <Client>,
    spectator_clients    : VecMap <Client>,
    peer_id_to_client_id : VecMap <server::ClientId>
  ) @ server {
    STATES [
      state Waiting ()
      state Running ()
    ]
    EVENTS [
      event Start <Waiting> => <Running> ()
    ]
    initial_state: Waiting
  }
}

/// Server-side representation of a client
#[derive(Debug)]
pub struct Client {
  pub enet_peer           : enet::Peer,
  pub status              : ClientStatus,
  pub ping_count          : u64,
  /// Client timestamp of the last ping (sequence number ping_count-1)
  pub ping_last_client_ts : u64,
  pub ping_rtt_sum        : u64,
  /// Average round trip time in milliseconds
  pub ping_rtt_avg        : u16,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum ClientStatus {
  Connecting,
  Connected
}

impl Server {
  pub fn tick (&mut self) {
    log::trace!("tick...");
    let t_start = time::Instant::now();
    let mut tick_ms = self.as_ref().timer.tick_ms as u32;
    // service loop
    while let Some (event) = self.as_mut().enet_host.service (tick_ms).unwrap() {
      tick_ms = 0;
      log::trace!("server received event:\n{:#?}", event);
      match event {
        enet::Event::Connect { mut peer, .. } => {
          log::info!("connection attempt from peer: {:?}", peer);
          match self.connect (peer.clone()) {
            Ok  (_client_id) => {
              self.as_mut().connection_count += 1;
              let ping_rtt_window_size = STATIC_CONFIG.ping_rtt_window_size;
              peer.send (0, enet::Packet::Allocate {
                bytes: bincode::serialize (
                  &server::Message::ConnectionEstablished {
                    ping_rtt_window_size,
                    version: VERSION
                  }
                ).unwrap().as_slice(),
                flags: enet::packet::Flags::RELIABLE
              }).unwrap();
              // if this is the first connection, set the next step time
              // to start simulating
              if self.as_ref().connection_count == 1 {
                let config = self.as_ref().simulation.config.clone();
                self.as_mut().simulation.history = simulation::History::new (
                  STATIC_CONFIG.history_size, STATIC_CONFIG.history_future_steps,
                  (0, simulation::World::create (&config), Vec::new()));
                debug_assert_eq!(0, self.as_ref().simulation.step_current());
                self.as_mut().timer.step         = self.as_ref().simulation
                  .step_current();
                self.as_mut().timer.step_next_ts = self.as_ref().timer
                  .timestamp_now();
              }
            }
            Err (refused) => {
              peer.send (0, enet::Packet::Allocate {
                bytes: bincode::serialize (
                  &server::Message::ConnectionRefused (refused)
                ).unwrap().as_slice(),
                flags: enet::packet::Flags::RELIABLE
              }).unwrap();
              peer.disconnect()
            }
          }
        },
        enet::Event::Disconnect { peer, .. } => {
          log::info!("peer disconnected: {:?}", peer);
          self.disconnect (peer)
        }
        enet::Event::Receive    { mut peer, packet, ..} => {
          let peer_id = peer.incoming_peer_id() as usize;
          let message : client::Message = bincode::deserialize (packet.data())
            .unwrap();
          log::debug!("received message from peer [{}]: {:#?}", peer_id, message);
          let _timestamp_now = self.as_ref().timer.timestamp_now();
          let client_id     = self.as_ref().peer_id_to_client_id[peer_id];
          let disconnect = match message {

            //
            //  client::Message::Ping
            //
            client::Message::Ping {
              sequence, step, ping_timestamp, last_pong_timestamp
            } => {
              let ext_state = self.as_mut();
              let (player_clients, simulation) = (
                &mut ext_state.player_clients,
                &ext_state.simulation);
              let client = match client_id {
                server::ClientId::Player (id)    =>
                  &mut player_clients[id.0 as usize],
                server::ClientId::Spectator (_n) =>
                  unimplemented!("TODO: spectator ping")
              };
              log::info!("{:?} Ping[{}] client step[{}] rtt avg: {}",
                client_id, sequence, step, client.ping_rtt_avg);
              debug_assert_eq!(client.peer(),     &peer);
              debug_assert_eq!(client.ping_count, sequence);
              // pong
              let step = ext_state.simulation.step_current();
              peer.send (0, enet::Packet::Allocate {
                bytes: bincode::serialize (
                  &server::Message::Pong { sequence, step }
                ).unwrap().as_slice(),
                flags: enet::packet::Flags::RELIABLE
              }).unwrap();
              client.ping_count += 1;
              if last_pong_timestamp > 0 {
                debug_assert!(client.ping_last_client_ts > 0);
                debug_assert!(last_pong_timestamp > client.ping_last_client_ts);
                let rtt_ns = last_pong_timestamp - client.ping_last_client_ts;
                let rtt_ms = rtt_ns / 1_000_000;
                client.ping_rtt_sum += rtt_ms;
                client.ping_last_client_ts = ping_timestamp;
                if client.ping_count%STATIC_CONFIG.ping_rtt_window_size as u64
                  == 0
                {
                  client.ping_rtt_avg = (
                    client.ping_rtt_sum/STATIC_CONFIG.ping_rtt_window_size as u64
                  ) as u16;
                  client.ping_rtt_sum = 0;

                  // update client status based on new ping_rtt_avg
                  match client.status {
                    ClientStatus::Connecting => {
                      if client.ping_rtt_avg > STATIC_CONFIG.max_ping_ms {
                        // ping too high
                        let refused = server::ConnectionRefused::PingTooHigh (
                          client.ping_rtt_avg,
                          STATIC_CONFIG.max_ping_ms);
                        peer.send (0, enet::Packet::Allocate {
                          bytes: bincode::serialize (
                            &server::Message::ConnectionRefused (refused)
                          ).unwrap().as_slice(),
                          flags: enet::packet::Flags::RELIABLE
                        }).unwrap();
                        true
                      } else {
                        // send initial simulation state
                        log::info!("connection accepted synchronizing peer: {:?}",
                          peer);
                        let simulation = simulation.clone();
                        let message    = server::Message::ConnectionAccepted (
                          client_id, simulation);
                        peer.send (0, enet::Packet::Allocate {
                          bytes: bincode::serialize (&message).unwrap().as_slice(),
                          flags: enet::packet::Flags::RELIABLE
                        }).unwrap();
                        client.status = ClientStatus::Connected;
                        false
                      }
                    }
                    ClientStatus::Connected => {
                      if client.ping_rtt_avg > STATIC_CONFIG.max_ping_ms {
                        // ping too high
                        let kicked = server::Kicked::PingTooHigh (
                          client.ping_rtt_avg, STATIC_CONFIG.max_ping_ms);
                        peer.send (0, enet::Packet::Allocate {
                          bytes: bincode::serialize (
                            &server::Message::Kicked (kicked)
                          ).unwrap().as_slice(),
                          flags: enet::packet::Flags::RELIABLE
                        }).unwrap();
                        true
                      } else {
                        // TODO: send checksum ?
                        false
                      }
                    }
                  } // end match client status
                } else {
                  false
                }
              } else {
                // first ping: no rtt to compute
                client.ping_last_client_ts = ping_timestamp;
                false
              }
            } // end ping message

            //
            //  client::Message::ScheduleCommand
            //
            client::Message::ScheduleCommand (step, command) => {
              // NB: unwrapping here because scheduled commands should only come
              // from players, not spectators
              let player_id  = client_id.player_id().unwrap();
              let disconnect = match &command {
                simulation::Command::Player (id, command) => {
                  debug_assert_eq!(*id, player_id);
                  match command {
                    player::Command::Quit => true,
                    _                     => false
                  }
                }
              };
              let player_index = player_id.0 as usize;
              let msg = server::Message::PeerCommand (step, command.clone());
              for (id, client) in self.as_mut().player_clients.iter_mut() {
                if id == player_index
                  || client.status == ClientStatus::Connecting
                { continue }
                client.peer_mut().send (0, enet::Packet::Allocate {
                  bytes: bincode::serialize (&msg).unwrap().as_slice(),
                  flags: enet::packet::Flags::RELIABLE
                }).unwrap();
              }
              for (_id, _client) in self.as_ref().spectator_clients.iter() {
                println!("TODO: forward commands to spectators");
              }
              self.as_mut().simulation.schedule_command (step, command);
              disconnect
            }

          }; // end match message
          if disconnect {
            peer.disconnect();
          }
        } // end receive event
      } // end match event
    } // end service loop
    let t_elapsed = t_start.elapsed();
    log::trace!("tick elapsed: {:?}", t_elapsed);
    self.as_mut().timer.tick();
    log::trace!("...tick");
  }

  pub fn step (&mut self) {
    log::trace!("step...");
    let ext_state = self.as_mut();
    let (
      connection_count,
      timer,
      simulation
    ) = (
      &ext_state.connection_count,
      &mut ext_state.timer,
      &mut ext_state.simulation
    );
    if *connection_count > 0 {
      let ts_now = timer.timestamp_now();
      while timer.step_next_ts <= ts_now {
        let t_start = std::time::Instant::now();
        simulation.step();
        let t_elapsed = t_start.elapsed().as_nanos() as u64;
        timer.step_average_ns = if timer.step_average_ns > 0 {
          (timer.step_average_ns + t_elapsed) / 2
        } else {
          t_elapsed
        };
        timer.step();
        log::trace!("step elapsed time: {:?}", t_elapsed);

        if cfg!(debug_assertions) {
          let step = simulation.step_current();
          if step % STATIC_CONFIG.debug_step == 0 {
            log::debug!("Server simulation step [{}]: {:#?}",
              step, simulation.world_current());
            debug_assert_eq!(step, simulation.world_current().step);
          }
        }
      }
    }
    log::trace!("...step");
  }

  fn connect (&mut self, peer : enet::Peer)
    -> Result <server::ClientId, server::ConnectionRefused>
  {
    let peer_index = peer.incoming_peer_id() as usize;
    debug_assert!(self.as_ref().peer_id_to_client_id.get (peer_index).is_none());
    for id in 0..STATIC_CONFIG.players_max as usize {
      if self.as_ref().player_clients.get (id).is_none() {
        let player_id = player::Id (id as u8);
        let client_id = server::ClientId::Player (player_id);
        assert!(self.as_mut().peer_id_to_client_id.insert (peer_index, client_id)
          .is_none());
        assert!(self.as_mut().player_clients.insert (id, Client::new (peer))
          .is_none());
        return Ok (client_id)
      }
    }
    println!("TODO: connect spectator");
    Err (server::ConnectionRefused::ServerFull)
  }

  fn disconnect (&mut self, peer : enet::Peer) {
    let peer_id = peer.incoming_peer_id() as usize;
    match self.as_ref().peer_id_to_client_id[peer_id] {
      server::ClientId::Player (id) => {
        assert!(self.as_mut().player_clients.remove (id.0 as usize).is_some());
      }
      server::ClientId::Spectator (_n) => {
        println!("TODO: disconnect spectator");
      }
    }
    assert!(self.as_mut().peer_id_to_client_id.remove (peer_id).is_some());
    peer.disconnect();
    self.as_mut().connection_count -= 1;
    if self.as_ref().connection_count == 0 {
      // reset the simulation when connection count drops to zero
      self.as_mut().simulation = Simulation {
        // NB: timer should be re-started when a player connects, but we set the
        // step_ms here just for consistency
        timer: simulation::Timer::start (0, self.as_ref().timer.step_ms),
        .. Simulation::default()
      };
    }
  }
}

impl Client {
  #[inline]
  fn new (enet_peer : enet::Peer) -> Self {
    let status              = ClientStatus::Connecting;
    let ping_count          = 0;
    let ping_rtt_avg        = 0;
    let ping_rtt_sum        = 0;
    let ping_last_client_ts = 0;
    Client {
      enet_peer, status, ping_count, ping_last_client_ts, ping_rtt_avg,
      ping_rtt_sum
    }
  }
  #[inline]
  pub fn peer (&self) -> &enet::Peer {
    &self.enet_peer
  }
  #[inline]
  pub fn peer_mut (&mut self) -> &mut enet::Peer {
    &mut self.enet_peer
  }
}

impl Default for ClientStatus {
  fn default() -> Self {
    ClientStatus::Connecting
  }
}
