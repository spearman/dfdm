// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

#![warn(unused_extern_crates)]
#![feature(const_fn)]
#![feature(core_intrinsics)]

use std::time;

use lazy_static::lazy_static;
use log;
use colored;
use enet;
use getopts;
use serde_yaml;
use simplelog;

use macro_machines;

use dfdm;

mod config;
mod program;
mod timer;

pub (crate) use timer::Timer;
use program::*;

lazy_static!{
  pub static ref STATIC_CONFIG    : config::StaticConfig = {
    let config_file = std::fs::File::open (config::STATIC_CONFIG_YAML_PATH)
      .unwrap();
    serde_yaml::from_reader::<std::fs::File, config::StaticConfig> (config_file)
      .unwrap()
  };
  pub static ref CMDLINE_OPTIONS  : getopts::Options = {
    let mut opts = getopts::Options::new();
    opts.optopt ("l", "log_level", "Log level filter", "LOG_LEVEL");
    opts.optopt ("h", "hostname", "Server address", "HOSTNAME");
    opts.optopt ("p", "port", "UDP port number", "PORT");
    opts
  };
  pub static ref CMDLINE_OPTION_MATCHES : getopts::Matches = {
    let args : Vec <String> = std::env::args().collect();
    CMDLINE_OPTIONS.parse (&args[1..]).unwrap()
  };
  //  Off, Error, Warn, Info, Debug, Trace
  pub static ref LOG_LEVEL_FILTER : simplelog::LevelFilter = {
    use std::str::FromStr;
    let log_level = CMDLINE_OPTION_MATCHES.opt_str ("l")
      .unwrap_or (STATIC_CONFIG.log_level.clone());
    simplelog::LevelFilter::from_str (log_level.as_str()).unwrap()
  };
  pub static ref ENET_ADDRESS     : enet::Address = {
    let hostname = CMDLINE_OPTION_MATCHES.opt_str ("h")
      .unwrap_or (STATIC_CONFIG.hostname.clone());
    let port     = CMDLINE_OPTION_MATCHES.opt_str ("p")
      .map (|p| u16::from_str_radix (p.as_str(), 10).unwrap())
      .unwrap_or (STATIC_CONFIG.port);
    enet::Address::with_hostname (hostname.as_str(), port).unwrap()
  };
  pub static ref DEBUG_DUR        : time::Duration =
    time::Duration::from_millis (STATIC_CONFIG.debug_ms);
  pub static ref CONNECTIONS_MAX  : u32 =
    STATIC_CONFIG.players_max as u32 + STATIC_CONFIG.spectators_max as u32;
}

pub fn report_sizes() {
  println!("server report sizes...");
  Server::report_sizes();
  println!("...server report sizes");
}

////////////////////////////////////////////////////////////////////////////////
//  main                                                                      //
////////////////////////////////////////////////////////////////////////////////

fn main() {
  use std::io::Write;
  use colored::Colorize;
  use macro_machines::MachineDotfile;

  let current_exe = std::env::current_exe().unwrap();
  println!("{}", format!("{:?} main...", current_exe).green().bold());

  rs_utils::app::init_simple_termlogger (*LOG_LEVEL_FILTER);

  // create a dotfile for the program state machine
  if std::env::var ("DFDM_DOTFILES").is_ok() {
    let mut f = std::fs::File::create ("server.dot").unwrap();
    f.write_all (Server::dotfile().as_bytes()).unwrap();
    drop (f);
  }

  // show some information about the program
  report_sizes();
  dfdm::simulation::report_sizes();
  // debug: show the static configuration
  log::debug!("server::STATIC_CONFIG: {}",
    serde_yaml::to_string (&*STATIC_CONFIG).unwrap());

  // create the server in the initial state
  let mut server = Server::initial();
  //log::debug!("server_program: {:#?}", server_program);
  //server.run();

  // main loop
  let mut _running = true;
  let mut t_debug = time::Instant::now();
  while _running {
    log::trace!("main loop iter [{}]", server.as_ref().main_iter);
    let t_start = time::Instant::now();
    server.tick();
    server.step();
    let t_after   = time::Instant::now();
    let t_elapsed = t_after.duration_since (t_start);
    if t_after > t_debug {
      debug (&server);
      t_debug = t_after + *DEBUG_DUR;
    }
    log::trace!("main iter elapsed: {:?}", t_elapsed);
    server.as_mut().main_iter += 1;
  }

  println!("{}", format!("...{:?} main", current_exe).green().bold());
}

fn debug (server : &Server) {
  log::debug!("debug...");
  let timer = &server.as_ref().timer;
  log::info!("Server tick[{}], step[{}]", timer.tick, timer.step);
  //log::debug!("Server timer: {:#?}", server.as_ref().timer);
  log::debug!("Server players: {:#?}", server.as_ref().player_clients);
  log::debug!("Server spectators: {:#?}", server.as_ref().spectator_clients);
  log::debug!("...debug");
}
