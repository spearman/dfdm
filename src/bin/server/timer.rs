// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use {std};

/// A timestamp measured as a nanosecond duration since the base initialization
/// time (the `t_base` field of the `Timer` struct)
#[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
pub struct TimestampNs (u64);

#[derive(Debug)]
pub struct Timer {
  /// Initialization time
  pub t_base           : std::time::Instant,
  pub tick             : u64,
  /// Input and packet service interval
  pub tick_ms          : u16,
  /// Input and packet service interval
  pub tick_ns          : u64,
  pub tick_last_ts     : TimestampNs,
  /// Average tick *duration* (not time between ticks)
  pub tick_average_ns  : u64,
  pub step             : u64,
  /// Simulation step interval
  pub step_ms          : u16,
  /// Simulation step interval
  pub step_ns          : u64,
  pub step_next_ts     : TimestampNs,
  /// Average step *duration* (not time between steps)
  pub step_average_ns  : u64,
}

impl Timer {
  pub fn start (tick_ms : u16, step_ms : u16) -> Self {
    let t_base           = std::time::Instant::now();
    let tick             = 0;
    let tick_ns          = 1_000_000 * tick_ms  as u64;
    let tick_last_ts     = TimestampNs (0);
    let tick_average_ns  = 0;
    let step             = 0;
    let step_ns          = 1_000_000 * step_ms  as u64;
    let step_next_ts     = TimestampNs (0);
    let step_average_ns  = 0;
    Timer {
      t_base,
      tick,            step,
      tick_ms,         step_ms,
      tick_ns,         step_ns,
      tick_last_ts,    step_next_ts,
      tick_average_ns, step_average_ns
    }
  }

  #[inline]
  pub fn timestamp_now (&self) -> TimestampNs {
    let t_now = self.t_base.elapsed().as_nanos();
    debug_assert!(t_now <= std::u64::MAX as u128);
    TimestampNs (t_now as u64)
  }

  #[inline]
  pub fn tick (&mut self) {
    self.tick_last_ts = self.timestamp_now();
    self.tick += 1;
  }

  #[inline]
  pub fn step (&mut self) {
    self.step_next_ts += self.step_ns;
    self.step += 1;
  }

  /*
  pub fn set_step (&mut self,
    step         : u64,
    step_ms      : u16,
    step_next_ts : TimestampNs
  ) {
    self.step            = step;
    self.step_ms         = step_ms;
    self.step_ns         = 1_000_000 * step_ms as u64;
    self.step_next_ts    = step_next_ts;
    self.step_average_ns = 0;
  }
  */
}

impl std::ops::Deref for TimestampNs {
  type Target = u64;
  fn deref (&self) -> &u64 {
    &self.0
  }
}
impl std::ops::DerefMut for TimestampNs {
  fn deref_mut (&mut self) -> &mut u64 {
    &mut self.0
  }
}
impl std::ops::Add <u64> for TimestampNs {
  type Output = Self;
  fn add (self, rhs : u64) -> Self {
    TimestampNs (*self + rhs)
  }
}
impl std::ops::AddAssign <u64> for TimestampNs {
  fn add_assign (&mut self, rhs : u64) {
    self.0 += rhs;
  }
}
impl std::ops::Sub <u64> for TimestampNs {
  type Output = Self;
  fn sub (self, rhs : u64) -> Self {
    TimestampNs (*self - rhs)
  }
}
