// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use {std, cgmath, collision, rfmod};
use glium::{self, glutin, uniform};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use vec_map::VecMap;

use {apis, gl_utils};
use enum_unitary::enum_unitary;
use fmod_utils::rfmod_ok;

use crate::thread;

////////////////////////////////////////////////////////////////////////////////
//  statics                                                                   //
////////////////////////////////////////////////////////////////////////////////

lazy_static!{
  pub static ref STATIC_CONFIG    : &'static StaticConfig =
    &crate::STATIC_CONFIG.mode_config.title_menu_config;
  pub static ref STATIC_INPUT_MAP : VecMap <InputFun>     = {
    let mut v = VecMap::new();
    for (keycode, input_fun) in STATIC_CONFIG.input_map.iter() {
      assert!(v.insert (*keycode as usize, *input_fun).is_none());
    }
    v
  };
}

////////////////////////////////////////////////////////////////////////////////
//  session                                                                   //
////////////////////////////////////////////////////////////////////////////////

//
//  mode TitleMenu
//
apis::def_session! {
  context TitleMenu {
    PROCESSES where
      let title_menu = self,
      let message_in = message_in
    [
      process MainThread (
        main_context : Option <thread::Main> = None,
        input_map    : VecMap <InputFun>     = STATIC_INPUT_MAP.clone()
      ) -> (TitleMenuResult) {
        kind           {
          apis::process::Kind::Mesochronous {
            tick_ms:          *thread::main::INPUT_MS,
            ticks_per_update: 1
          }
        }
        sourcepoints   [MainToAv]
        endpoints      [ ]
        handle_message { unreachable!() }
        update         { title_menu.main_update() }
      }
      process AvThread (
        av_context          : Option <Box <thread::Av>> = None,
        rfmod_channel_fire  : Option <rfmod::Channel>   = None,
        rfmod_channel_music : Option <rfmod::Channel>   = None,
        frame               : u64  = 0,
        // free audio resources when terminating
        reset_audio_on_term : bool = true
      ) {
        kind           { apis::process::Kind::Anisochronous }
        sourcepoints   [ ]
        endpoints      [MainToAv]
        initialize     { title_menu.init() }
        terminate      { title_menu.term() }
        handle_message { title_menu.av_handle_message (message_in) }
        update         { title_menu.av_update() }
      }
    ]
    CHANNELS [
      channel MainToAv <MainToAvMsg> (Simplex) {
        producers [MainThread]
        consumers [AvThread]
      }
    ]
    MESSAGES [
      message MainToAvMsg {
        Default (thread::MainToAvMsg),
        MainMenuActive { option : MainMenuOption }
      }
    ]
    main: MainThread
  }
} // end mode TitleMenu

////////////////////////////////////////////////////////////////////////////////
//  input functions                                                           //
////////////////////////////////////////////////////////////////////////////////

impl_input_fun! {
  MainThread {
    MainThread::screenshot,       Screenshot
    MainThread::quit_to_system,   QuitToSystem
    MainThread::main_menu_select, MainMenuSelect
    MainThread::main_menu_next,   MainMenuNext
    MainThread::main_menu_prev,   MainMenuPrev
    MainThread::main_menu_first,  MainMenuFirst
    MainThread::main_menu_last,   MainMenuLast
  }
}

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  pub input_map : std::collections::HashMap
    <thread::main::input::keyboard::Keycode, InputFun>
}

////////////////////////////////////////////////////////////////////////////////
//  enums                                                                     //
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone,Debug,Eq,PartialEq)]
pub enum TitleMenuResult {
  StartPractice,
  StartMultiSetup,
  QuitToSystem
}

enum_unitary! {
  #[derive(Copy, Debug, Eq, PartialEq)]
  pub enum MainMenuOption {
    Practice,
    Multiplayer,
    Settings,
    Exit
  }
}

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

//  fn main_menu_vertices
/// Return vertices from the button bounds returned by
/// `main_menu_button_bounds`.
pub fn main_menu_button_vertices (
  button_dimensions : cgmath::Vector2 <u16>,
  screen_resolution : cgmath::Vector2 <u16>,
  gui_scale         : u8,
  active_option     : MainMenuOption
) -> [thread::av::render::shader::Vert2dSpriteArrayColorize; 4] {
  use cgmath::ElementWise;

  let colorize_color = [1.0, 0.75, 0.0];   // color active vertex golden yellow
  let button_bounds  =
    main_menu_button_bounds (screen_resolution, gui_scale, button_dimensions);
  let button_dimensions_ndc : cgmath::Vector2 <f32> =
    (2.0 * gui_scale as f32 * button_dimensions.cast().unwrap())
      .div_element_wise (screen_resolution.cast().unwrap());
  let vertex_n = |n : usize| {
    use collision::Aabb;
    thread::av::render::shader::Vert2dSpriteArrayColorize {
      in_position:        gl_utils::camera2d::screen_2d_to_ndc_2d (
        screen_resolution,
        button_bounds[n].center()).into(),
      in_dimensions:      button_dimensions_ndc.into(),
      in_texture_index:   n as u32,
      in_colorize_flag:   (n == active_option as usize) as u32,
      in_colorize_color:  colorize_color
    }
  };

  // array of four vertices
  [ vertex_n (0), vertex_n (1), vertex_n (2), vertex_n (3) ]

} // end fn main_menu_vertices

// fn main menu cursor vertex
pub fn main_menu_cursor_vertex (
  cursor_dimensions : cgmath::Vector2 <u16>,
  screen_resolution : cgmath::Vector2 <u16>,
  gui_scale         : u8,
  active_option     : MainMenuOption,
  button_vertices   :
    &[thread::av::render::shader::Vert2dSpriteArrayColorize; 4]
) -> [thread::av::render::shader::Vert2dSprite; 1] {
  let cursor_gap_px      = gui_scale as i16; // 1-pixel gap (scaled)
  let cursor_width_px    = cursor_dimensions.x as i16 * gui_scale as i16;
  let cursor_height_px   = cursor_dimensions.y as i16 * gui_scale as i16;
  let button_position_px = gl_utils::camera2d::ndc_2d_to_screen_2d (
    screen_resolution,
    button_vertices[active_option as usize].in_position.into());
  let button_width_px    = ((button_vertices[active_option as usize]
    .in_dimensions[0]) * 0.5 * screen_resolution.x as f32) as i16;

  [thread::av::render::shader::Vert2dSprite {
    in_position:   [
      (button_position_px.x - (button_width_px/2) - (cursor_width_px/2)
        - cursor_gap_px) as f32,
      button_position_px.y as f32],
    in_dimensions: [cursor_width_px as f32, cursor_height_px as f32]
  }]
}

//  fn main_menu_button_bounds
/// This function gives vertices for the main menu buttons.
///
/// The menu should be aligned horizontally with the center of the screen and
/// vertically centered on the point -1/3 (NDC coordinates):
///
/// ```text
/// [ PRACTICE    ]
///
/// [ MULTIPLAYER ]
///        * -------- (0, -1/3)
/// [ SETTINGS    ]
///
/// [ EXIT        ]
/// ```

pub fn main_menu_button_bounds (
  screen_resolution : cgmath::Vector2 <u16>,
  gui_scale         : u8,
  button_dimensions : cgmath::Vector2 <u16>
) -> [collision::Aabb2 <i16>; 4] {
  use cgmath::ElementWise;

  let vertical_base_ndc     = -0.55;
  let separation_height_px  = 4.0;
  let gui_scale             = gui_scale as f32;
  // to compute the ndc sizes below there is a factor of 2.0 since the screen
  // bounds are from -1.0 to 1.0
  let separation_height_ndc =
    (2.0 * gui_scale * separation_height_px) / screen_resolution.y as f32;
  let button_dimensions_ndc : cgmath::Vector2 <f32> =
    (2.0 * gui_scale * button_dimensions.cast().unwrap())
      .div_element_wise (screen_resolution.cast().unwrap());
  let button_dimensions_ndc_half = 0.5 * button_dimensions_ndc;
  let button0_vertical_center    = vertical_base_ndc
    + 1.5 * separation_height_ndc + 1.5 * button_dimensions_ndc.y;
  let bounds_n = |n : usize| {
    use cgmath::EuclideanSpace;
    let center = cgmath::vec2 (
      0.02,
      button0_vertical_center
        - (n as f32 * (separation_height_ndc + button_dimensions_ndc.y))
    );
    collision::Aabb2::new (
      gl_utils::camera2d::ndc_2d_to_screen_2d (
        screen_resolution,
        cgmath::Point2::from_vec (center - button_dimensions_ndc_half)),
      gl_utils::camera2d::ndc_2d_to_screen_2d (
        screen_resolution,
        cgmath::Point2::from_vec (center + button_dimensions_ndc_half)))
  };

  [ bounds_n (0), bounds_n (1), bounds_n (2), bounds_n (3) ]

} // end fn main_menu_button_bounds

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

impl MainThread {
  /*
  pub fn init (&mut self) {
    //let gui_context = &self.main_context.as_ref().unwrap().gui;
  }
  */

  //
  //  process behavior methods
  //

  fn main_update (&mut self) -> apis::process::ControlFlow {
    use colored::Colorize;
    use glutin::platform::desktop::EventLoopExtDesktop;
    log::trace!("MainThread update...");
    let mut control_flow = apis::process::ControlFlow::Continue;

    // glutin poll input events
    let mut event_loop = self.main_context.as_mut().unwrap().input.event_loop
      .take().unwrap();
    event_loop.run_return (|event, _, event_loop_control_flow| {
      *event_loop_control_flow = glutin::event_loop::ControlFlow::Poll;
      use glutin::event::{self, Event};
      log::trace!("{}: {:?}", "glutin event".to_string().cyan().bold(), event);
      match event {
        Event::MainEventsCleared => {
          *event_loop_control_flow = glutin::event_loop::ControlFlow::Exit;
        }
        Event::WindowEvent { event, .. } => match event {
          // closed
          event::WindowEvent::CloseRequested |
          event::WindowEvent::Destroyed      => {
            log::info!("glutin WindowEvent::Closed: quitting to system...");
            // always returns Some(Break)
            control_flow = self.quit_to_system().control_flow.unwrap();
          }
          // resize
          event::WindowEvent::Resized (physical_size) => {
            use apis::Process;
            let (x, y) = { // borrow gui
              let gui = &mut self.main_context.as_mut().unwrap().gui;
              let (width, height) = {
                let (x, y) : (u32, u32) = physical_size.into();
                debug_assert!(x <= std::u16::MAX as u32);
                debug_assert!(y <= std::u16::MAX as u32);
                (x as u16, y as u16)
              };
              gui.set_screen_resolution (width, height);
              (width, height)
            };
            self.send (
              ChannelId::MainToAv,
              MainToAvMsg::Default (thread::MainToAvMsg::WindowSize { x, y })
            ).unwrap();
          }
          // keyboard input
          event::WindowEvent::KeyboardInput {
            input: event::KeyboardInput {
              state: event::ElementState::Pressed,
              virtual_keycode,
              scancode,
              ..
            },
            ..
          } => {
            log::debug!("keyboard input virtual keycode: {:?}", virtual_keycode);
            log::debug!("keyboard input scancode: {}", scancode);
            if let Some (virtual_keycode) = virtual_keycode {
              let keycode : thread::main::input::keyboard::Keycode =
                virtual_keycode.into();
              let maybe_input_fun = self.input_map.get (keycode as usize)
                .and_then (|input_fun| Some (*input_fun));
              if let Some (input_fun) = maybe_input_fun {
                let input_fun_result = input_fun (self);
                control_flow = input_fun_result.control_flow
                  .unwrap_or (apis::process::ControlFlow::Continue);
              }
            } else {
              // TODO: unhandled scancodes?
            }
          } // end KeyboardInput
          // mouse input
          event::WindowEvent::MouseInput { state, button, .. } => match state {
            event::ElementState::Pressed => match button {
              event::MouseButton::Left => {
                let input_fun_result = self.main_menu_select();
                control_flow = input_fun_result.control_flow
                  .unwrap_or (apis::process::ControlFlow::Continue);
              }
              _ => {}
            } // end pressed MouseButton
            _ => {}
          } // end MouseInput
          // cursor moved
          event::WindowEvent::CursorMoved { position, .. } => {
            use apis::Process;

            let (x, y) : (i32, i32) = position.into();
            log::trace!("cursor moved raw (physical): ({},{})", x, y);

            // x and y *can* be negative
            //debug_assert!(0 <= x);
            //debug_assert!(0 <= y);
            // x and y souldn't overflow a 16-bit integer
            debug_assert!(x < 2i32.pow (16));
            debug_assert!(y < 2i32.pow (16));

            let (x, y) = {
              // clamp to positive values
              let x : u16 = std::cmp::max (x, 0) as u16;
              let y : u16 = std::cmp::max (y, 0) as u16;
              // set mouse position in gui
              let main_context = self.main_context.as_mut().unwrap();
              main_context.gui.set_mouse_position (x, y);
              ( main_context.gui.mouse_position.x,
                main_context.gui.mouse_position.y )
            };

            self.send (
              ChannelId::MainToAv,
              MainToAvMsg::Default (thread::MainToAvMsg::MousePosition { x, y })
            ).unwrap();

            let button_intersect = {
              use enum_unitary::EnumUnitary;
              let main_context   = self.main_context.as_mut().unwrap();
              let mouse_position = cgmath::Point2::new (
                x as i16,
                main_context.gui.screen_resolution.y as i16 - y as i16);

              let mut button_intersect = None;
              for option in MainMenuOption::iter_variants() {
                use collision::Contains;
                let i = option as usize;
                if main_context.gui.main_menu_button_bounds[i]
                  .contains (&mouse_position)
                {
                  if main_context.gui.main_menu_active_option != option {
                    main_context.gui.main_menu_active_option = option;
                    button_intersect = Some (option);
                    break;
                  }
                }
              }
              button_intersect
            };
            if let Some (option) = button_intersect {
              self.send (
                ChannelId::MainToAv, MainToAvMsg::MainMenuActive { option }
              ).unwrap();
            }
          } // end DeviceEvent::MouseMotion
          _ => ()
        } // end WindowEvent
        Event::DeviceEvent { event, .. } => match event {
          _ => {}
        } // end DeviceEvent
        //Event::Awakened
        //Event::Suspended (b)
        _ => ()
      } // end match glutin event
    }); // end glutin poll input events
    debug_assert!(self.main_context.as_mut().unwrap().input.event_loop
      .is_none());
    self.main_context.as_mut().unwrap().input.event_loop = Some (event_loop);

    log::trace!("...MainThread update");

    control_flow
  } // end fn main_update

  //
  //  input functions
  //

  pub fn screenshot (&mut self) -> InputFunResult {
    use apis::Process;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Screenshot)
    ).unwrap();
    InputFunResult::default()
  }

  pub fn quit_to_system (&mut self) -> InputFunResult {
    use apis::Process;
    *self.result_mut() = TitleMenuResult::QuitToSystem;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Quit {
        reset_audio_on_term: true
      })
    ).unwrap();
    InputFunResult {
      control_flow: Some (apis::process::ControlFlow::Break),
      .. InputFunResult::default()
    }
  }

  pub fn start_practice (&mut self) -> InputFunResult {
    use apis::Process;
    *self.result_mut() = TitleMenuResult::StartPractice;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Quit {
        reset_audio_on_term: true
      })
    ).unwrap();
    InputFunResult {
      control_flow: Some (apis::process::ControlFlow::Break),
      .. InputFunResult::default()
    }
  }

  pub fn start_multi_setup (&mut self) -> InputFunResult {
    use apis::Process;
    *self.result_mut() = TitleMenuResult::StartMultiSetup;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Quit {
        reset_audio_on_term: false
      })
    ).unwrap();
    InputFunResult {
      control_flow: Some (apis::process::ControlFlow::Break),
      .. InputFunResult::default()
    }
  }

  pub fn main_menu_select (&mut self) -> InputFunResult {
    let mut result = InputFunResult::default();
    match self.main_context.as_mut().unwrap().gui.main_menu_active_option {
      MainMenuOption::Practice => {
        result = self.start_practice();
      }
      MainMenuOption::Multiplayer => {
        result = self.start_multi_setup();
      }
      MainMenuOption::Settings => {
        println!("TODO: SETTINGS");
      }
      MainMenuOption::Exit => {
        result = self.quit_to_system();
      }
    }
    result
  }

  pub fn main_menu_next (&mut self) -> InputFunResult {
    if let Some (next_option) = self.main_context.as_mut().unwrap()
      .gui.main_menu_next()
    {
      use apis::Process;
      self.send (
        ChannelId::MainToAv,
        MainToAvMsg::MainMenuActive { option: next_option }
      ).unwrap();
    }
    InputFunResult::default()
  }

  pub fn main_menu_prev (&mut self) -> InputFunResult {
    if let Some (prev_option) = self.main_context.as_mut().unwrap()
      .gui.main_menu_prev()
    {
      use apis::Process;
      self.send (
        ChannelId::MainToAv,
        MainToAvMsg::MainMenuActive { option: prev_option }
      ).unwrap();
    }
    InputFunResult::default()
  }

  pub fn main_menu_first (&mut self) -> InputFunResult {
    if let Some (first_option) = self.main_context.as_mut().unwrap()
      .gui.main_menu_first()
    {
      use apis::Process;
      self.send (
        ChannelId::MainToAv,
        MainToAvMsg::MainMenuActive { option: first_option }
      ).unwrap();
    }
    InputFunResult::default()
  }

  pub fn main_menu_last (&mut self) -> InputFunResult {
    if let Some (last_option) = self.main_context.as_mut().unwrap()
      .gui.main_menu_last()
    {
      use apis::Process;
      self.send (
        ChannelId::MainToAv,
        MainToAvMsg::MainMenuActive { option: last_option }
      ).unwrap();
    }
    InputFunResult::default()
  }

} // end impl MainThread

impl AvThread {

  fn init (&mut self) {
    {
      use thread::av::render;
      let render_context = &self.av_context.as_ref().unwrap().render;
      // set bg vertex texture
      render_context.bg_vertex_buffer.write (&[
        render::shader::Vert2dSpriteArray {
          in_position:      [0.0, 0.0],
          in_dimensions:    [2.0, 2.0],   // -1.0 to 1.0 in both dimensions
          in_texture_index: render::BackgroundId::Court as u32
        }
      ]);
    }

    let audio_context = &mut self.av_context.as_mut().unwrap().audio;
    // set global reverb properties
    let mut reverb_properties = rfmod::ReverbProperties::default();
    reverb_properties.environment = 0;
    rfmod_ok!(audio_context.rfmod_sys.set_reverb_properties (reverb_properties));
    // add echo dsp
    let _dsp_connection =
      audio_context.rfmod_sys.add_DSP (&audio_context.dsp_echo).unwrap();
    // start playing ambient fire audio
    self.rfmod_channel_fire = {
      let rfmod_channel_fire = audio_context.sound_ambient_fire.play().unwrap();
      rfmod_ok!(rfmod_channel_fire.set_3D_attributes (
        &rfmod::Vector { x: -2.0, y: 0.0, z: -10.0 },
        &rfmod::Vector { x:  0.0, y: 0.0, z:   0.0 }));
      Some (rfmod_channel_fire)
    };

    // start playing title music
    self.rfmod_channel_music = {
      let rfmod_channel_music = audio_context.sound_music_title.play().unwrap();
      Some (rfmod_channel_music)
    };
    rfmod_ok!(audio_context.rfmod_sys.update());
  }

  fn term (&mut self) {

    if self.reset_audio_on_term {
      // stop playing ambient fire audio
      // FIXME: this assert sometimes fails with error 'ChannelStolen' when
      // quitting to system
      // TODO: on windows this is a different error ... TODO
      //error!("DEBUG terminating av thread");
      rfmod_ok!(self.rfmod_channel_fire.as_ref().unwrap().stop());
      //error!("DEBUG fire audio stopped");
      // FIXME: error with "InvalidHandle"
      rfmod_ok!(self.rfmod_channel_music.as_ref().unwrap().stop());
      //error!("DEBUG title music stopped");

      // remove echo dsp
      let audio_context = &mut self.av_context.as_mut().unwrap().audio;
      rfmod_ok!(audio_context.dsp_echo.remove());
      // disable global reverb
      let mut reverb_properties = rfmod::ReverbProperties::default();
      reverb_properties.environment = -1; // -1 == OFF
      rfmod_ok!(audio_context.rfmod_sys.set_reverb_properties (reverb_properties));
    }
  }

  fn av_handle_message (&mut self, _message : GlobalMessage)
    -> apis::process::ControlFlow
  {
    log::trace!("AvThread handle message...");
    #[allow(unused_assignments)]
    let mut control_flow = apis::process::ControlFlow::Continue;
    match _message {
      // window size
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::WindowSize { x, y })
      ) => {
        self.av_context.as_mut().unwrap().render.set_resolution (x,y);
      }
      // mouse position
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::MousePosition { x, y })
      ) => {
        self.av_context.as_mut().unwrap().render.set_mouse_position (x,y);
      }
      // quit
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::Quit { reset_audio_on_term })
      ) => {
        self.reset_audio_on_term = reset_audio_on_term;
        control_flow = apis::process::ControlFlow::Break;
      }
      // screenshot
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::Screenshot)
      ) => {
        self.av_context.as_ref().unwrap().render.screenshot();
      }
      // main menu selection
      GlobalMessage::MainToAvMsg (MainToAvMsg::MainMenuActive { option }) => {
        let av_context = &mut self.av_context.as_mut().unwrap();
        let _ = av_context.audio.sound_sfx_gui_tick.play().unwrap();
        av_context.render.main_menu_set_active_option (option);
      }
    }
    log::trace!("...AvThread handle message");
    control_flow
  }

  fn av_update (&mut self) -> apis::process::ControlFlow {
    use glium::Surface;
    use thread::av::render::shader;
    log::trace!("AvThread update...");

    log::trace!("AvThread frame: {}", self.frame);

    { // audio frame
      let audio_context = &mut self.av_context.as_mut().unwrap().audio;
      // fmod update
      rfmod_ok!(audio_context.rfmod_sys.update());
    }

    { // render frame
    let render_context = &mut self.av_context.as_mut().unwrap().render;
    let mut glium_frame = render_context.glium_display.draw();
    glium_frame.clear_all ((0.0, 0.0, 0.0, 1.0), 0.0, 0);

    { // draw bg sprite
      let program = &render_context
        .shader_programs [shader::ProgramId::ClipSpace2dSpriteArray as usize];
      glium_frame.draw (
        &render_context.bg_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_sampler_2d_array: render_context.bg_textures.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest),
        },
        &Default::default()
      ).unwrap();
    } // end draw bg sprite

    { // draw main menu buttons
      let program = &render_context.shader_programs [
        shader::ProgramId::ClipSpace2dSpriteArrayColorize as usize
      ];
      // TODO: should we keep a current sampler in the render state ?
      let uni_sampler_2d_array
        : glium::uniforms::Sampler <glium::texture::Texture2dArray>
        = render_context.main_menu_button_textures.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest);
      glium_frame.draw (
        &render_context.main_menu_button_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_sampler_2d_array: uni_sampler_2d_array
        },
        &Default::default()
      ).unwrap();
    } // end draw main menu buttons

    { // draw main menu cursor
      let program = &render_context
        .shader_programs [shader::ProgramId::ScreenSpace2dSprite as usize];
      let uni_projection_mat_ortho : &[[f32; 4]; 4] =
        render_context.projection_mat_ortho.as_ref();

      glium_frame.draw (
        &render_context.main_menu_cursor_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_projection_mat_ortho: *uni_projection_mat_ortho,
          uni_sampler_2d: render_context.main_menu_cursor_texture.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest),
            //.anisotropy (0),
          //uni_frame:             self.frame as u32
        },
        &Default::default()
      ).unwrap();
    } // end draw main menu cursor

    { // draw mouse pointer
      if render_context.last_mouse_position != render_context.mouse_position {
        let width  : u16 =
          render_context.mouse_pointer_texture.width() as u16
            * render_context.gui_scale as u16;
        let height : u16 =
          render_context.mouse_pointer_texture.height() as u16
            * render_context.gui_scale as u16;
        render_context.mouse_pointer_vertex_buffer.write (
          &[shader::Vert2dSprite {
            in_position:   [
              (render_context.mouse_position.x + width/2) as f32,
              (render_context.resolution.y as i32
                - render_context.mouse_position.y as i32
                - height as i32/2) as f32],
            in_dimensions: [width as f32, height as f32]
          }]
        );
        render_context.last_mouse_position = render_context.mouse_position;
      }
      let program = &render_context
        .shader_programs [ shader::ProgramId::ScreenSpace2dSprite as usize];
      let uni_projection_mat_ortho : &[[f32; 4]; 4] =
        render_context.projection_mat_ortho.as_ref();

      glium_frame.draw (
        &render_context.mouse_pointer_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_projection_mat_ortho: *uni_projection_mat_ortho,
          uni_sampler_2d: render_context.mouse_pointer_texture.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest),
            //.anisotropy (0),
          //uni_frame:             self.frame as u32
        },
        &Default::default()
      ).unwrap();
    } // end draw mouse pointer

    glium_frame.finish().unwrap();
    } // end render frame

    self.av_context.as_mut().unwrap().do_frame();

    self.frame += 1;

    log::trace!("...AvThread update");
    apis::process::ControlFlow::Continue
  } // end fn av_update

} // end impl AvThread

impl Default for TitleMenuResult {
  fn default() -> Self {
    TitleMenuResult::QuitToSystem
  }
}

impl MainMenuOption {
  pub fn next (&self) -> Self {
    use num_traits::FromPrimitive;
    MainMenuOption::from_usize (
      (self.clone() as usize + 1) % MainMenuOption::count()
    ).unwrap()
  }
  pub fn prev (&self) -> Self {
    use num_traits::FromPrimitive;
    MainMenuOption::from_usize (
      ((self.clone() as usize + MainMenuOption::count()) - 1)
        % MainMenuOption::count()
    ).unwrap()
  }
}
