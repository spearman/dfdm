// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use std::{self, time};
use {/*cgmath, collision,*/ rfmod};
use glium::{self, glutin, uniform};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use vec_map::VecMap;

use {apis, /*gl_utils*/};
use fmod_utils::rfmod_ok;

use dfdm::{client, server, VERSION};

use crate::thread::{
  self,
  main::input::keyboard,
  av::render::shader
};


////////////////////////////////////////////////////////////////////////////////
//  constants                                                                 //
////////////////////////////////////////////////////////////////////////////////

const CONNECT_TO_STR_FULL       : &str  = "123.123.123.123:12345c";
const CONNECTING_STR            : &str  = "Connecting...";
const FAILED_STR                : &str  = "Failed to connect";
const REFUSED_STR               : &str  = "Connection refused";
const VERSION_MISMATCH_STR      : &str  = "Version mismatch";

const CONNECT_TO_STR_MAX_LEN    : usize = CONNECT_TO_STR_FULL.len();
const CONNECTING_STR_LEN        : usize = CONNECTING_STR.len();
const FAILED_STR_LEN            : usize = FAILED_STR.len();
const REFUSED_STR_LEN           : usize = REFUSED_STR.len();
const VERSION_MISMATCH_STR_LEN  : usize = VERSION_MISMATCH_STR.len();

const RESULT_TILE_ROW           : i32   = -2;
const RESULT_TILE_COLUMN        : i32   = -10;

const CONNECT_TIMEOUT_MS        : u64   = 5000;
const CONNECT_TIMEOUT_DUR       : time::Duration  =
  time::Duration::from_millis (CONNECT_TIMEOUT_MS);

const CONNECTING_ANIM_FRAME_MS  : u64   = 500;
const CONNECTING_ANIM_FRAMES    : u64   = 4;

////////////////////////////////////////////////////////////////////////////////
//  statics                                                                   //
////////////////////////////////////////////////////////////////////////////////

lazy_static!{
  pub static ref STATIC_CONFIG    : &'static StaticConfig =
    &crate::STATIC_CONFIG.mode_config.multi_setup_config;
  pub static ref STATIC_INPUT_MAP : VecMap <InputFun>     = {
    let mut v = VecMap::new();
    for (keycode, input_fun) in STATIC_CONFIG.input_map.iter() {
      assert!(v.insert (*keycode as usize, *input_fun).is_none());
    }
    v
  };
  pub static ref TILE_COUNT       : usize = CONNECT_TO_STR_MAX_LEN +
    usize::max (
      usize::max (CONNECTING_STR_LEN, FAILED_STR_LEN),
      usize::max (REFUSED_STR_LEN, VERSION_MISMATCH_STR_LEN));
}

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  /// Initial contents of the connection address
  pub connect_to : Option <(std::net::Ipv4Addr, u16)>,
  pub input_map  : std::collections::HashMap <keyboard::Keycode, InputFun>
}

/// State for entering a server address and connection status
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct ConnectTo {
  a1      : Option <String>,
  a2      : Option <String>,
  a3      : Option <String>,
  a4      : Option <String>,
  port    : Option <String>,
  skip    : bool,
  timeout : Option <std::time::Instant>,
  failed  : bool,
  refused : bool
}

////////////////////////////////////////////////////////////////////////////////
//  enums                                                                     //
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone,Debug,Eq,PartialEq)]
pub enum MultiSetupResult {
  ConnectionAccepted,
  QuitToTitle,
  QuitToSystem
}

#[derive(Clone,Copy,Debug,Eq,PartialEq)]
pub enum ConnectionState {
  None,
  Connecting,
  Failed,
  Refused,
  VersionMismatch
}

////////////////////////////////////////////////////////////////////////////////
//  session                                                                   //
////////////////////////////////////////////////////////////////////////////////

//
//  mode MultiSetup
//
apis::def_session! {
  context MultiSetup {
    PROCESSES where
      let proc       = self,
      let message_in = message_in
    [
      process MainThread (
        main_context    : Option <thread::Main> = None,
        input_map       : VecMap <InputFun>     = STATIC_INPUT_MAP.clone(),
        connect_to      : ConnectTo             = {
          if let Some ((host, port)) = STATIC_CONFIG.connect_to {
            let host = host.octets();
            ConnectTo {
              a1:   Some (host[0].to_string()),
              a2:   Some (host[1].to_string()),
              a3:   Some (host[2].to_string()),
              a4:   Some (host[3].to_string()),
              port: Some (port.to_string()),
              .. ConnectTo::default()
            }
          } else {
            ConnectTo::default()
          }
        }
      ) -> (MultiSetupResult) {
        kind           {
          apis::process::Kind::Mesochronous {
            tick_ms:          *thread::main::INPUT_MS,
            ticks_per_update: 1
          }
        }
        sourcepoints   [MainToAv, MainToNet]
        endpoints      [NetToMain]
        initialize     { proc.main_init() }
        handle_message { proc.main_handle_message (message_in) }
        update         { proc.main_update() }
      }
      process AvThread (
        av_context            : Option <Box <thread::Av>> = None,
        rfmod_channel_fire    : Option <rfmod::Channel>   = None,
        rfmod_channel_music   : Option <rfmod::Channel>   = None,
        frame                 : u64                       = 0,
        // free audio resources when terminating
        reset_audio_on_term   : bool                      = false,
        connection_state      : ConnectionState           =
          ConnectionState::None,
        // timestamp to animate the 'Connecting...' tiles
        connecting_anim_start : time::Instant = time::Instant::now()
      ) {
        kind           { apis::process::Kind::Anisochronous }
        sourcepoints   [ ]
        endpoints      [MainToAv]
        initialize     { proc.av_init() }
        terminate      { proc.av_term() }
        handle_message { proc.av_handle_message (message_in) }
        update         { proc.av_update() }
      }
      process NetThread (
        net_context         : Option <thread::Net> = Some (thread::Net::create()),
        timeout             : Option <std::time::Instant>
      ) {
        kind           { apis::process::Kind::Anisochronous }
        sourcepoints   [NetToMain]
        endpoints      [MainToNet]
        handle_message { proc.net_handle_message (message_in) }
        update         { proc.net_update() }
      }
    ]
    CHANNELS [
      channel MainToAv <MainToAvMsg> (Simplex) {
        producers [MainThread]
        consumers [AvThread]
      }
      channel MainToNet <MainToNetMsg> (Simplex) {
        producers [MainThread]
        consumers [NetThread]
      }
      channel NetToMain <NetToMainMsg> (Simplex) {
        producers [NetThread]
        consumers [MainThread]
      }
    ]
    MESSAGES [
      message MainToAvMsg {
        Default (thread::MainToAvMsg),
        // string of extended ASCII bytes
        ConnectToString (Vec <u8>),
        Connecting,
        Failed,
        Refused,
        VersionMismatch
      }
      message MainToNetMsg {
        ConnectTo (enet::Address),
        Quit
      }
      message NetToMainMsg {
        ConnectionFailed,
        ConnectionRefused,
        VersionMismatch,
        ConnectionAccepted (server::ClientId),
        Synchronized
      }
    ]
    main: MainThread
  }
} // end mode MultiSetup

////////////////////////////////////////////////////////////////////////////////
//  input functions                                                           //
////////////////////////////////////////////////////////////////////////////////

impl_input_fun! {
  MainThread {
    MainThread::screenshot,    Screenshot
    MainThread::quit_to_title, QuitToTitle
  }
}

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

/// Center pixel position of "connect to" box sprite
pub fn connect_to_box_vertex (
  box_dimensions    : cgmath::Vector2 <u16>,  // pixel dimensions
  screen_resolution : cgmath::Vector2 <u16>,
  gui_scale         : u8
) -> shader::Vert2dSprite {
  let scaled_dimensions = gui_scale as u16 * box_dimensions;
  let half_resolution   = screen_resolution / 2;
  shader::Vert2dSprite {
    in_position:   [
      half_resolution.x as f32 - gui_scale as f32 * 22.0,
      half_resolution.y as f32 + gui_scale as f32 *  5.0],
    in_dimensions: [
      scaled_dimensions.x as f32,
      scaled_dimensions.y as f32]
  }
}

pub fn result_tiles (result_str : &str) -> Vec <shader::Vert2dTile> {
  let in_row = RESULT_TILE_ROW;
  result_str.chars().enumerate().map (|(i, ch)|{
    let in_column = RESULT_TILE_COLUMN + i as i32;
    let in_tile   = ch as u8;
    shader::Vert2dTile { in_row, in_column, in_tile }
  }).collect()
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

impl MainThread {

  pub fn main_init (&mut self) {
    //let gui_context = &self.main_context.as_ref().unwrap().gui;
    // send initial connect-to string
    let connect_to_string = self.connect_to.to_bytes();
    use apis::Process;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::ConnectToString (connect_to_string)
    ).unwrap();
  }

  //
  //  process behavior methods
  //
  fn main_handle_message (&mut self, message : GlobalMessage)
    -> apis::process::ControlFlow
  {
    log::trace!("MainThread handle message...");
    let mut control_flow = apis::process::ControlFlow::Continue;
    match message {
      GlobalMessage::NetToMainMsg (NetToMainMsg::ConnectionFailed)   =>
        self.connection_failed(),
      GlobalMessage::NetToMainMsg (NetToMainMsg::ConnectionRefused)  =>
        self.connection_refused(),
      GlobalMessage::NetToMainMsg (NetToMainMsg::VersionMismatch)  =>
        self.version_mismatch(),
      GlobalMessage::NetToMainMsg (NetToMainMsg::ConnectionAccepted (client_id))
        =>
      {
        self.connection_accepted (client_id);
        control_flow = apis::process::ControlFlow::Break;
      }
      _ => unreachable!()
    }
    log::trace!("...MainThread handle message");
    control_flow
  }

  fn main_update (&mut self) -> apis::process::ControlFlow {
    log::trace!("MainThread update...");
    let control_flow = self.input();
    log::trace!("...MainThread update");
    control_flow
  } // end fn main_update

  //
  //  input functions
  //

  pub fn screenshot (&mut self) -> InputFunResult {
    use apis::Process;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Screenshot)
    ).unwrap();
    InputFunResult::default()
  }

  pub fn quit_to_title (&mut self) -> InputFunResult {
    use apis::Process;
    *self.result_mut() = MultiSetupResult::QuitToTitle;
    self.send (ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Quit {
        reset_audio_on_term: false
      })
    ).unwrap();
    self.send (ChannelId::MainToNet, MainToNetMsg::Quit).unwrap();
    InputFunResult {
      control_flow: Some (apis::process::ControlFlow::Break),
      .. InputFunResult::default()
    }
  }

  pub fn quit_to_system (&mut self) -> InputFunResult {
    use apis::Process;
    *self.result_mut() = MultiSetupResult::QuitToSystem;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Quit {
        reset_audio_on_term: true
      })
    ).unwrap();
    self.send (ChannelId::MainToNet, MainToNetMsg::Quit).unwrap();
    InputFunResult {
      control_flow: Some (apis::process::ControlFlow::Break),
      .. InputFunResult::default()
    }
  }

  fn connect_to_try_insert_digit (&mut self, digit : u32) {
    self.connect_to.try_insert_digit (digit);
    let connect_to_string = self.connect_to.to_bytes();
    use apis::Process;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::ConnectToString (connect_to_string)
    ).unwrap();
  }

  fn connect_to_try_erase_digit (&mut self) {
    self.connect_to.try_erase_digit();
    let connect_to_string = self.connect_to.to_bytes();
    use apis::Process;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::ConnectToString (connect_to_string)
    ).unwrap();
  }

  fn connect_to_try_skip_digit (&mut self) {
    self.connect_to.try_skip_digit();
    let connect_to_string = self.connect_to.to_bytes();
    use apis::Process;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::ConnectToString (connect_to_string)
    ).unwrap();
  }

  /// This function requires that `connect_to` fields a1, a2, a3, a4, and port
  /// are all `Some`
  fn try_connect (&mut self) {
    use apis::Process;
    self.send (ChannelId::MainToAv, MainToAvMsg::Connecting).unwrap();
    let connect_to = self.connect_to.clone();
    let a1   = connect_to.a1.unwrap();
    let a2   = connect_to.a2.unwrap();
    let a3   = connect_to.a3.unwrap();
    let a4   = connect_to.a4.unwrap();
    let port = connect_to.port.unwrap();
    let ip   = [
      a1, ".".to_string(),
      a2, ".".to_string(),
      a3, ".".to_string(),
      a4
    ].concat();
    let address = enet::Address::with_hostname (
      ip.as_str(), u16::from_str_radix (&port, 10).unwrap()
    ).unwrap();
    log::info!("connecting to server ({:?})...", address);
    self.send (ChannelId::MainToNet, MainToNetMsg::ConnectTo (address))
      .unwrap();
    self.connect_to.timeout = Some (time::Instant::now() + CONNECT_TIMEOUT_DUR);
  }

  fn input (&mut self) -> apis::process::ControlFlow {
    use colored::Colorize;
    use glutin::platform::desktop::EventLoopExtDesktop;
    log::trace!("MainThread input...");
    let mut control_flow = apis::process::ControlFlow::Continue;
    // glutin poll input events
    let mut event_loop = self.main_context.as_mut().unwrap().input.event_loop
      .take().unwrap();
    event_loop.run_return (|event, _, event_loop_control_flow|{
      use glutin::event::{self, Event};
      *event_loop_control_flow = glutin::event_loop::ControlFlow::Poll;
      log::trace!("{}: {:?}", "glutin event".to_string().cyan().bold(), event);
      match event {
        Event::MainEventsCleared => {
          *event_loop_control_flow = glutin::event_loop::ControlFlow::Exit;
        }
        Event::WindowEvent { event, .. } => match event {
          // closed
          event::WindowEvent::CloseRequested |
          event::WindowEvent::Destroyed      => {
            log::info!("glutin WindowEvent::Closed: quitting to system...");
            // always returns Some(Break)
            control_flow = self.quit_to_system().control_flow.unwrap();
          }
          // resize
          event::WindowEvent::Resized (physical_size) => {
            use apis::Process;
            let (x, y) = { // borrow gui
              let gui = &mut self.main_context.as_mut().unwrap().gui;
              let (width, height) = {
                let (x, y) : (u32, u32) = physical_size.into();
                debug_assert!(x <= std::u16::MAX as u32);
                debug_assert!(y <= std::u16::MAX as u32);
                (x as u16, y as u16)
              };
              gui.set_screen_resolution (width, height);
              (width, height)
            };
            self.send (
              ChannelId::MainToAv,
              MainToAvMsg::Default (thread::MainToAvMsg::WindowSize { x, y })
            ).unwrap();
          }
          // keyboard input
          // TODO: fix deprecated use of modifiers field
          #[allow(deprecated)]
          event::WindowEvent::KeyboardInput {
            input: event::KeyboardInput {
              state: event::ElementState::Pressed,
              virtual_keycode,
              scancode,
              modifiers,
              ..
            },
            ..
          } => {
            log::debug!("keyboard input virtual keycode: {:?}", virtual_keycode);
            log::debug!("keyboard input scancode: {}", scancode);
            if let Some (virtual_keycode) = virtual_keycode {
              let keycode : keyboard::Keycode = virtual_keycode.into();
              let maybe_input_fun = self.input_map.get (keycode as usize)
                .and_then (|input_fun| Some (*input_fun));
              if let Some (input_fun) = maybe_input_fun {
                let input_fun_result = input_fun (self);
                control_flow = input_fun_result.control_flow
                  .unwrap_or (apis::process::ControlFlow::Continue);
              } else {
                // text input
                match virtual_keycode {
                  event::VirtualKeyCode::Back =>
                    self.connect_to_try_erase_digit(),
                  event::VirtualKeyCode::Tab    |
                  event::VirtualKeyCode::Period |
                  event::VirtualKeyCode::Colon  =>
                    self.connect_to_try_skip_digit(),
                  event::VirtualKeyCode::Return => match self.connect_to {
                    ConnectTo {
                      a1:      Some (_),
                      a2:      Some (_),
                      a3:      Some (_),
                      a4:      Some (_),
                      port:    Some (_),
                      timeout: None,
                      ..
                    } => self.try_connect(),
                    _ => {}
                  },
                  _ => if !modifiers.shift() && !modifiers.ctrl() &&
                    !modifiers.alt() && !modifiers.logo()
                  {
                    if let Some (digit) = match virtual_keycode {
                      event::VirtualKeyCode::Key1 => Some (1),
                      event::VirtualKeyCode::Key2 => Some (2),
                      event::VirtualKeyCode::Key3 => Some (3),
                      event::VirtualKeyCode::Key4 => Some (4),
                      event::VirtualKeyCode::Key5 => Some (5),
                      event::VirtualKeyCode::Key6 => Some (6),
                      event::VirtualKeyCode::Key7 => Some (7),
                      event::VirtualKeyCode::Key8 => Some (8),
                      event::VirtualKeyCode::Key9 => Some (9),
                      event::VirtualKeyCode::Key0 => Some (0),
                      _ => None
                    } {
                      self.connect_to_try_insert_digit (digit);
                    }
                  }
                }
              }
            } else {
              // TODO: unhandled scancodes ?
            }
          } // end KeyboardInput
          // mouse input
          event::WindowEvent::MouseInput { state, button, .. } => match state {
            event::ElementState::Pressed => match button {
              event::MouseButton::Left => {
                // TODO: mouse button select
                /*
                let input_fun_result = self.main_menu_select();
                control_flow = input_fun_result.control_flow
                  .unwrap_or (apis::process::ControlFlow::Continue);
                */
              }
              _ => {}
            } // end pressed MouseButton
            _ => {}
          } // end MouseInput
          // cursor moved
          event::WindowEvent::CursorMoved { position, .. } => {
            use apis::Process;

            let (x, y) : (i32, i32) = position.into();
            log::trace!("cursor moved raw (physical): ({},{})", x, y);

            // x and y *can* be negative
            //debug_assert!(0 <= x);
            //debug_assert!(0 <= y);
            // x and y souldn't overflow a 16-bit integer
            debug_assert!(x < 2i32.pow (16));
            debug_assert!(y < 2i32.pow (16));

            let (x, y) = {
              // clamp to positive values
              let x : u16 = std::cmp::max (x, 0) as u16;
              let y : u16 = std::cmp::max (y, 0) as u16;
              // set mouse position in gui
              let main_context = self.main_context.as_mut().unwrap();
              main_context.gui.set_mouse_position (x, y);
              ( main_context.gui.mouse_position.x,
                main_context.gui.mouse_position.y )
            };

            self.send (
              ChannelId::MainToAv,
              MainToAvMsg::Default (thread::MainToAvMsg::MousePosition { x, y })
            ).unwrap();

            // TODO: mouse over buttons
            /*
            let button_intersect = {
              let main_context   = self.main_context.as_mut().unwrap();
              let mouse_position = cgmath::Point2::new (
                x as i16,
                main_context.gui.screen_resolution.y as i16 - y as i16);

              let mut button_intersect = None;
              for option in MainMenuOption::iter_variants() {
                use collision::Contains;
                let i = option as usize;
                if main_context.gui.main_menu_button_bounds[i]
                  .contains (&mouse_position)
                {
                  if main_context.gui.main_menu_active_option != option {
                    main_context.gui.main_menu_active_option = option;
                    button_intersect = Some (option);
                    break;
                  }
                }
              }
              button_intersect
            };
            if let Some (option) = button_intersect {
              self.send (
                ChannelId::MainToAv, MainToAvMsg::MainMenuActive { option }
              ).unwrap();
            }
            */
          } // end DeviceEvent::MouseMotion
          _ => ()
        } // end WindowEvent
        Event::DeviceEvent { event, .. } => match event {
          _ => {}
        } // end DeviceEvent
        //Event::Awakened
        //Event::Suspended (b)
        _ => ()
      } // end match glutin event
    }); // end glutin poll input events
    debug_assert!(self.main_context.as_mut().unwrap().input.event_loop
      .is_none());
    self.main_context.as_mut().unwrap().input.event_loop = Some (event_loop);
    log::trace!("...MainThread input");
    control_flow
  }

  /// Should only be called when `self.connect_to.timeout` is `Some`
  fn connection_failed (&mut self) {
    use apis::Process;
    self.send (ChannelId::MainToAv, MainToAvMsg::Failed).unwrap();
    debug_assert!(self.connect_to.timeout.is_some());
    self.connect_to.failed  = true;
    self.connect_to.refused = false;
    self.connect_to.timeout = None;
  }
  /// Should only be called when `self.connect_to.timeout` is `Some`
  fn connection_refused (&mut self) {
    use apis::Process;
    self.send (ChannelId::MainToAv, MainToAvMsg::Refused).unwrap();
    debug_assert!(self.connect_to.timeout.is_some());
    self.connect_to.failed  = false;
    self.connect_to.refused = true;
    self.connect_to.timeout = None;
  }
  /// Should only be called when `self.connect_to.timeout` is `Some`
  fn version_mismatch (&mut self) {
    use apis::Process;
    self.send (ChannelId::MainToAv, MainToAvMsg::VersionMismatch).unwrap();
    debug_assert!(self.connect_to.timeout.is_some());
    self.connect_to.failed  = true;
    self.connect_to.refused = false;
    self.connect_to.timeout = None;
  }
  /// Should only be called when `self.connect_to.timeout` is `Some`
  fn connection_accepted (&mut self, client_id : server::ClientId) {
    use apis::Process;
    {
      let mut lock = thread::CLIENT_ID.write().unwrap();
      debug_assert!(lock.is_none());
      *lock = Some (client_id);
    }
    *self.result_mut() = MultiSetupResult::ConnectionAccepted;
    self.send (ChannelId::MainToNet, MainToNetMsg::Quit).unwrap();
    self.send (ChannelId::MainToAv,  MainToAvMsg::Default (
      thread::MainToAvMsg::Quit { reset_audio_on_term: true }
    )).unwrap();
  }
} // end impl MainThread

impl AvThread {

  fn av_init (&mut self) {
    {
      use thread::av::render;
      let render_context = &self.av_context.as_ref().unwrap().render;
      // set bg vertex texture
      render_context.bg_vertex_buffer.write (&[
        shader::Vert2dSpriteArray {
          in_position:      [0.0, 0.0],
          in_dimensions:    [2.0, 2.0],   // -1.0 to 1.0 in both dimensions
          in_texture_index: render::BackgroundId::SkySepia as u32
        }
      ]);
    }

    // TODO: init audio
    /*
    //let render_context = &self.av_context.as_ref().unwrap().render;
    let audio_context = &mut self.av_context.as_mut().unwrap().audio;
    // set global reverb properties
    let mut reverb_properties = rfmod::ReverbProperties::default();
    reverb_properties.environment = 0;
    rfmod_ok!(audio_context.rfmod_sys.set_reverb_properties (reverb_properties));
    // add echo dsp
    let _dsp_connection =
      audio_context.rfmod_sys.add_DSP (&audio_context.dsp_echo).unwrap();
    // start playing ambient fire audio
    self.rfmod_channel_fire = {
      let rfmod_channel_fire = audio_context.sound_ambient_fire.play().unwrap();
      rfmod_ok!(rfmod_channel_fire.set_3D_attributes (
        &rfmod::Vector { x: -2.0, y: 0.0, z: -10.0 },
        &rfmod::Vector { x:  0.0, y: 0.0, z:   0.0 }));
      Some (rfmod_channel_fire)
    };
    // start playing title music
    self.rfmod_channel_music = {
      let rfmod_channel_music = audio_context.sound_music_title.play().unwrap();
      Some (rfmod_channel_music)
    };
    rfmod_ok!(audio_context.rfmod_sys.update());
    */
  }

  fn av_term (&mut self) {
    if self.reset_audio_on_term {
      // stop playing ambient fire audio
      // FIXME: this assert sometimes fails with error 'ChannelStolen' when
      // quitting to system
      // FIXME: on windows this is a different error ... TODO
      //error!("DEBUG terminating av thread");
      rfmod_ok!(self.rfmod_channel_fire.as_ref().unwrap().stop());
      //error!("DEBUG fire audio stopped");
      rfmod_ok!(self.rfmod_channel_music.as_ref().unwrap().stop());
      //error!("DEBUG title music stopped");

      // remove echo dsp
      let audio_context = &mut self.av_context.as_mut().unwrap().audio;
      rfmod_ok!(audio_context.dsp_echo.remove());
      // disable global reverb
      let mut reverb_properties = rfmod::ReverbProperties::default();
      reverb_properties.environment = -1; // -1 == OFF
      rfmod_ok!(audio_context.rfmod_sys.set_reverb_properties (reverb_properties));

      // FIXME: testing update here, keep it if it fixes segfault problem
      // FIXME: if "segfault problem" refers to the 'channel stolen' error
      // above, then it doesn't seem to have fixed it
      rfmod_ok!(audio_context.rfmod_sys.update());
    }
  }

  fn av_handle_message (&mut self, _message : GlobalMessage)
    -> apis::process::ControlFlow
  {
    log::trace!("AvThread handle message...");
    #[allow(unused_assignments)]
    let mut control_flow = apis::process::ControlFlow::Continue;
    match _message {
      // window size
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::WindowSize { x, y })
      ) => self.av_context.as_mut().unwrap().render.set_resolution (x,y),
      // mouse position
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::MousePosition { x, y })
      ) => self.av_context.as_mut().unwrap().render.set_mouse_position (x,y),
      // quit
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::Quit {
          reset_audio_on_term
        })
      ) => {
        self.reset_audio_on_term = reset_audio_on_term;
        control_flow = apis::process::ControlFlow::Break;
      }
      // screenshot
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::Screenshot)
      ) => self.av_context.as_ref().unwrap().render.screenshot(),
      //
      // multi setup actions
      //
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::ConnectToString (string)
      ) => {
        const BASE_COL : i32 = -10;
        debug_assert!(string.len() <= CONNECT_TO_STR_MAX_LEN);
        let mut vertices = Vec::new();
        for (i, byte) in string.into_iter().enumerate() {
          let in_column = BASE_COL + i as i32;
          vertices.push (
            shader::Vert2dTile { in_row: 1, in_column, in_tile: byte });
        }
        // trailing whitespace
        let mut in_column = vertices.len() as i32;
        while in_column < CONNECT_TO_STR_MAX_LEN as i32 {
          vertices.push (
            shader::Vert2dTile { in_row: 1, in_column, in_tile: ' ' as u8 });
          in_column += 1;
        }
        self.av_context.as_mut().unwrap().render.connect_to_tile_vertex_buffer
          .slice (0..CONNECT_TO_STR_MAX_LEN).unwrap()
          .write (&vertices[..]);
      }
      GlobalMessage::MainToAvMsg (MainToAvMsg::Connecting) => {
        let slice_min = CONNECT_TO_STR_MAX_LEN;
        let slice_max = CONNECT_TO_STR_MAX_LEN + CONNECTING_STR_LEN;
        self.av_context.as_mut().unwrap().render.connect_to_tile_vertex_buffer
          .slice (slice_min..slice_max).unwrap()
          .write (&result_tiles (CONNECTING_STR)[..]);
        self.connection_state      = ConnectionState::Connecting;
        self.connecting_anim_start = time::Instant::now();
      }
      GlobalMessage::MainToAvMsg (MainToAvMsg::Failed) => {
        let slice_min = CONNECT_TO_STR_MAX_LEN;
        let slice_max = CONNECT_TO_STR_MAX_LEN + FAILED_STR_LEN;
        self.av_context.as_mut().unwrap().render.connect_to_tile_vertex_buffer
          .slice (slice_min..slice_max).unwrap()
          .write (&result_tiles (FAILED_STR)[..]);
        self.connection_state      = ConnectionState::Failed;
      }
      GlobalMessage::MainToAvMsg (MainToAvMsg::Refused) => {
        let slice_min = CONNECT_TO_STR_MAX_LEN;
        let slice_max = CONNECT_TO_STR_MAX_LEN + REFUSED_STR_LEN;
        self.av_context.as_mut().unwrap().render.connect_to_tile_vertex_buffer
          .slice (slice_min..slice_max).unwrap()
          .write (&result_tiles (REFUSED_STR)[..]);
        self.connection_state      = ConnectionState::Refused;
      }
      GlobalMessage::MainToAvMsg (MainToAvMsg::VersionMismatch) => {
        let slice_min = CONNECT_TO_STR_MAX_LEN;
        let slice_max = CONNECT_TO_STR_MAX_LEN + VERSION_MISMATCH_STR_LEN;
        self.av_context.as_mut().unwrap().render.connect_to_tile_vertex_buffer
          .slice (slice_min..slice_max).unwrap()
          .write (&result_tiles (VERSION_MISMATCH_STR)[..]);
        self.connection_state      = ConnectionState::VersionMismatch;
      }
      _ => unreachable!()
    }
    log::trace!("...AvThread handle message");
    control_flow

  }

  fn av_update (&mut self) -> apis::process::ControlFlow {
    use glium::Surface;
    log::trace!("AvThread update...");

    log::trace!("AvThread frame: {}", self.frame);

    // TODO: audio frame
    { // audio frame
      let audio_context = &mut self.av_context.as_mut().unwrap().audio;
      // fmod update
      rfmod_ok!(audio_context.rfmod_sys.update());
    }

    { // render frame
    let render_context = &mut self.av_context.as_mut().unwrap().render;
    let mut glium_frame = render_context.glium_display.draw();
    glium_frame.clear_all ((0.0, 0.0, 0.0, 1.0), 0.0, 0);

    { // draw bg sprite
      let program = &render_context
        .shader_programs [shader::ProgramId::ClipSpace2dSpriteArray as usize];
      glium_frame.draw (
        &render_context.bg_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_sampler_2d_array: render_context.bg_textures.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest),
        },
        &Default::default()
      ).unwrap();
    } // end draw bg sprite

    { // draw connection box background
      let program = &render_context
        .shader_programs [ shader::ProgramId::ScreenSpace2dSprite as usize];
      let uni_projection_mat_ortho : &[[f32; 4]; 4] =
        render_context.projection_mat_ortho.as_ref();

      glium_frame.draw (
        &render_context.connect_to_box_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_projection_mat_ortho: *uni_projection_mat_ortho,
          uni_sampler_2d: render_context.connect_to_box_texture.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest),
            //.anisotropy (0),
          //uni_frame:             self.frame as u32
        },
        &Default::default()
      ).unwrap();
    }

    { // draw connection box text tiles
      let program = &render_context
        .shader_programs [shader::ProgramId::ScreenSpace2dTile as usize];
      let uni_projection_mat_ortho : &[[f32; 4]; 4] =
        render_context.projection_mat_ortho.as_ref();
      // draw tiles

      // TODO: determine tile dimensions programmatically?
      const TILE_DIMENSIONS : [i32; 2] = [6, 9];
      let uni_viewport_dimensions = [
        render_context.resolution.x as i32,
        render_context.resolution.y as i32];

      let vert_max = match self.connection_state {
        ConnectionState::None       => CONNECT_TO_STR_MAX_LEN,
        ConnectionState::Connecting => {
          // animate dots (...)
          let elapsed_ms = self.connecting_anim_start.elapsed().as_millis()
            as u64;
          (CONNECT_TO_STR_MAX_LEN + CONNECTING_STR_LEN)
            - (CONNECTING_ANIM_FRAMES
              - (elapsed_ms / CONNECTING_ANIM_FRAME_MS) % CONNECTING_ANIM_FRAMES)
                  as usize + 1
        }
        ConnectionState::Failed     => CONNECT_TO_STR_MAX_LEN + FAILED_STR_LEN,
        ConnectionState::Refused    => CONNECT_TO_STR_MAX_LEN + REFUSED_STR_LEN,
        ConnectionState::VersionMismatch =>
          CONNECT_TO_STR_MAX_LEN + VERSION_MISMATCH_STR_LEN
      };

      glium_frame.draw (
        render_context.connect_to_tile_vertex_buffer.slice (0..vert_max)
          .unwrap(),
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_gui_scale:            render_context.gui_scale as i32,
          uni_viewport_dimensions:  uni_viewport_dimensions,
          uni_tile_dimensions:      TILE_DIMENSIONS,
          uni_projection_mat_ortho: *uni_projection_mat_ortho,
          uni_sampler_2d:           render_context.tilesets[
            thread::av::render::TilesetId::Default as usize
          ].texture.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest),
        },
        &Default::default()
      ).unwrap();
    }

    { // draw mouse pointer
      if render_context.last_mouse_position != render_context.mouse_position {
        let width  : u16 =
          render_context.mouse_pointer_texture.width() as u16
            * render_context.gui_scale as u16;
        let height : u16 =
          render_context.mouse_pointer_texture.height() as u16
            * render_context.gui_scale as u16;
        render_context.mouse_pointer_vertex_buffer.write (
          &[shader::Vert2dSprite {
            in_position:   [
              (render_context.mouse_position.x + width/2) as f32,
              (render_context.resolution.y as i32
                - render_context.mouse_position.y as i32
                - height as i32/2) as f32],
            in_dimensions: [width as f32, height as f32]
          }]
        );
        render_context.last_mouse_position = render_context.mouse_position;
      }
      let program = &render_context
        .shader_programs [ shader::ProgramId::ScreenSpace2dSprite as usize];
      let uni_projection_mat_ortho : &[[f32; 4]; 4] =
        render_context.projection_mat_ortho.as_ref();

      glium_frame.draw (
        &render_context.mouse_pointer_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_projection_mat_ortho: *uni_projection_mat_ortho,
          uni_sampler_2d: render_context.mouse_pointer_texture.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest),
            //.anisotropy (0),
          //uni_frame:             self.frame as u32
        },
        &Default::default()
      ).unwrap();
    } // end draw mouse pointer

    glium_frame.finish().unwrap();
    } // end render frame

    self.av_context.as_mut().unwrap().do_frame();

    self.frame += 1;

    log::trace!("...AvThread update");
    apis::process::ControlFlow::Continue

  } // end fn av_update

} // end impl AvThread

impl NetThread {
  fn net_handle_message (&mut self, message : GlobalMessage)
    -> apis::process::ControlFlow
  {
    log::trace!("NetThread handle message...");
    #[allow(unused_assignments)]
    let mut control_flow = apis::process::ControlFlow::Continue;
    match message {
      GlobalMessage::MainToNetMsg (MainToNetMsg::ConnectTo (address)) => {
        {
          let net = self.net_mut();
          const CHANNEL_COUNT : u8  = 2;
          const NODATA        : u32 = 0;
          let server_peer = net.enet_host
            .connect (&address, CHANNEL_COUNT, NODATA).unwrap();
          log::debug!("server peer: {:?}", server_peer);
          net.server_peer = Some (server_peer);
        }
        self.timeout = Some (time::Instant::now() + CONNECT_TIMEOUT_DUR);
      }
      GlobalMessage::MainToNetMsg (MainToNetMsg::Quit) => {
        let net = self.net_mut();
        // NB: this is how the network thread decides if the next mode is
        // the main title (connection not yet accepted) or starting multiplayer
        // (connected as some client)
        if net.connected_as.is_none() {
          if let Some (server_peer) = net.server_peer.take() {
            server_peer.disconnect();
            net.enet_host.flush();
          }
        }
        control_flow = apis::process::ControlFlow::Break;
      }
      _ => unreachable!()
    }
    log::trace!("...NetThread handle message");
    control_flow
  }
  fn net_update (&mut self) -> apis::process::ControlFlow {
    let control_flow = apis::process::ControlFlow::Continue;
    log::trace!("NetThread update...");
    if self.net().server_peer.is_some() {
      if self.timeout.is_none() {
        // no timeout indicates connection is established:
        // send pings at regular intervals
        if let Ok (ts) = self.net().timer.is_ping_time() {
          // only send ping if pong was received for the last ping
          if self.net().timer.pong_count() == self.net().timer.ping_count() {
            let ping_timestamp      = *ts;
            let last_pong_timestamp = *self.net().timer.last_pong_timestamp();
            let sequence            = self.net().timer.ping_count();
            let step                = thread::sim::STEP
              .load (std::sync::atomic::Ordering::SeqCst);
            let msg = client::Message::Ping {
              sequence, step, ping_timestamp, last_pong_timestamp
            };
            self.net_mut().send (msg);
            self.net_mut().enet_host.flush();
            self.net_mut().timer.ping (ts);
            log::debug!("NetThread: sent Ping [{}] with timestamp: {:?}",
              sequence, ping_timestamp);
          }
        }
      }
      if let Some (message) = self.service() {
        use apis::Process;
        self.send (ChannelId::NetToMain, message).unwrap();
      }
    } else {
      std::thread::sleep (*thread::net::SERVICE_DUR);
    }
    log::trace!("...NetThread update");
    control_flow
  } // end fn net_update

  /// Should only be called when `self.server_peer` is `Some`
  fn service (&mut self) -> Option <NetToMainMsg> {
    log::trace!("NetThread service...");
    debug_assert!(self.net().server_peer.is_some());
    let t_now = time::Instant::now();
    if let Some (timeout) = self.timeout {
      if t_now > timeout {
        self.timeout = None;
        let net = self.net_mut();
        let server_peer = net.server_peer.take().unwrap();
        server_peer.disconnect();
        net.enet_host.flush();
        return Some (NetToMainMsg::ConnectionFailed)
      }
    }
    let out = match self.net_mut().enet_host
      .service (*thread::net::SERVICE_MS)
    {
      Err (host_error) => {
        log::warn!("NetThread: host service error while connecting: {:?}",
          host_error);
        self.timeout = None;
        self.net_mut().server_peer = None;
        Some (NetToMainMsg::ConnectionFailed)
      }
      Ok (None) => None,
      Ok (Some (enet_event)) => {
        match enet_event {
          enet::Event::Connect {..} => {
            // do nothing: wait until server sends "connection established"
            None
          },
          enet::Event::Receive { peer: _peer, packet, .. } => {
            let t_now  = time::Instant::now();
            let ts_now = self.net().timer
              .timestamp (t_now);
            log::trace!("NetThread: received packet: {:#?}", packet);
            // TODO: handle unwrap failure?
            let message : server::Message =
              bincode::deserialize (packet.data()).unwrap();
            log::trace!("NetThread: deserialized message: {:#?}", message);
            match message {
              server::Message::ConnectionEstablished {
                ping_rtt_window_size,
                version
              } => {
                log::info!("NetThread: connection established");
                if VERSION != version {
                  log::info!("NetThread: disconnecting: client version \
                    {:?} != server version {:?}", VERSION, version);
                  let server_peer = self.net_mut().server_peer.take().unwrap();
                  server_peer.disconnect();
                  self.net_mut().enet_host.flush();
                  Some (NetToMainMsg::VersionMismatch)
                } else {
                  self.net_mut().timer
                    .set_ping_rtt_window_size (ping_rtt_window_size);
                  self.timeout = None;  // clearing timeout will start ping/pong
                  None
                }
              }
              server::Message::ConnectionRefused (refused) => {
                log::info!("NetThread: connection refused: {:?}", refused);
                self.timeout = None;
                self.net_mut().server_peer = None;
                Some (NetToMainMsg::ConnectionRefused)
              }
              server::Message::Pong { sequence, step } => {
                debug_assert_eq!(sequence, self.net().timer.pong_count());
                self.net_mut().timer.pong (ts_now);
                log::info!("received Pong[{}] server step[{}] avg RTT ms: {}",
                  sequence, step, self.net().timer.average_ping_rtt_ms());
                None
              }
              server::Message::ConnectionAccepted (client_id, simulation) => {
                let init = {
                  let net = self.net_mut();
                  net.connected_as   = Some (client_id);
                  let latency_avg_ms = net.timer.average_ping_rtt_ms() as u64
                    / 2;
                  ( t_now - time::Duration::from_millis (latency_avg_ms),
                    simulation
                  )
                };
                {
                  let mut simulation_init = thread::SIMULATION_INIT.lock()
                    .unwrap();
                  *simulation_init = Some (init);
                }
                Some (NetToMainMsg::ConnectionAccepted (client_id))
              }
              _ => unreachable!("ConnectTo service: unexpected message")
            }
          }
          enet::Event::Disconnect {..}  => {
            self.net_mut().server_peer = None;
            self.timeout = None;
            Some (NetToMainMsg::ConnectionFailed)
          }
        }
      }
    };
    log::trace!("...NetThread service");
    out
  }

  /// Unwraps the `Net` context
  fn net (&self) -> &thread::Net {
    self.net_context.as_ref().unwrap()
  }
  /// Unwraps the `Net` context
  fn net_mut (&mut self) -> &mut thread::Net {
    self.net_context.as_mut().unwrap()
  }

} // end impl NetThread

impl Default for MultiSetupResult {
  fn default() -> Self {
    MultiSetupResult::QuitToTitle
  }
}

impl ConnectTo {
  /// String of extended ASCII bytes
  pub fn to_bytes (&self) -> Vec <u8> {
    let string = match (
      self.a1.as_ref(),
      self.a2.as_ref(),
      self.a3.as_ref(),
      self.a4.as_ref(),
      self.port.as_ref()
    ) {
      (None, None, None, None, None)      => " ._._._:_".to_string(),
      (Some (a1), None, None, None, None) => {
        if a1.len() == 3 ||
          u8::from_str_radix (a1.as_str(), 10).unwrap() == 0 ||
          self.skip
        {
          format!("{}. ._._:_", a1)
        } else {
          format!("{} ._._._:_", a1)
        }
      }
      (Some (a1), Some (a2), None, None, None) => {
        if a2.len() == 3 ||
          u8::from_str_radix (a2.as_str(), 10).unwrap() == 0 ||
          self.skip
        {
          format!("{}.{}. ._:_", a1, a2)
        } else {
          format!("{}.{} ._._:_", a1, a2)
        }
      }
      (Some (a1), Some (a2), Some (a3), None, None) => {
        if a3.len() == 3 ||
          u8::from_str_radix (a3.as_str(), 10).unwrap() == 0 ||
          self.skip
        {
          format!("{}.{}.{}. :_", a1, a2, a3)
        } else {
          format!("{}.{}.{} ._:_", a1, a2, a3)
        }
      }
      (Some (a1), Some (a2), Some (a3), Some (a4), None) => {
        if a4.len() == 3 ||
          u8::from_str_radix (a4.as_str(), 10).unwrap() == 0 ||
          self.skip
        {
          format!("{}.{}.{}.{}: ", a1, a2, a3, a4)
        } else {
          format!("{}.{}.{}.{} :_", a1, a2, a3, a4)
        }
      }
      (Some (a1), Some (a2), Some (a3), Some (a4), Some (port)) => {
        debug_assert_eq!(self.skip, false);
        format!("{}.{}.{}.{}:{} ", a1, a2, a3, a4, port)
      }
      _ => unreachable!()
    };
    let mut bytes = string.into_bytes();
    for b in &mut bytes[..] {
      if *b == ' ' as u8 {
        *b = 0xDB;
        break;
      }
    }
    bytes
  }

  pub fn try_insert_digit (&mut self, digit : u32) {
    fn insert_digit (
      current : &mut String, next : &mut Option <String>,
      skip : &mut bool, digit_string : String,
    ) {
      debug_assert!(next.is_none());
      if current.len() < 3 &&
        u8::from_str_radix (current.as_str(), 10).unwrap() > 0 &&
        !*skip
      {
        let mut try_current = current.clone();
        try_current.push_str (digit_string.as_str());
        if u16::from_str_radix (try_current.as_str(), 10).unwrap()
          <= std::u8::MAX as u16
        {
          *current = try_current;
        } else {
          *next = Some (digit_string);
          *skip = false;
        }
      } else {
        *next = Some (digit_string);
        *skip = false;
      }
    }
    let digit_string = digit.to_string();
    match (
      &mut self.a1,
      &mut self.a2,
      &mut self.a3,
      &mut self.a4,
      &mut self.port
    ) {
      (a1 @ None, None, None, None, None) => {
        debug_assert_eq!(self.skip, false);
        *a1 = Some (digit_string);
      }
      (Some (a1), ref mut a2 @ None, None, None, None) => {
        insert_digit (a1, a2, &mut self.skip, digit_string);
      }
      (Some (_), Some (a2), ref mut a3 @ None, None, None) => {
        insert_digit (a2, a3, &mut self.skip, digit_string);
      }
      (Some (_), Some (_), Some (a3), ref mut a4 @ None, None) => {
        insert_digit (a3, a4, &mut self.skip, digit_string);
      }
      (Some (_), Some (_), Some (_), Some (a4), ref mut port @ None) => {
        insert_digit (a4, port, &mut self.skip, digit_string);
      }
      (Some (_), Some (_), Some (_), Some (_), Some (port)) => {
        debug_assert_eq!(self.skip, false);
        if port.len() < 5 &&
          u16::from_str_radix (port.as_str(), 10).unwrap() > 0
        {
          let mut try_port = port.clone();
          try_port.push_str (digit_string.as_str());
          if u32::from_str_radix (try_port.as_str(), 10).unwrap()
            <= std::u16::MAX as u32
          {
            *port = try_port;
          }
        }
      }
      _ => unreachable!()
    }
  }

  pub fn try_erase_digit (&mut self) {
    fn erase_digit (current : &mut Option <String>, skip : &mut bool) {
      current.as_mut().unwrap().pop();
      if current.as_ref().unwrap().is_empty() {
        *current = None;
        *skip    = true;
      } else {
        *skip = false;
      }
    }
    match (
      &mut self.a1,
      &mut self.a2,
      &mut self.a3,
      &mut self.a4,
      &mut self.port
    ) {
      (   None,    None,    None,    None, None) => {}
      (     a1,    None,    None,    None, None) => {
        erase_digit (a1, &mut self.skip);
        self.skip = false;  // we actually want this to stay false in this case
      },
      (Some(_),      a2,    None,    None, None) =>
        erase_digit (a2,   &mut self.skip),
      (Some(_), Some(_),      a3,    None, None) =>
        erase_digit (a3,   &mut self.skip),
      (Some(_), Some(_), Some(_),      a4, None) =>
        erase_digit (a4,   &mut self.skip),
      (Some(_), Some(_), Some(_), Some(_), port) =>
        erase_digit (port, &mut self.skip),
      _ => unreachable!()
    }
  }
  pub fn try_skip_digit (&mut self) {
    match (&self.a1, &self.a2, &self.a3, &self.a4, &self.port) {
      (   None,    None,    None,    None,    None) => {}
      (Some(_), Some(_), Some(_), Some(_), Some(_)) => {}
      _ => { self.skip = true; }
    }
  }
}

impl Default for ConnectionState {
  fn default() -> Self {
    ConnectionState::None
  }
}
