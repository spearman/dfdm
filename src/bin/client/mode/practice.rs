// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use std::{self, time};
use {cgmath, rfmod};
use glium::{self, glutin, uniform};
use lazy_static::lazy_static;
use rand_xorshift::XorShiftRng;
use serde::{Deserialize, Serialize};
use vec_map::VecMap;

use apis;
use fmod_utils::rfmod_ok;

use dfdm::{simulation, Simulation};
use dfdm::simulation::world::{player, Player};

use crate::geometry;
use crate::thread::{self, main::input::keyboard};

////////////////////////////////////////////////////////////////////////////////
//  constants                                                                 //
////////////////////////////////////////////////////////////////////////////////

pub const PLAYER_ID        : player::Id = player::Id (0);
const HISTORY_SIZE         : usize = 2;
const HISTORY_FUTURE_STEPS : usize = 1;

////////////////////////////////////////////////////////////////////////////////
//  statics                                                                   //
////////////////////////////////////////////////////////////////////////////////

lazy_static!{
  pub static ref STATIC_CONFIG    : &'static StaticConfig =
    &crate::STATIC_CONFIG.mode_config.practice_config;
  pub static ref STATIC_INPUT_MAP : VecMap <InputFun> = {
    let mut v = VecMap::new();
    for (keycode, input_fun) in STATIC_CONFIG.input_map.iter() {
      assert!(v.insert (*keycode as usize, *input_fun).is_none());
    }
    v
  };
  pub static ref DEBUG_GRID_COLOR : [f32; 4] = {
    let (r, g, b, a) = crate::STATIC_CONFIG.thread_config.av_config
      .debug_grid_color;
    [ (r as f32) / 255.0,
      (g as f32) / 255.0,
      (b as f32) / 255.0,
      (a as f32) / 255.0
    ]
  };
  pub static ref DEBUG_SPHERE_COLOR : [f32; 4] = {
    let (r, g, b, a) = crate::STATIC_CONFIG.thread_config.av_config
      .debug_sphere_color;
    [ (r as f32) / 255.0,
      (g as f32) / 255.0,
      (b as f32) / 255.0,
      (a as f32) / 255.0
    ]
  };
  pub static ref DEBUG_CYLINDER_COLOR : [f32; 4] = {
    let (r, g, b, a) = crate::STATIC_CONFIG.thread_config.av_config
      .debug_cylinder_color;
    [ (r as f32) / 255.0,
      (g as f32) / 255.0,
      (b as f32) / 255.0,
      (a as f32) / 255.0
    ]
  };
  pub static ref DEBUG_CAPSULE_COLOR : [f32; 4] = {
    let (r, g, b, a) = crate::STATIC_CONFIG.thread_config.av_config
      .debug_capsule_color;
    [ (r as f32) / 255.0,
      (g as f32) / 255.0,
      (b as f32) / 255.0,
      (a as f32) / 255.0
    ]
  };
}

////////////////////////////////////////////////////////////////////////////////
//  config                                                                    //
////////////////////////////////////////////////////////////////////////////////

#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  /// Set to zero to disable
  pub debug_steps             : u64,
  pub player_initial_position : [f64; 3],
  pub player_initial_heading  : u16,  // 0-360
  /// Bots beyond the provided defaults will be randomly generated
  pub bot_count               : u8,
  pub bots_initial_positions_headings_classes :
    Vec <([f64; 3], u16, player::Class)>,
  pub input_map : std::collections::HashMap <keyboard::Keycode, InputFun>
}

////////////////////////////////////////////////////////////////////////////////
//  session                                                                   //
////////////////////////////////////////////////////////////////////////////////

//
//  mode Practice
//
apis::def_session! {
  context Practice {
    PROCESSES where
      let proc       = self,
      let message_in = message_in
    [

      //
      //  process MainThread
      //
      process MainThread (
        main_context      : Option <thread::Main> = None,
        // keeps track of whether right mouse button is pressed
        lock_mouselook    : bool = false,
        input_map_keydown : VecMap <InputFun>
          = STATIC_INPUT_MAP.clone(),
        input_map_keyup   : VecMap <InputFun> = VecMap::new()
      ) -> (PracticeResult) {
        kind           {
          apis::process::Kind::Mesochronous {
            tick_ms:          *thread::main::INPUT_MS,
            ticks_per_update: 1
          }
        }
        sourcepoints   [MainToAv, MainToSimulation]
        endpoints      [ ]
        handle_message { unreachable!() }
        update         { proc.main_update() }
      }

      //
      //  process AvThread
      //
      process AvThread (
        av_context          : Option <Box <thread::Av>> = None,
        rfmod_channel_music : Option <rfmod::Channel>   = None,
        frame               : u64 = 0
      ) {
        kind           { apis::process::Kind::Anisochronous }
        sourcepoints   [ ]
        endpoints      [MainToAv, SimulationToAv]
        initialize     { proc.av_init() }
        terminate      { proc.av_term() }
        handle_message { proc.av_handle_message (message_in) }
        update         { proc.av_update() }
      }

      //
      //  process SimulationThread
      //
      process SimulationThread (
        simulation_context : Simulation = Simulation::new (
          simulation::STATIC_CONFIG.clone(),
          HISTORY_SIZE, HISTORY_FUTURE_STEPS,
          ( 0,
            simulation::World::create (&*simulation::STATIC_CONFIG),
            Vec::new()
          )
        ),
        rng                : XorShiftRng = rand::SeedableRng::seed_from_u64 (0),
        bot_move_states    : Vec <player::MoveState>,
        quit               : bool = false
      ) {
        kind           { apis::process::Kind::Anisochronous }
        sourcepoints   [SimulationToAv]
        endpoints      [MainToSimulation]
        initialize     { proc.simulation_init() }
        handle_message { proc.simulation_handle_message (message_in) }
        update         { proc.simulation_update() }
      }
    ]
    CHANNELS [
      channel MainToAv <MainToAvMsg> (Simplex) {
        producers [MainThread]
        consumers [AvThread]
      }
      channel MainToSimulation <MainToSimulationMsg> (Simplex) {
        producers [MainThread]
        consumers [SimulationThread]
      }
      channel SimulationToAv <SimulationToAvMsg> (Simplex) {
        producers [SimulationThread]
        consumers [AvThread]
      }
    ]
    MESSAGES [
      message MainToAvMsg {
        Default (thread::MainToAvMsg)
      }
      message MainToSimulationMsg {
        Command (simulation::Command),
        Quit
      }
      message SimulationToAvMsg {
        PlayerSpawn     (u64, player::Id, Player),
        PlayerUpdate    (u64, player::Id, Player),
        PlayerView      (player::Id),
        Corpses         (Vec <cgmath::Point3 <f32>>),
        ViewPosition    (cgmath::Point3 <f32>),
        ViewOrientation (cgmath::Basis3 <f32>),
        Quit
      }
    ]
    main: MainThread
  }
} // end mode Practice

////;///////////////////////////////////////////////////////////////////////////
//  input functions                                                           //
////////////////////////////////////////////////////////////////////////////////

impl_input_fun! {
  MainThread {
    MainThread::screenshot,           Screenshot
    MainThread::quit_to_title,        QuitToTitle
    MainThread::quit_to_system,       QuitToSystem
    MainThread::player_move_forward,  PlayerMoveForward
    MainThread::player_move_backward, PlayerMoveBackward
    MainThread::player_move_left,     PlayerMoveLeft
    MainThread::player_move_right,    PlayerMoveRight
    MainThread::player_move_up,       PlayerMoveUp
    MainThread::player_move_down,     PlayerMoveDown
    MainThread::player_turn_left,     PlayerTurnLeft
    MainThread::player_turn_right,    PlayerTurnRight
    MainThread::player_turn_up,       PlayerTurnUp
    MainThread::player_turn_down,     PlayerTurnDown
    MainThread::player_look_center,   PlayerLookCenter
    MainThread::player_jump,          PlayerJump
    MainThread::player_walk,          PlayerWalk
    MainThread::player_run,           PlayerRun
  }
}

////////////////////////////////////////////////////////////////////////////////
//  enums                                                                     //
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone,Debug,Eq,PartialEq)]
pub enum PracticeResult {
  QuitToTitle,
  QuitToSystem
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

//
//  impl MainThread
//
impl MainThread {
  /*
  pub fn main_init(&mut self) {
    //let gui_context = &self.main_context.as_ref().unwrap().gui;
  }
  */

  //
  //  process behavior methods
  //

  fn main_update (&mut self) -> apis::process::ControlFlow {
    use colored::Colorize;
    use glutin::platform::desktop::EventLoopExtDesktop;
    log::trace!("MainThread update...");
    let mut control_flow = apis::process::ControlFlow::Continue;

    // glutin poll input events
    let mut event_loop = self.main_context.as_mut().unwrap().input.event_loop
      .take().unwrap();
    event_loop.run_return (|event, _, event_loop_control_flow| {
      use glutin::event::{self, Event};
      *event_loop_control_flow = glutin::event_loop::ControlFlow::Poll;
      log::trace!("{}: {:?}", "glutin event".to_string().cyan().bold(), event);
      match event {
        Event::MainEventsCleared => {
          *event_loop_control_flow = glutin::event_loop::ControlFlow::Exit;
        }
        Event::WindowEvent { event, .. } => match event {
          // closed
          event::WindowEvent::CloseRequested |
          event::WindowEvent::Destroyed      => {
            log::info!("glutin WindowEvent::Closed: quitting to system...");
            // always returns Some(Break)
            control_flow = self.quit_to_system().control_flow.unwrap();
          }
          // resize
          event::WindowEvent::Resized (physical_size) => {
            use apis::Process;
            let (x, y) = { // borrow gui
              let gui = &mut self.main_context.as_mut().unwrap().gui;
              let (width, height) = {
                let (x, y) : (u32, u32) = physical_size.into();
                debug_assert!(x <= std::u16::MAX as u32);
                debug_assert!(y <= std::u16::MAX as u32);
                (x as u16, y as u16)
              };
              gui.set_screen_resolution (width, height);
              (width, height)
            };
            self.send (
              ChannelId::MainToAv,
              MainToAvMsg::Default (
                thread::MainToAvMsg::WindowSize { x, y })
            ).unwrap();
          }
          // keyboard input
          event::WindowEvent::KeyboardInput {
            input: event::KeyboardInput { state, scancode, virtual_keycode, .. },
            ..
          } => match state {
            event::ElementState::Pressed => if let Some (virtual_keycode) =
              virtual_keycode
            {
              let keycode : keyboard::Keycode =
                virtual_keycode.into();
              // NOTE: glutin does not specify whether this is a repeating
              // keypress so we check the keyup map to see if the key is
              // already pressed
              if !self.input_map_keyup.contains_key (keycode as usize) {
                let maybe_input_fun =
                  self.input_map_keydown.get (keycode as usize).and_then (
                    |input_fun| Some (*input_fun));
                if let Some (input_fun) = maybe_input_fun {
                  let input_fun_result = input_fun (self);
                  if let Some (input_fun_keyup) =
                    input_fun_result.input_fun_keyup
                  {
                    assert!(self.input_map_keyup
                      .insert (keycode as usize, input_fun_keyup).is_none());
                  }
                  control_flow = input_fun_result.control_flow
                    .unwrap_or (apis::process::ControlFlow::Continue);
                }
              }
            } else {
              // TODO: temporary screenshot code since glutin does not produce
              // virtual keycode for Snapshot (PrtScn) key
              if scancode == 99 {
                let _ = self.screenshot();
              }
            } // end Pressed
            event::ElementState::Released => if let Some (virtual_keycode) =
              virtual_keycode
            {
              let keycode : keyboard::Keycode =
                virtual_keycode.into();
              let maybe_input_fun =
                self.input_map_keyup.remove (keycode as usize).and_then (
                  |input_fun| Some (*input_fun));
              if let Some (input_fun) = maybe_input_fun {
                let input_fun_result = input_fun (self);
                control_flow = input_fun_result.control_flow
                  .unwrap_or (apis::process::ControlFlow::Continue);
              }
            } // end Released
          } // end KeyboardInput
          // mouse input
          event::WindowEvent::MouseInput { state, button, .. } => match state {
            // mouse button pressed
            event::ElementState::Pressed  => match button {
              event::MouseButton::Right => {
                debug_assert!(!self.lock_mouselook);
                self.lock_mouselook = true;
              }
              _ => {}
            } // end Pressed
            // mouse button released
            event::ElementState::Released => match button {
              event::MouseButton::Right => {
                debug_assert!(self.lock_mouselook);
                self.lock_mouselook = false;
              }
              _ => {}
            } // end Released
          } // end MouseInput
          _ => ()
        } // end WindowEvent
        Event::DeviceEvent { event, .. } => match event {
          // mouse motion
          event::DeviceEvent::MouseMotion { delta: (xrel, yrel) } => {
            if !self.lock_mouselook ||
               !crate::STATIC_CONFIG.lock_mouse_when_attacking
            {
              let sensitivity = crate::STATIC_CONFIG.mouselook_sensitivity;
              self.player_rotate (
                cgmath::Rad ( sensitivity * xrel as f64),
                cgmath::Rad (-sensitivity * yrel as f64)
              );
            }
            if self.lock_mouselook {
              use std::f64::consts::*;
              use cgmath::InnerSpace;
              // attack only if attack sensitivity threshold is exceeded
              let magnitude =
                cgmath::vec2 (xrel as f64, yrel as f64).magnitude();
              if crate::STATIC_CONFIG.attack_sensitivity < magnitude {
                // choose melee attack cycle based on mouse motion direction
                let angle = (xrel as f64).atan2 (-yrel as f64);
                debug_assert!(angle.abs() <= PI);
                if -FRAC_PI_6 <= angle && angle <= FRAC_PI_6 {
                  self.player_melee_stab();
                } else if FRAC_PI_6 <= angle && angle <= FRAC_PI_2 {
                  self.player_melee_slash_high_right();
                } else if FRAC_PI_2 <= angle && angle <= 5.0*FRAC_PI_6 {
                  self.player_melee_slash_low_right();
                } else if -FRAC_PI_2 <= angle && angle <= -FRAC_PI_6 {
                  self.player_melee_slash_high_left();
                } else if -5.0*FRAC_PI_6 < angle && angle <= -FRAC_PI_2 {
                  self.player_melee_slash_low_left();
                } else {
                  debug_assert!(angle <= -5.0*FRAC_PI_6 || 5.0*FRAC_PI_6 <= angle);
                  self.player_melee_chop();
                }
              }
            }
          } // end DeviceEvent::MouseMotion
          _ => {}

        } // end DeviceEvent
        //Event::Awakened
        //Event::Suspended (b)
        _ => ()
      } // end match glutin event
    }); // end glutin poll input events
    debug_assert!(self.main_context.as_mut().unwrap().input.event_loop
      .is_none());
    self.main_context.as_mut().unwrap().input.event_loop = Some (event_loop);

    log::trace!("...MainThread update");

    control_flow
  } // end fn main_update

  //
  //  input functions
  //

  pub fn screenshot (&mut self) -> InputFunResult {
    use apis::Process;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Screenshot)
    ).unwrap();
    InputFunResult::default()
  }

  pub fn quit_to_title (&mut self) -> InputFunResult {
    use apis::Process;
    *self.result_mut() = PracticeResult::QuitToTitle;
    // TODO: this is kind of kludgy, this reset flag is not actually used
    self.send (ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Quit {
        reset_audio_on_term: true
      })
    ).unwrap();
    self.send (ChannelId::MainToSimulation, MainToSimulationMsg::Quit).unwrap();
    InputFunResult {
      control_flow: Some (apis::process::ControlFlow::Break),
      .. InputFunResult::default()
    }
  }

  pub fn quit_to_system (&mut self) -> InputFunResult {
    use apis::Process;
    *self.result_mut() = PracticeResult::QuitToSystem;
    // TODO: this is kind of kludgy, this reset flag is not actually used
    self.send (ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Quit {
        reset_audio_on_term: true
      })
    ).unwrap();
    self.send (ChannelId::MainToSimulation, MainToSimulationMsg::Quit).unwrap();
    InputFunResult {
      control_flow: Some (apis::process::ControlFlow::Break),
      .. InputFunResult::default()
    }
  }

  pub fn player_move_forward (&mut self) -> InputFunResult {
    let direction = player::MoveState::default().move_forward();
    let command   = simulation::Command::Player (
      PLAYER_ID, player::Command::Move (direction));
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerMoveBackward),
      .. InputFunResult::default()
    }
  }

  pub fn player_move_backward (&mut self) -> InputFunResult {
    let direction = player::MoveState::default().move_backward();
    let command   = simulation::Command::Player (
      PLAYER_ID, player::Command::Move (direction));
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerMoveForward),
      .. InputFunResult::default()
    }
  }

  pub fn player_move_left (&mut self) -> InputFunResult {
    let direction = player::MoveState::default().move_left();
    let command   = simulation::Command::Player (
      PLAYER_ID, player::Command::Move (direction));
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerMoveRight),
      .. InputFunResult::default()
    }
  }

  pub fn player_move_right (&mut self) -> InputFunResult {
    let direction = player::MoveState::default().move_right();
    let command   = simulation::Command::Player (
      PLAYER_ID, player::Command::Move (direction));
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerMoveLeft),
      .. InputFunResult::default()
    }
  }

  pub fn player_move_up (&mut self) -> InputFunResult {
    let direction = player::MoveState::default().move_up();
    let command   = simulation::Command::Player (
      PLAYER_ID, player::Command::Move (direction));
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerMoveDown),
      .. InputFunResult::default()
    }
  }

  pub fn player_move_down (&mut self) -> InputFunResult {
    let direction = player::MoveState::default().move_down();
    let command   = simulation::Command::Player (
      PLAYER_ID, player::Command::Move (direction));
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerMoveUp),
      .. InputFunResult::default()
    }
  }

  pub fn player_turn_left (&mut self) -> InputFunResult {
    let direction = player::TurnState::default().turn_left();
    let command   = simulation::Command::Player (
      PLAYER_ID, player::Command::Turn (direction));
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerTurnRight),
      .. InputFunResult::default()
    }
  }

  pub fn player_turn_right (&mut self) -> InputFunResult {
    let direction = player::TurnState::default().turn_right();
    let command   = simulation::Command::Player (
      PLAYER_ID, player::Command::Turn (direction));
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerTurnLeft),
      .. InputFunResult::default()
    }
  }

  pub fn player_turn_up (&mut self) -> InputFunResult {
    let direction = player::TurnState::default().turn_up();
    let command   = simulation::Command::Player (
      PLAYER_ID, player::Command::Turn (direction));
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerTurnDown),
      .. InputFunResult::default()
    }
  }

  pub fn player_turn_down (&mut self) -> InputFunResult {
    let direction = player::TurnState::default().turn_down();
    let command   = simulation::Command::Player (
      PLAYER_ID, player::Command::Turn (direction));
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerTurnUp),
      .. InputFunResult::default()
    }
  }

  pub fn player_look_center (&mut self) -> InputFunResult {
    let command = simulation::Command::Player (
      PLAYER_ID, player::Command::SetOrient (None, Some (cgmath::Rad (0.0))));
    self.send_simulation_command (command);
    InputFunResult::default()
  }

  pub fn player_jump (&mut self) -> InputFunResult {
    let command =
      simulation::Command::Player (PLAYER_ID, player::Command::Jump);
    self.send_simulation_command (command);
    InputFunResult::default()
  }

  pub fn player_walk (&mut self) -> InputFunResult {
    let command =
      simulation::Command::Player (PLAYER_ID, player::Command::Walk);
    self.send_simulation_command (command);
    InputFunResult {
      input_fun_keyup: Some (InputFun::PlayerRun),
      .. InputFunResult::default()
    }
  }

  pub fn player_run (&mut self) -> InputFunResult {
    let command =
      simulation::Command::Player (PLAYER_ID, player::Command::Run);
    self.send_simulation_command (command);
    InputFunResult::default()
  }

  pub fn player_melee_chop (&mut self) -> InputFunResult {
    let attack_kind = player::MeleeKind::Chop.into();
    let command     = simulation::Command::Player (
      PLAYER_ID, player::Command::Attack (attack_kind));
    self.send_simulation_command (command);
    InputFunResult::default()
  }

  pub fn player_melee_stab (&mut self) -> InputFunResult {
    let attack_kind = player::MeleeKind::Stab.into();
    let command     = simulation::Command::Player (
      PLAYER_ID, player::Command::Attack (attack_kind));
    self.send_simulation_command (command);
    InputFunResult::default()
  }

  pub fn player_melee_slash_high_right (&mut self) -> InputFunResult {
    let attack_kind = player::MeleeKind::SlashHighRight.into();
    let command     = simulation::Command::Player (
      PLAYER_ID, player::Command::Attack (attack_kind));
    self.send_simulation_command (command);
    InputFunResult::default()
  }

  pub fn player_melee_slash_high_left (&mut self) -> InputFunResult {
    let attack_kind = player::MeleeKind::SlashHighLeft.into();
    let command     = simulation::Command::Player (
      PLAYER_ID, player::Command::Attack (attack_kind));
    self.send_simulation_command (command);
    InputFunResult::default()
  }

  pub fn player_melee_slash_low_right (&mut self) -> InputFunResult {
    let attack_kind = player::MeleeKind::SlashLowRight.into();
    let command     = simulation::Command::Player (
      PLAYER_ID, player::Command::Attack (attack_kind));
    self.send_simulation_command (command);
    InputFunResult::default()
  }

  pub fn player_melee_slash_low_left (&mut self) -> InputFunResult {
    let attack_kind = player::MeleeKind::SlashLowLeft.into();
    let command     = simulation::Command::Player (
      PLAYER_ID, player::Command::Attack (attack_kind));
    self.send_simulation_command (command);
    InputFunResult::default()
  }

  //
  //  other functions
  //

  /// Used for mouselook.
  pub fn player_rotate (&mut self,
    delta_yaw : cgmath::Rad <f64>, delta_pitch : cgmath::Rad <f64>
  ) {
    let command = simulation::Command::Player (
      PLAYER_ID, player::Command::Rotate (delta_yaw, delta_pitch));
    self.send_simulation_command (command);
  }

  /// Returns the step that was attached to the command
  #[inline]
  fn send_simulation_command (&mut self, command : simulation::Command) {
    use apis::Process;
    self.send (
      ChannelId::MainToSimulation, MainToSimulationMsg::Command (command)
    ).unwrap();
  }

} // end impl MainThread

//
//  impl AvThread
//
impl AvThread {
  fn av_init (&mut self) {
    { // render
    use cgmath::Rotation3;
    let render_context = &mut self.av_context.as_mut().unwrap().render;
    // TODO: is the following initial view setup necessary ?
    // initial view position and orientation from player
    render_context.view_position =
      cgmath::Point3::<f64>::from (STATIC_CONFIG.player_initial_position)
        .cast().unwrap();
    let yaw = cgmath::Rad::from (
      cgmath::Deg (STATIC_CONFIG.player_initial_heading as f32));
    render_context.view_orientation = cgmath::Basis3::from_angle_y (yaw);

    render_context.frame_time       = time::Instant::now();
    } // end render

    { // audio
    let audio_context = &mut self.av_context.as_mut().unwrap().audio;
    // start playing fight music
    self.rfmod_channel_music = {
      let rfmod_channel_music = audio_context.sound_music_deathmatch.play()
        .unwrap();
      Some (rfmod_channel_music)
    };
    } // end audio
  }

  fn av_term (&mut self) {
    {
      let render_context = &mut self.av_context.as_mut().unwrap().render;
      // clear player data
      render_context.view_player = None;
      render_context.player_animation_state.clear();
      render_context.player_vertex_data.clear();
    }
    // stop playing fight music
    rfmod_ok!(self.rfmod_channel_music.as_ref().unwrap().stop());
  }

  fn av_handle_message (&mut self, _message : GlobalMessage)
    -> apis::process::ControlFlow
  {
    log::trace!("AvThread handle message...");
    #[allow(unused_assignments)]
    let mut control_flow = apis::process::ControlFlow::Continue;
    match _message {
      //
      //  MainToAvMsg
      //
      GlobalMessage::MainToAvMsg (main_to_av_msg) => {
        match main_to_av_msg {
          // window size
          MainToAvMsg::Default (thread::MainToAvMsg::WindowSize { x, y }) => {
            self.av_context.as_mut().unwrap().render.set_resolution (x,y);
          }
          // mouse position
          MainToAvMsg::Default (thread::MainToAvMsg::MousePosition { x, y }) => {
            self.av_context.as_mut().unwrap().render.mouse_position =
              (x,y).into();
          }
          // quit
          MainToAvMsg::Default (thread::MainToAvMsg::Quit {..}) => {
            control_flow = apis::process::ControlFlow::Break;
          }
          // screenshot
          MainToAvMsg::Default (thread::MainToAvMsg::Screenshot) => {
            self.av_context.as_ref().unwrap().render.screenshot();
          }

        }
      }
      //
      //  SimulationToAvMsg
      //
      GlobalMessage::SimulationToAvMsg (simulation_to_av_msg) => {
        use thread::av::render;
        match simulation_to_av_msg {
          SimulationToAvMsg::Quit => {
            control_flow = apis::process::ControlFlow::Break;
          }
          SimulationToAvMsg::PlayerSpawn (step, player_id, player) => {
            let render_context      = &mut self.av_context.as_mut().unwrap().render;
            let mut animation_state = render::PlayerAnimationState::default();
            let player_vertex       = render::player_vertex (
              step, render_context.frame_time, &player, &mut animation_state);
            assert!(render_context.player_vertex_data
              .insert (player_id.into(), player_vertex).is_none());
            assert!(render_context.player_animation_state
              .insert (player_id.into(), animation_state).is_none());
          }
          SimulationToAvMsg::PlayerUpdate (step, player_id, player) => {
            let (old_frame, new_frame, position, velocity) = {
              // render update
              let render_context = &mut self.av_context.as_mut().unwrap().render;
              if let Some ((
                view_player_id, view_attack_kind, last_health, hurt_fade
              )) = render_context.view_player.as_mut() {
                if *view_player_id == player_id {
                  if let Some (attack_state) = player.attack_state.as_ref() {
                    *view_attack_kind = attack_state.attack_kind.clone();
                  }
                  if player.health < *last_health {
                    let health_lost = *last_health - player.health;
                    // TODO: class specific life totals
                    let loss_percentage =
                      health_lost as f32 / player::BASE_PLAYER_HEALTH as f32;
                    *hurt_fade += 0.5 * loss_percentage;
                    *hurt_fade = f32::max (*hurt_fade, 1.0);
                  }
                  *last_health = player.health;
                }
              }
              let player_index    = player_id.into();
              let animation_state = render_context.player_animation_state
                .get_mut (player_index).unwrap();
              for player::Wound (position) in &player.wounds[..] {
                let anim_state = render::EffectAnimationState::from_id (
                  render::EffectId::Blood);
                let vertex     = render::shader::Vert3dSpriteArrayFrames {
                  in_position:      *position.cast().unwrap().as_ref(),
                  in_texture_index: render::EffectId::Blood as u32,
                  in_frame:         0
                };
                render_context.effect_animation_state.push (anim_state);
                render_context.effect_vertex_data.push (vertex);
              }
              let player_vertex   = render_context.player_vertex_data
                .get_mut (player_index).unwrap();
              let old_frame = player_vertex.in_frame;
              *player_vertex = render::player_vertex (
                step, render_context.frame_time, &player, animation_state);
              let new_frame = player_vertex.in_frame;
              ( old_frame, new_frame,
                player.position.cast().unwrap(),
                player.velocity.cast().unwrap()
              )
            };
            // audio update
            let av_context = self.av_context.as_mut().unwrap();
            let position = rfmod::Vector {
              x: position.x, y: position.y, z: position.z
            };
            let velocity = rfmod::Vector {
              x: velocity.x, y: velocity.y, z: velocity.z
            };
            if let Some (channel) = {
              if player.health == 0 {
                // TODO: this doesn't actually work since the player is killed and
                // respawned with full health between steps
                // if player died, play death sounds
                // TODO: player death voice
                //let channel = audio_context.sound_sfx_corpse_fall.play()
                //  .unwrap();
                //Some (channel)
                None
              } else if old_frame != new_frame {
                use num_traits::FromPrimitive;
                // otherwise play triggered audio SFX
                let player_frame = render::PlayerSpriteFrame::from_u32 (new_frame)
                  .unwrap();
                // TODO: in the following handling of triggered sounds, only one
                // sound can be triggered on a single step, so if a player attacks
                // and is hit on the same step, the last state will be the sound
                // effect played
                match player_frame {
                  render::PlayerSpriteFrame::Melee2 =>
                    // TODO: different audio depending on weapon type
                    Some (av_context.audio.sound_sfx_swing_medium.play().unwrap()),
                  render::PlayerSpriteFrame::Walk2 =>
                    Some (av_context.audio.sound_sfx_footstep_soft1.play()
                      .unwrap()),
                  render::PlayerSpriteFrame::Walk4 =>
                    Some (av_context.audio.sound_sfx_footstep_soft2.play()
                      .unwrap()),
                  render::PlayerSpriteFrame::Hurt => {
                    use rand::Rng;
                    // play a random hit sound
                    Some (match av_context.rng.gen_range (0, 3) {
                      0 => av_context.audio.sound_sfx_hit_melee1.play().unwrap(),
                      1 => av_context.audio.sound_sfx_hit_melee2.play().unwrap(),
                      2 => av_context.audio.sound_sfx_hit_melee3.play().unwrap(),
                      _ => unreachable!() // TODO: compiler hint?
                    })
                  }
                  _ => None
                }
              } else {
                None
              }
            } {
              rfmod_ok!(channel.set_3D_attributes (&position, &velocity));
              // NB: fmod sys update must be called immediately or else a 'pop'
              // will be heard
              rfmod_ok!(av_context.audio.rfmod_sys.update());
            }
          }
          SimulationToAvMsg::PlayerView (player_id) => {
            // NOTE: setting the player state fields to arbitrary values,
            // actual values will be updated when receiving PlayerUpdates
            // for this player
            self.av_context.as_mut().unwrap().render.view_player = Some ((
              player_id,
              player::AttackKind::Melee (player::MeleeKind::Chop),
              0,    // last observed health
              0.0   // hurt alpha fade percentage
            ));
          }
          SimulationToAvMsg::Corpses (corpse_vec) => {
            // play audio for any newly spawned corpses
            let av_context = self.av_context.as_mut().unwrap();
            for i in av_context.render.corpse_vertex_data.len()..corpse_vec.len()
            {
              let position = {
                let position = corpse_vec[i];
                rfmod::Vector { x: position.x, y: position.y, z: position.z }
              };
              let velocity = rfmod::Vector { x: 0.0, y: 0.0, z: 0.0 };
              let channel  = av_context.audio.sound_sfx_corpse_fall.play()
                .unwrap();
              rfmod_ok!(channel.set_3D_attributes (&position, &velocity));
              // NB: fmod sys update must be called immediately or else a 'pop'
              // will be heard
              rfmod_ok!(av_context.audio.rfmod_sys.update());
            }
            av_context.render.corpse_vertex_data = corpse_vec.into_iter().map (
              |position| render::shader::Vert3d {
                in_position: *position.as_ref()
              }
            ).collect();
          }
          SimulationToAvMsg::ViewPosition (view_position) => {
            self.av_context.as_mut().unwrap().render.view_position =
              view_position;
          }
          SimulationToAvMsg::ViewOrientation (view_orientation) => {
            self.av_context.as_mut().unwrap().render.view_orientation =
              view_orientation;
          }
        }
      }
      //
      // other
      //
      _ => unreachable!("invalid message type received")
    }
    log::trace!("...AvThread handle message");
    control_flow
  } // end av_handle_message

  fn av_update (&mut self) -> apis::process::ControlFlow {
    use glium::Surface;
    use thread::av::render::shader;
    log::trace!("AvThread update...");

    log::trace!("AvThread frame: {}", self.frame);

    { // render frame
    let render_context = &mut self.av_context.as_mut().unwrap().render;
    render_context.frame_time_last = render_context.frame_time;
    render_context.frame_time      = time::Instant::now();
    render_context.frame_duration  = render_context.frame_time.duration_since (
      render_context.frame_time_last);
    // update player vertex data
    render_context.update_players();
    // update corpse vertex buffer
    render_context.update_corpses();
    // update effect vertex data
    render_context.update_effects();
    // update view information
    render_context.update_view_transform_matrix();
    // parameters
    let draw_parameters = glium::DrawParameters {
      depth: glium::Depth {
        test:  glium::DepthTest::IfLess,
        write: true,
        .. Default::default()
      },
      .. Default::default()
    };

    // uniforms
    let uni_projection_mat_perspective : &[[f32; 4]; 4] =
      render_context.projection_mat_perspective.as_ref();
    let uni_transform_mat_view         : &[[f32; 4]; 4] =
      render_context.transform_mat_view.as_ref();
    let uniform_view_3d_mats = &uniform! {
      uni_projection_mat_perspective: *uni_projection_mat_perspective,
      uni_transform_mat_view:         *uni_transform_mat_view
    };

    // draw frame
    let mut glium_frame = render_context.glium_display.draw();
    glium_frame.clear_all ((0.0, 0.0, 0.0, 1.0), 1.0, 0);

    { // draw debug grid
      let program = &render_context.shader_programs [
        shader::ProgramId::ModelSpace3dInstancedScaleUniformColor as usize];
      glium_frame.draw (
        (&render_context.mesh_grid_vertex_buffer,
          render_context.mesh_grid_per_instance_vertex_buffer.per_instance()
            .unwrap()
        ),
        &render_context.mesh_grid_index_buffer,
        program,
        &uniform! {
          uni_projection_mat_perspective: *uni_projection_mat_perspective,
          uni_transform_mat_view:         *uni_transform_mat_view,
          uni_color:                      *DEBUG_GRID_COLOR
        },
        &draw_parameters
      ).unwrap();
    } // end draw debug grid

    { // draw debug basis vectors at origin
      let program = &render_context.shader_programs [
        shader::ProgramId::WorldSpace3dOrientationBasis as usize];
      glium_frame.draw (
        &render_context.origin_vertex,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        uniform_view_3d_mats,
        &draw_parameters
      ).unwrap();
    } // end draw debug basis vectors at origin

    /*
    { // draw debug basis vectors for bots
      let program = &render_context.shader_programs [
        shader::ProgramId::WorldSpace3dOrientationBasis as usize];
      glium_frame.draw (
        &render_context.player_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        uniform_view_3d_mats,
        &draw_parameters
      ).unwrap();
    } // end draw debug basis vectors for bots
    */

    { // draw sprites for bots
      let program = &render_context.shader_programs [
        shader::ProgramId::WorldSpace3dOrientationSpriteArrayFrames as usize];
      let uni_projection_mat_perspective : &[[f32; 4]; 4] =
        render_context.projection_mat_perspective.as_ref();
      let uni_transform_mat_view         : &[[f32; 4]; 4] =
        render_context.transform_mat_view.as_ref();
      // TODO: should we keep a current sampler in the render state ?
      let uni_sampler_2d_array
        : glium::uniforms::Sampler <glium::texture::Texture2dArray>
        = render_context.player_sprite_textures.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest);
      glium_frame.draw (
        &render_context.player_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_projection_mat_perspective: *uni_projection_mat_perspective,
          uni_transform_mat_view:         *uni_transform_mat_view,
          uni_sampler_2d_array:           uni_sampler_2d_array
        },
        &draw_parameters
      ).unwrap();
    } // end draw sprites for bots

    { // draw corpse sprites
      let program = &render_context.shader_programs [
        shader::ProgramId::WorldSpace3dPointSprite as usize];
      let uni_projection_mat_perspective : &[[f32; 4]; 4] =
        render_context.projection_mat_perspective.as_ref();
      let uni_transform_mat_view         : &[[f32; 4]; 4] =
        render_context.transform_mat_view.as_ref();
      // TODO: should we keep a current sampler in the render state ?
      let uni_sampler_2d
        : glium::uniforms::Sampler <glium::texture::Texture2d>
        = render_context.corpse_sprite_texture.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest);
      glium_frame.draw (
        &render_context.corpse_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_projection_mat_perspective: *uni_projection_mat_perspective,
          uni_transform_mat_view:         *uni_transform_mat_view,
          uni_sampler_2d:                 uni_sampler_2d
        },
        &draw_parameters
      ).unwrap();
    } // end draw corpse sprites

    { // draw effect sprites
      let program = &render_context.shader_programs [
        shader::ProgramId::WorldSpace3dPointSpriteArrayFrames as usize];
      let uni_projection_mat_perspective : &[[f32; 4]; 4] =
        render_context.projection_mat_perspective.as_ref();
      let uni_transform_mat_view         : &[[f32; 4]; 4] =
        render_context.transform_mat_view.as_ref();
      // TODO: should we keep a current sampler in the render state ?
      let uni_sampler_2d_array
        : glium::uniforms::Sampler <glium::texture::Texture2dArray>
        = render_context.effect_sprite_textures.sampled()
            .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
            .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest);
      glium_frame.draw (
        &render_context.effect_vertex_buffer,
        glium::index::NoIndices (glium::index::PrimitiveType::Points),
        program,
        &uniform! {
          uni_projection_mat_perspective: *uni_projection_mat_perspective,
          uni_transform_mat_view:         *uni_transform_mat_view,
          uni_sampler_2d_array:           uni_sampler_2d_array
        },
        &draw_parameters
      ).unwrap();
    } // end draw effect sprites

    /*
    { // draw instanced bounding spheres for bots
      let program = &render_context.shader_programs [
        shader::ProgramId::ModelSpace3dInstancedScaleUniformColor as usize];
      let uni_projection_mat_perspective : &[[f32; 4]; 4]
        = render_context.projection_mat_perspective.as_ref();
      let uni_transform_mat_view         : &[[f32; 4]; 4]
        = render_context.transform_mat_view.as_ref();
      glium_frame.draw (
        (&render_context.mesh_sphere_vertex_buffer,
          render_context.player_vertex_buffer.per_instance().unwrap()
        ),
        &render_context.mesh_sphere_index_buffer,
        program,
        &uniform! {
          uni_projection_mat_perspective: *uni_projection_mat_perspective,
          uni_transform_mat_view:         *uni_transform_mat_view,
          uni_color:                      *DEBUG_SPHERE_COLOR
        },
        &draw_parameters
      ).unwrap();
    } // end draw instanced bounding spheres for bots

    { // draw instanced bounding cylinders for bots
      let program = &render_context.shader_programs [
        shader::ProgramId::ModelSpace3dInstancedScaleUniformColor as usize];
      let uni_projection_mat_perspective : &[[f32; 4]; 4]
        = render_context.projection_mat_perspective.as_ref();
      let uni_transform_mat_view         : &[[f32; 4]; 4]
        = render_context.transform_mat_view.as_ref();
      glium_frame.draw (
        (&render_context.mesh_cylinder_vertex_buffer,
          render_context.player_vertex_buffer.per_instance().unwrap()
        ),
        &render_context.mesh_cylinder_index_buffer,
        program,
        &uniform! {
          uni_projection_mat_perspective: *uni_projection_mat_perspective,
          uni_transform_mat_view:         *uni_transform_mat_view,
          uni_color:                      *DEBUG_CYLINDER_COLOR
        },
        &draw_parameters
      ).unwrap();
    } // end draw debug bounding cylinders for bots
    */

    /*
    { // draw instanced bounding capsules for bots
      let program = &render_context.shader_programs [
        shader::ProgramId::ModelSpace3dInstancedCapsuleUniformColor as usize];
      glium_frame.draw (
        (&render_context.mesh_capsule_vertex_buffer,
          render_context.player_vertex_buffer.per_instance().unwrap()
        ),
        &render_context.mesh_capsule_index_buffer,
        program,
        &uniform! {
          uni_projection_mat_perspective: *uni_projection_mat_perspective,
          uni_transform_mat_view:         *uni_transform_mat_view,
          uni_color:                      *DEBUG_CAPSULE_COLOR
        },
        &draw_parameters
      ).unwrap();
    } // end draw instanced bounding capsules for bots
    */

    if render_context.view_player.is_some() {
      { // draw on-screen weapon graphics
        let program = &render_context.shader_programs[
          shader::ProgramId::ClipSpace2dSpriteArrayFrames as usize];
        glium_frame.draw (
          &render_context.weapon_graphic_vertex_buffer,
          glium::index::NoIndices (glium::index::PrimitiveType::Points),
          program,
          &uniform! {
            uni_sampler_2d_array: render_context.weapon_graphic_textures.sampled()
              .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
              .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest),
            uni_row_count:        7u32,
            uni_column_count:     5u32
          },
          &Default::default()
        ).unwrap();
      } // end draw on-screen weapons graphics

      { // draw player hurt screen flash
        let program = &render_context.shader_programs[
          shader::ProgramId::ClipSpace2dColor as usize];
        glium_frame.draw (
          &render_context.screen_quad_vertex_buffer,
          glium::index::NoIndices (glium::index::PrimitiveType::TriangleStrip),
          program,
          &glium::uniforms::EmptyUniforms,
          &glium::DrawParameters {
            blend: glium::draw_parameters::Blend::alpha_blending(),
            .. Default::default()
          }
        ).unwrap();
      } // end draw player hurt screen flash
    }

    // flip buffers
    glium_frame.finish().unwrap();
    } // end draw frame

    self.av_context.as_mut().unwrap().do_frame();

    self.frame += 1;

    log::trace!("...AvThread update");
    apis::process::ControlFlow::Continue
  } // end fn av_update

} // end impl AvThread

//
//  impl SimulationThread
//
impl SimulationThread {
  fn simulation_init (&mut self) {
    use apis::Process;

    let config = &*simulation::STATIC_CONFIG;
    // set up human player
    let mut player = Player::with_position_heading (
      STATIC_CONFIG.player_initial_position.into(),
      cgmath::Deg (STATIC_CONFIG.player_initial_heading as f64),
      config);
    player.class = player::Class::FighterLightM;
    self.spawn_player (PLAYER_ID, player);
    self.send (
      ChannelId::SimulationToAv, SimulationToAvMsg::PlayerView (PLAYER_ID)
    ).unwrap();

    // set up bots
    for i in 0..STATIC_CONFIG.bot_count {
      let player_id = player::Id (i+1);
      let (position, heading, class) = if (i as usize)
        < STATIC_CONFIG.bots_initial_positions_headings_classes.len()
      {
        STATIC_CONFIG.bots_initial_positions_headings_classes[i as usize]
      } else {
        use rand::Rng;
        use enum_unitary::FromPrimitive;
        let position = geometry::Aabb3::with_minmax (
          [-5.0, -5.0,  2.0].into(),
          [ 5.0,  5.0, 20.0].into()
        ).rand_point (&mut self.rng);
        let heading  = self.rng.gen_range (0, 360);
        let class    = player::Class::from_u16 (self.rng.gen_range (0, 2))
          .unwrap();
        (*position.as_ref(), heading, class)
      };
      let mut bot = Player::with_position_heading (
        position.into(), cgmath::Deg (heading as f64), config);
      bot.class   = class;
      // ID 0 reserved for player
      self.spawn_player (player_id, bot);
      self.bot_move_states.push (player::MoveState::default());
    }

    // trace: print initialized world structure
    if cfg!(debug_assertions) {
      use colored::Colorize;
      log::trace!("{} {:?}",
        "SimulationThread initialized World:".magenta().bold(),
        self.simulation_context.world_current())
    }
  }

  fn simulation_handle_message (&mut self, _message : GlobalMessage)
    -> apis::process::ControlFlow
  {
    log::trace!("SimulationThread handle message...");
    let mut result = apis::process::ControlFlow::Continue;
    //#[allow(unused_assignments)]
    match _message {
      GlobalMessage::MainToSimulationMsg (msg) => {
        match msg {
          // quit
          MainToSimulationMsg::Quit => {
            self.quit = true;
            result    = apis::process::ControlFlow::Break;
          }
          MainToSimulationMsg::Command (command) =>
            self.simulation_context.schedule_command_current (command),
        } // end MainToSimulationMsg
      } // end GlobalMessage::MainToSimulationMsg
      _ => unreachable!("invalid message type received")
    }
    log::trace!("...SimulationThread handle message");
    result
  }

  fn simulation_update (&mut self) -> apis::process::ControlFlow {
    log::trace!("SimulationThread update...");
    let mut control_flow = apis::process::ControlFlow::Continue;

    let t_before = self.simulation_context.timer.timestamp_now();
    if self.quit {
      use apis::Process;
      self.send (ChannelId::SimulationToAv, SimulationToAvMsg::Quit).unwrap();
      control_flow = apis::process::ControlFlow::Break;
    } else {
      self.do_step();
      if cfg!(debug_assertions) {
        self.debug_step();
      }
      self.update_av();
    }
    let t_after   = self.simulation_context.timer.timestamp_now();
    let next_step = self.simulation_context.step_current();

    let t_elapsed    = *t_after - *t_before;
    log::trace!("  update time elapsed: {:?}", time::Duration::from_nanos (t_elapsed));
    let t_next       = self.simulation_context.timer.next_step_ts();
    if t_after < t_next {
      let t_until = *t_next - *t_after;  // must be positive
      std::thread::sleep (time::Duration::from_nanos (t_until));
    } else {
      use colored::Colorize;
      log::warn!("SimulationThread step[{}]: {}",
        next_step, "late step".yellow().bold());
    }

    log::trace!("...SimulationThread update");
    control_flow
  } // end fn simulation_update

  //
  // private
  //

  fn spawn_player (&mut self, player_id : player::Id, player : Player) {
    use apis::Process;
    let command = simulation::Command::Player (
      player_id, player::Command::Spawn (player.clone()));
    let current_step = self.simulation_context.step_current();
    self.simulation_context.schedule_command (current_step, command);
    self.send (
      ChannelId::SimulationToAv,
      SimulationToAvMsg::PlayerSpawn (current_step, player_id, player)
    ).unwrap();
  }

  #[inline]
  fn do_step (&mut self) {
    let current_step = self.simulation_context.step_current();
    log::trace!("SimulationThread step: {}", current_step);
    self.bot_commands();
    self.simulation_context.step();
  }

  fn bot_commands (&mut self) {
    use rand::Rng;
    let current_step = self.simulation_context.step_current();
    // some bot commands
    for i in 0..STATIC_CONFIG.bot_count {
      let player_id = player::Id (i+1);
      // attack randomly
      if self.rng.gen_range (0, 60) == 0 {
        let attack_kind = player::MeleeKind::Chop.into();
        let command = simulation::Command::Player (
          player_id, player::Command::Attack (attack_kind));
        self.simulation_context.schedule_command_current (command);
      }
      // jump randomly
      if self.rng.gen_range (0, 150) == 0 {
        let command =
          simulation::Command::Player (player_id, player::Command::Jump);
        self.simulation_context.schedule_command_current (command);
      }
    }
    // walk cycle
    for i in 0..STATIC_CONFIG.bot_count {
      let player_id = player::Id (i+1);
      let cycle_len = 115 + 10 * i as u64;
      let step_mod  = current_step % cycle_len;
      let sixth     = cycle_len / 6;
      if step_mod == 0*sixth {
        let command = simulation::Command::Player (
          player_id,
          player::Command::Move (player::MoveState::default().move_forward())
        );
        self.simulation_context.schedule_command_current (command);
      } else if step_mod == 1*sixth {
        let player_id = player::Id (i+1);
        let command = simulation::Command::Player (
          player_id,
          player::Command::Move (player::MoveState::default().move_backward())
        );
        self.simulation_context.schedule_command_current (command);
      } else if step_mod == 2*sixth {
        let player_id = player::Id (i+1);
        let command = simulation::Command::Player (
          player_id,
          player::Command::Move (player::MoveState::default().move_backward())
        );
        self.simulation_context.schedule_command_current (command);
      } else if step_mod == 3*sixth {
        let player_id = player::Id (i+1);
        let command = simulation::Command::Player (
          player_id,
          player::Command::Move (player::MoveState::default().move_forward())
        );
        self.simulation_context.schedule_command_current (command);
      }
    }
  }

  #[inline]
  fn debug_step (&self) {
    // debug simulation step
    let step = self.simulation_context.step_current();
    debug_assert_eq!(step, self.simulation_context.world_current().step);
    debug_assert_eq!(step, self.simulation_context.timer.step_current());
    if STATIC_CONFIG.debug_steps != 0 {
      if step % STATIC_CONFIG.debug_steps == 0 {
        use colored::Colorize;
        log::info!("{}", format!("simulation step[{}]", step).magenta().bold());
        {
          if let Some (player) = self.simulation_context.world_current().players
            .get (PLAYER_ID.0 as usize)
          {
            log::debug!("player (yaw, pitch): ({:?}, {:?})", player.yaw, player.pitch);
          }
        }
      }
    }
  }

  fn update_av (&mut self) {
    use apis::Process;
    // send updated step data to AvThread
    let current_step = self.simulation_context.step_current();
    for (id, player) in self.simulation_context.world_current().players.iter() {
      self.send (
        ChannelId::SimulationToAv,
        SimulationToAvMsg::PlayerUpdate (
          current_step, player::Id (id as u8), player.clone())
      ).unwrap();
    }
    let corpse_vec : Vec <cgmath::Point3 <f32>> = self.simulation_context
      .world_current().system.objects_dynamic().iter().filter_map (
        |(index, object)| if index >= simulation::world::MAX_PLAYER_ID as usize {
          Some (object.position.0.cast().unwrap())
        } else {
          None
        })
      .collect();
    self.send (
      ChannelId::SimulationToAv,
      SimulationToAvMsg::Corpses (corpse_vec)
    ).unwrap();
  }
} // end impl SimulationThread

impl Default for PracticeResult {
  fn default() -> Self {
    PracticeResult::QuitToTitle
  }
}
