// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use {std, /*glium, glium::glutin,*/ rfmod};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use vec_map::VecMap;

use apis;
use fmod_utils::rfmod_ok;

use crate::thread;

///////////////////////////////////////////////////////////////////////////////
//  config                                                                   //
///////////////////////////////////////////////////////////////////////////////

#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  /// `AvThread` will wait for this length of time before starting the splash
  /// animation.
  pub wait_ms   : u64,
  /// `AvThread` will emit an SDL `Escape` key event after this length of time;
  /// this is basically an ad-hoc way of sending the `MainThread` a message
  /// based on renderer state (i.e. the splash animation has finished).
  pub splash_ms : u64,
  pub input_map : std::collections::HashMap
    <thread::main::input::keyboard::Keycode, InputFun>
}

///////////////////////////////////////////////////////////////////////////////
//  statics                                                                  //
///////////////////////////////////////////////////////////////////////////////

lazy_static!{
  pub static ref STATIC_CONFIG : &'static StaticConfig =
    &crate::STATIC_CONFIG.mode_config.splash_config;
  pub static ref STATIC_INPUT_MAP : VecMap <InputFun>  = {
    let mut v = VecMap::new();
    for (keycode, input_fun) in STATIC_CONFIG.input_map.iter() {
      assert!(v.insert (*keycode as usize, *input_fun).is_none());
    }
    v
  };
  static ref STATIC_WAIT_DUR   : std::time::Duration =
    std::time::Duration::from_millis (STATIC_CONFIG.wait_ms);
  static ref STATIC_SPLASH_DUR : std::time::Duration =
    std::time::Duration::from_millis (STATIC_CONFIG.splash_ms);
}

///////////////////////////////////////////////////////////////////////////////
//  session                                                                  //
///////////////////////////////////////////////////////////////////////////////

// /!\ NOTE: processes are initialized in given order and the render context
// below must be acquired *after* the input context is initialized, so
// MainThread must come before AvThread
apis::def_session! {
  context Splash {
    PROCESSES where
      let splash     = self,
      let message_in = message_in
    [
      process MainThread (
        main_context : Option <thread::Main> = Some (thread::Main::create()),
        input_map    : VecMap <InputFun>     = STATIC_INPUT_MAP.clone()
      ) -> (SplashResult) {
        kind           {
          apis::process::Kind::Mesochronous {
            tick_ms:          *thread::main::INPUT_MS,
            ticks_per_update: 1
          }
        }
        sourcepoints   [MainToAv]
        endpoints      [ ]
        handle_message { unreachable!() }
        update         { splash.main_update() }
      }
      process AvThread (
        // av context is boxed because it is large (>15KB)
        av_context    : Option <Box <thread::Av>>      =
          Some (Box::new (thread::Av::create())),
        frame         : u64                            = 0,
        start_time    : Option <std::time::SystemTime> = None,
        wait_time     : Option <std::time::SystemTime> = None,
        end_time      : Option <std::time::SystemTime> = None,
        wakeup_sent   : bool                           = false,
        clear_color   : (f32, f32, f32, f32)           = (1.0, 1.0, 1.0, 1.0),
        rfmod_channel : Option <rfmod::Channel>        = None
      ) {
        kind           { apis::process::Kind::Anisochronous }
        sourcepoints   [ ]
        endpoints      [MainToAv]
        initialize     { splash.init() }
        terminate      { splash.term() }
        handle_message { splash.av_handle_message (message_in) }
        update         { splash.av_update() }
      }
    ]
    CHANNELS [
      channel MainToAv <MainToAvMsg> (Simplex) {
        producers [MainThread]
        consumers [AvThread]
      }
    ]
    MESSAGES [
      message MainToAvMsg {
        Default (thread::MainToAvMsg),
        Skip
      }
    ]
    main: MainThread
  }
}

///////////////////////////////////////////////////////////////////////////////
//  input functions                                                          //
///////////////////////////////////////////////////////////////////////////////

impl_input_fun! {
  MainThread {
    MainThread::screenshot,     Screenshot
    MainThread::quit_to_system, QuitToSystem
    MainThread::skip,           Skip
  }
}

///////////////////////////////////////////////////////////////////////////////
//  enums                                                                    //
///////////////////////////////////////////////////////////////////////////////

#[derive(Clone,Debug,Eq,PartialEq)]
pub enum SplashResult {
  StartTitleMenu,
  QuitToSystem
}

///////////////////////////////////////////////////////////////////////////////
//  impls                                                                    //
///////////////////////////////////////////////////////////////////////////////

impl MainThread {
  fn main_update (&mut self) -> apis::process::ControlFlow {
    //use colored::Colorize;
    log::trace!("MainThread update...");
    let control_flow = self.skip().control_flow.unwrap();
    // TODO: splash intro currently disabled
    /*
    let mut control_flow = apis::process::ControlFlow::Continue;
    // glutin poll input events
    let mut events_loop  = self.main_context.as_mut().unwrap().input.events_loop
      .take().unwrap();
    events_loop.poll_events (|event|{
      log::trace!("{}: {:?}", "glutin event".to_string().cyan().bold(), event);
      match event {
        glutin::Event::WindowEvent { event, .. } => match event {
          // closed
          glutin::WindowEvent::CloseRequested |
          glutin::WindowEvent::Destroyed      => {
            info!("glutin WindowEvent::Closed: quitting to system...");
            // safe to unwrap: always returns Some(Break)
            control_flow = self.quit_to_system().control_flow.unwrap();
          }
          // resize
          glutin::WindowEvent::Resized (logical_size) => {
            use apis::Process;
            let (x, y) = { // borrow gui
              let gui = &mut self.main_context.as_mut().unwrap().gui;
              let (width, height) = {
                let (x, y) : (u32, u32) =
                  logical_size.to_physical (gui.window_hidpi_factor).into();
                debug_assert!(x <= std::u16::MAX as u32);
                debug_assert!(y <= std::u16::MAX as u32);
                (x as u16, y as u16)
              };
              gui.set_screen_resolution (width, height);
              (width, height)
            };
            self.send (
              ChannelId::MainToAv,
              MainToAvMsg::Default (thread::MainToAvMsg::WindowSize { x, y })
            ).unwrap();
          }
          // keyboard input
          glutin::WindowEvent::KeyboardInput {
            input: glutin::KeyboardInput { state, virtual_keycode, .. },
            ..
          } => match state {
            glutin::ElementState::Pressed => if let Some (virtual_keycode) =
              virtual_keycode
            {
              let keycode : thread::main::input::keyboard::Keycode =
                virtual_keycode.into();
              let maybe_input_fun = self.input_map.get (keycode as usize)
                .and_then (|input_fun| Some (*input_fun));
              if let Some (input_fun) = maybe_input_fun {
                let input_fun_result = input_fun (self);
                control_flow = input_fun_result.control_flow
                  .unwrap_or (apis::process::ControlFlow::Continue);
              }
            }
            _ => ()
          }
          _ => ()
        } // end WindowEvent
        //glutin::Event::DeviceEvent { event, .. } => {}
        glutin::Event::Awakened => {
          // awakened by av thread, break
          control_flow = self.skip().control_flow.unwrap();
        }
        //glutin::Event::Suspended (b)
        _ => ()
      } // end match glutin event
    }); // end glutin poll input events
    debug_assert!(self.main_context.as_mut().unwrap().input.events_loop
      .is_none());
    self.main_context.as_mut().unwrap().input.events_loop = Some (events_loop);
    */
    log::trace!("...MainThread update");

    control_flow
  } // end fn main_update

  pub fn skip (&mut self) -> InputFunResult {
    use apis::Process;
    self.send (ChannelId::MainToAv, MainToAvMsg::Skip).unwrap();
    let mut result = InputFunResult::default();
    result.control_flow = Some (apis::process::ControlFlow::Break);
    result
  }

  //
  //  default MainToAv messages
  //

  pub fn quit_to_system (&mut self) -> InputFunResult {
    use apis::Process;
    *self.result_mut() = SplashResult::QuitToSystem;
    // ignore the send result, program is being terminated anyway
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Quit {
        // TODO: this flag is kind of a kludge, not used in this mode
        reset_audio_on_term: true
      })
    ).unwrap();
    let mut result = InputFunResult::default();
    result.control_flow = Some (apis::process::ControlFlow::Break);
    result
  }

  pub fn screenshot (&mut self) -> InputFunResult {
    use apis::Process;
    self.send (
      ChannelId::MainToAv,
      MainToAvMsg::Default (thread::MainToAvMsg::Screenshot)
    ).unwrap();
    InputFunResult::default()
  }

} // end impl MainThread

impl AvThread {

  fn init (&mut self) {
    let t_now = std::time::SystemTime::now();
    self.start_time = Some (t_now);
    self.wait_time  = Some (t_now + *STATIC_WAIT_DUR);
    self.end_time   = Some (t_now + *STATIC_SPLASH_DUR);
  }

  fn term (&mut self) {
    if let Some (ref rfmod_channel) = self.rfmod_channel {
      rfmod_ok!(rfmod_channel.stop())
    }
  }

  fn av_handle_message (&mut self, message : GlobalMessage)
    -> apis::process::ControlFlow
  {
    log::trace!("AvThread handle message...");
    #[allow(unused_assignments)]
    let mut control_flow = apis::process::ControlFlow::Continue;
    match message {
      // window size
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::WindowSize { x, y })
      ) => {
        self.av_context.as_mut().unwrap().render.set_resolution (x,y);
      }
      // mouse position
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::MousePosition { x, y })
      ) => {
        self.av_context.as_mut().unwrap().render.set_mouse_position (x,y);
      }
      // quit
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::Quit {..})
      ) => {
        control_flow = apis::process::ControlFlow::Break;
      }
      // screenshot
      GlobalMessage::MainToAvMsg (
        MainToAvMsg::Default (thread::MainToAvMsg::Screenshot)
      ) => {
        self.av_context.as_ref().unwrap().render.screenshot();
      }
      // skip
      GlobalMessage::MainToAvMsg (MainToAvMsg::Skip) => {
        control_flow = apis::process::ControlFlow::Break;
      }
    }
    log::trace!("...AvThread handle message");
    control_flow
  }

  fn av_update (&mut self) -> apis::process::ControlFlow {
    use glium::Surface;
    log::trace!("AvThread update...");

    log::trace!("AvThread frame: {}", self.frame);

    let t_now = std::time::SystemTime::now();
    if self.rfmod_channel.is_none() {
      if self.wait_time.unwrap() <= t_now {
        /*
        self.rfmod_channel = {
          let audio_context = &mut self.av_context.as_mut().unwrap().audio;
          Some (audio_context.sound_sfx_aura_good.play().unwrap())
        };
        */
      }
    }

    if self.end_time.unwrap() <= t_now && !self.wakeup_sent {
      // notify the input thread to finish
      log::debug!("AvThread: wakeup input thread to continue");
      self.av_context.as_ref().unwrap().event_loop_proxy.send_event(())
        .unwrap();
      self.wakeup_sent = true;
    }

    { // draw frame
      let render_context = &self.av_context.as_ref().unwrap().render;
      let mut glium_frame = render_context.glium_display.draw();
      glium_frame.clear_all (self.clear_color, 0.0, 0);

      if self.wait_time.unwrap() <= t_now {
        // draw splash graphic
        /*
        use thread::av::render::shader;
        let program = &render_context
          .shader_programs [shader::ProgramId::ClipSpace2dSprite as usize];
        glium_frame.draw (
          &render_context.splash_vertex_buffer,
          glium::index::NoIndices (glium::index::PrimitiveType::Points),
          program,
          &uniform! {
            uni_sampler_2d: render_context.splash_texture.sampled()
              .minify_filter  (glium::uniforms::MinifySamplerFilter::Nearest)
              .magnify_filter (glium::uniforms::MagnifySamplerFilter::Nearest),
          },
          &Default::default()
        ).unwrap();
        */
      }

      glium_frame.finish().unwrap();
    }

    self.av_context.as_mut().unwrap().do_frame();
    self.frame += 1;

    log::trace!("...AvThread update");
    apis::process::ControlFlow::Continue
  } // end fn av_update
} // end impl AvThread

impl Default for SplashResult {
  fn default() -> Self {
    SplashResult::StartTitleMenu
  }
}
