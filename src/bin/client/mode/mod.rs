// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

pub mod splash;
pub mod title_menu;
pub mod practice;
pub mod multi_setup;
pub mod multi_run;

pub use self::splash::{Splash, SplashResult};
pub use self::title_menu::{TitleMenu, TitleMenuResult};
pub use self::practice::{Practice, PracticeResult};
pub use self::multi_setup::{MultiSetup, MultiSetupResult};
pub use self::multi_run::{MultiRun, MultiRunResult};

#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  pub splash_config      : splash::StaticConfig,
  pub title_menu_config  : title_menu::StaticConfig,
  pub practice_config    : practice::StaticConfig,
  pub multi_setup_config : multi_setup::StaticConfig,
  pub multi_run_config   : multi_run::StaticConfig
}
