// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use std;
use serde::{Deserialize, Serialize};
use crate::{thread, mode};

pub const STATIC_CONFIG_YAML_PATH : &str = "data/client-config.yaml";

/// Static configuration to be loaded from `STATIC_CONFIG_YAML_PATH`
#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  /// Can be overriden with `-l <LOG_LEVEL>` command-line option
  pub log_level                 : String,
  /// Can be overriden with `-p <ARENA2_PATH>` command-line option
  pub arena2_path               : std::path::PathBuf,
  // window creation parameters
  pub resolution_x              : u16,
  pub resolution_y              : u16,
  /// Fullscreen will override the resolution settings
  pub fullscreen                : bool,
  pub vsync                     : bool,
  pub mouselook_sensitivity     : f64,
  /// Length of mouse motion vector must exceed this number to register an
  /// attack action.
  pub attack_sensitivity        : f64,
  pub lock_mouse_when_attacking : bool,
  pub thread_config             : thread::StaticConfig,
  pub mode_config               : mode::StaticConfig
}
