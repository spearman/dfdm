// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use apis::def_program;
use crate::mode::{self, splash, title_menu, practice, multi_setup, multi_run};

//
//  program Client
//
def_program! {
  program Client where
    let result = session.run()
  {
    MODES [
      mode splash::Splash {
        use apis::Process;
        log::debug!("Splash result: {:?}", result);
        let main_thread_result =
          mode::splash::MainThread::extract_result (&mut result).unwrap();
        match main_thread_result {
          mode::SplashResult::QuitToSystem   => None,
          mode::SplashResult::StartTitleMenu =>
            Some (EventId::SplashToTitleMenu)
        }
      }

      mode title_menu::TitleMenu {
        use apis::Process;
        log::debug!("TitleMenu result: {:?}", result);
        let main_thread_result =
          mode::title_menu::MainThread::extract_result (&mut result).unwrap();
        match main_thread_result {
          mode::TitleMenuResult::QuitToSystem    => None,
          mode::TitleMenuResult::StartPractice   =>
            Some (EventId::TitleMenuToPractice),
          mode::TitleMenuResult::StartMultiSetup =>
            Some (EventId::TitleMenuToMultiSetup)
        }
      }

      mode practice::Practice {
        use apis::Process;
        log::debug!("Practice result: {:?}", result);
        let main_thread_result =
          mode::practice::MainThread::extract_result (&mut result).unwrap();
        match main_thread_result {
          mode::PracticeResult::QuitToSystem => None,
          mode::PracticeResult::QuitToTitle  =>
            Some (EventId::PracticeToTitleMenu)
        }
      }

      mode multi_setup::MultiSetup {
        use apis::Process;
        log::debug!("MultiSetup result: {:?}", result);
        let main_thread_result =
          mode::multi_setup::MainThread::extract_result (&mut result).unwrap();
        match main_thread_result {
          mode::MultiSetupResult::QuitToSystem       => None,
          mode::MultiSetupResult::QuitToTitle        =>
            Some (EventId::MultiSetupToTitleMenu),
          mode::MultiSetupResult::ConnectionAccepted =>
            Some (EventId::MultiSetupToMultiRun)
        }
      }

      mode multi_run::MultiRun {
        use apis::Process;
        log::debug!("MultiRun result: {:?}", result);
        let main_thread_result =
          mode::multi_run::MainThread::extract_result (&mut result).unwrap();
        match main_thread_result {
          mode::MultiRunResult::QuitToSystem       => None,
          mode::MultiRunResult::QuitToTitle        =>
            Some (EventId::MultiRunToTitleMenu)
        }
      }
    ]
    TRANSITIONS  [
      transition SplashToTitleMenu <splash::Splash> => <title_menu::TitleMenu> [
        MainThread (prev) => MainThread (next) {
          next.main_context = prev.main_context.take();
        }
        AvThread (prev) => AvThread (next) {
          next.av_context   = prev.av_context.take();
        }
      ]

      transition TitleMenuToPractice
        <title_menu::TitleMenu> => <practice::Practice>
      [
        MainThread (prev) => MainThread (next) {
          next.main_context = prev.main_context.take();
        }
        AvThread (prev) => AvThread (next) {
          next.av_context   = prev.av_context.take();
        }
      ]

      transition PracticeToTitleMenu
        <practice::Practice> => <title_menu::TitleMenu>
      [
        MainThread (prev) => MainThread (next) {
          next.main_context = prev.main_context.take();
        }
        AvThread (prev) => AvThread (next) {
          next.av_context   = prev.av_context.take();
        }
      ]

      transition TitleMenuToMultiSetup
        <title_menu::TitleMenu> => <multi_setup::MultiSetup>
      [
        MainThread (prev) => MainThread (next) {
          next.main_context = prev.main_context.take();
        }
        AvThread (prev) => AvThread (next) {
          next.av_context          = prev.av_context.take();
          next.rfmod_channel_fire  = prev.rfmod_channel_fire.take();
          next.rfmod_channel_music = prev.rfmod_channel_music.take();
        }
      ]

      transition MultiSetupToTitleMenu
        <multi_setup::MultiSetup> => <title_menu::TitleMenu>
      [
        MainThread (prev) => MainThread (next) {
          next.main_context = prev.main_context.take();
        }
        AvThread (prev) => AvThread (next) {
          next.av_context          = prev.av_context.take();
          next.rfmod_channel_fire  = prev.rfmod_channel_fire.take();
          next.rfmod_channel_music = prev.rfmod_channel_music.take();
        }
      ]

      transition MultiSetupToMultiRun
        <multi_setup::MultiSetup> => <multi_run::MultiRun>
      [
        MainThread (prev) => MainThread (next) {
          next.main_context = prev.main_context.take();
        }
        AvThread (prev) => AvThread (next) {
          next.av_context   = prev.av_context.take();
        }
        NetThread (prev) => NetThread (next) {
          next.net_context  = prev.net_context.take();
        }
      ]

      transition MultiRunToTitleMenu
        <multi_run::MultiRun> => <title_menu::TitleMenu>
      [
        MainThread (prev) => MainThread (next) {
          next.main_context = prev.main_context.take();
        }
        AvThread   (prev) => AvThread   (next) {
          next.av_context   = prev.av_context.take();
        }
      ]

    ]
    initial_mode: Splash
  }
} // end program Client
