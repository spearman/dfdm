// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use std::time;

#[derive(Debug)]
pub struct Timer {
  /// Initialization time
  t_base               : time::Instant,
  tick                 : u64,
  /// Input and packet service interval
  tick_ms              : u16,
  /// Input and packet service interval
  tick_ns              : u64,
  tick_last_ts         : TimestampNs,
  /// Average tick *duration* (not time between ticks)
  tick_average_ns      : u64,
  ping                 : u64,
  pong                 : u64,
  ping_ms              : u16,
  ping_ns              : u64,
  ping_last_ts         : TimestampNs,
  pong_last_ts         : TimestampNs,
  /// Update average ping every nth ping.
  ///
  /// Should be set by server to ensure that averages agree.
  ping_rtt_window_size : u16,
  ping_rtt_sum         : u64,
  /// Average round-trip time in milliseconds
  ping_rtt_avg_ms      : u16
}

/// A timestamp measured as a nanosecond duration since the base initialization
/// time (the `t_base` field of the `Timer` struct)
#[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
pub struct TimestampNs (u64);

impl Timer {
  pub fn start (tick_ms : u16, ping_ms : u16) -> Self {
    let t_base          = time::Instant::now();
    let tick            = 0;
    let tick_ns         = 1_000_000 * tick_ms  as u64;
    let tick_last_ts    = TimestampNs (0);
    let tick_average_ns = 0;
    let ping            = 0;
    let pong            = 0;
    let ping_ns         = 1_000_000 * ping_ms  as u64;
    let ping_last_ts    = TimestampNs (0);
    let pong_last_ts    = TimestampNs (0);
    let ping_rtt_window_size = 4;
    let ping_rtt_avg_ms = 0;
    let ping_rtt_sum    = 0;
    Timer {
      t_base,
      tick,
      tick_ms,
      tick_ns,
      tick_last_ts,
      tick_average_ns,
      ping,
      pong,
      ping_ms,
      ping_ns,
      ping_last_ts,
      pong_last_ts,
      ping_rtt_window_size,
      ping_rtt_avg_ms,
      ping_rtt_sum
    }
  }

  #[inline]
  pub fn timestamp_now (&self) -> TimestampNs {
    let t_now = self.t_base.elapsed().as_nanos();
    debug_assert!(t_now <= std::u64::MAX as u128);
    TimestampNs (t_now as u64)
  }

  #[inline]
  pub fn timestamp (&self, instant : time::Instant) -> TimestampNs {
    let t_since = instant.duration_since (self.t_base).as_nanos();
    debug_assert!(t_since <= std::u64::MAX as u128);
    TimestampNs (t_since as u64)
  }

  #[inline]
  pub fn tick (&mut self) {
    self.tick_last_ts = self.timestamp_now();
    self.tick += 1;
  }

  #[inline]
  pub fn ping_count (&self) -> u64 {
    self.ping
  }

  #[inline]
  pub fn pong_count (&self) -> u64 {
    self.pong
  }

  #[inline]
  pub fn last_pong_timestamp (&self) -> TimestampNs {
    self.pong_last_ts
  }

  #[inline]
  pub fn average_ping_rtt_ms (&self) -> u16 {
    self.ping_rtt_avg_ms as u16
  }

  /// This should be set once before commencing ping/pong
  #[inline]
  pub fn set_ping_rtt_window_size (&mut self, ping_rtt_window_size : u16) {
    self.ping_rtt_window_size = ping_rtt_window_size;
  }

  /// Returns `Ok(timestamp)` if the returned time was after the next ping time,
  /// or `Err(timestamp)` if the current time was before the next ping time.
  #[inline]
  pub fn is_ping_time (&self) -> Result <TimestampNs, TimestampNs>  {
    let ts_now = self.timestamp_now();
    if ts_now < self.ping_last_ts + self.ping_ns {
      Err (ts_now)
    } else {
      Ok (ts_now)
    }
  }

  #[inline]
  pub fn ping (&mut self, timestamp : TimestampNs) {
    self.ping_last_ts = timestamp;
    self.ping += 1;
  }

  pub fn pong (&mut self, timestamp : TimestampNs) {
    self.pong_last_ts = timestamp;
    debug_assert!(*timestamp > *self.ping_last_ts);
    let ping_rtt_ms = {
      let ping_rtt_ms = (*timestamp - *self.ping_last_ts) / 1_000_000;
      debug_assert!(ping_rtt_ms <= std::u16::MAX as u64);
      ping_rtt_ms
    };
    self.ping_rtt_sum += ping_rtt_ms;
    self.pong         += 1;
    if self.pong % self.ping_rtt_window_size as u64 == 0 {
      self.ping_rtt_avg_ms =
        (self.ping_rtt_sum/self.ping_rtt_window_size as u64) as u16;
      self.ping_rtt_sum = 0;
    }
  }
}

impl std::ops::Deref for TimestampNs {
  type Target = u64;
  fn deref (&self) -> &u64 {
    &self.0
  }
}
impl std::ops::DerefMut for TimestampNs {
  fn deref_mut (&mut self) -> &mut u64 {
    &mut self.0
  }
}
impl std::ops::Add <u64> for TimestampNs {
  type Output = Self;
  fn add (self, rhs : u64) -> Self {
    TimestampNs (*self + rhs)
  }
}
impl std::ops::AddAssign <u64> for TimestampNs {
  fn add_assign (&mut self, rhs : u64) {
    self.0 += rhs;
  }
}
impl std::ops::Sub <u64> for TimestampNs {
  type Output = Self;
  fn sub (self, rhs : u64) -> Self {
    TimestampNs (*self - rhs)
  }
}
