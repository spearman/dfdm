// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use enet;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};

use rs_utils::show;

use dfdm;

mod timer;

use self::timer::Timer;

lazy_static! {
  pub static ref STATIC_CONFIG : &'static StaticConfig =
    &crate::STATIC_CONFIG.thread_config.net_config;
  pub static ref SERVICE_MS : u32 = {
    let service_ms = 1000 / (STATIC_CONFIG.service_frequency as u32);
    // sanity check
    assert!(0 < service_ms);
    assert!(service_ms <= 500);  // unlikely
    service_ms
  };
  pub static ref SERVICE_DUR : std::time::Duration =
    std::time::Duration::from_millis (*SERVICE_MS as u64);
}

#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  pub service_frequency : u16,
  pub ping_ms           : u16,
}

#[derive(Debug)]
pub struct Net {
  pub enet_host    : enet::Host,
  pub server_peer  : Option <enet::Peer>,
  pub connected_as : Option <dfdm::server::ClientId>,
  pub timer        : Timer
}

pub fn report_sizes() {
  use std::mem::size_of;
  println!("net report sizes...");
  show!(size_of::<Net>());
  println!("...net report sizes");
}

impl Net {
  pub fn create() -> Self {
    let enet      = enet::initialize().unwrap();
    // NB: enet host will keep enet context alive until it is dropped
    let enet_host = enet.client_host_create (1, None, None).unwrap();
    Net {
      enet_host,
      server_peer:   None,
      connected_as:  None,
      timer:         Timer::start (
        *SERVICE_MS           as u16,
        STATIC_CONFIG.ping_ms as u16)
    }
  }
  /// `server_peer` must be `Some`
  pub fn send (&mut self, message : dfdm::client::Message) {
    self.server_peer.as_mut().unwrap().send (
      0,
      enet::Packet::Allocate {
        bytes: bincode::serialize (&message).unwrap().as_slice(),
        flags: enet::packet::Flags::RELIABLE
      }
    ).unwrap();
  }
}
