// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use glium::glutin;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use rs_utils::show;
use crate::thread;

pub mod gui;
pub mod input;

pub use self::gui::Gui;
pub use self::input::Input;

////////////////////////////////////////////////////////////////////////////////
//  config                                                                    //
////////////////////////////////////////////////////////////////////////////////

#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  // input config
  pub input_frequency : u16,
  // gui config
  pub gui_scale       : u8
}

////////////////////////////////////////////////////////////////////////////////
//  statics                                                                   //
////////////////////////////////////////////////////////////////////////////////

lazy_static! {
  pub static ref STATIC_CONFIG : &'static StaticConfig =
    &crate::STATIC_CONFIG.thread_config.main_config;
  pub static ref INPUT_MS : u32 = {
    let input_ms = 1000 / (STATIC_CONFIG.input_frequency as u32);
    // sanity check
    assert!(0 < input_ms);
    assert!(input_ms <= 500);  // unlikely
    input_ms
  };
}

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

#[derive(Debug)]
pub struct Main {
  pub input : Input,
  pub gui   : Gui
}

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

pub fn report_sizes() {
  use std::mem::size_of;
  println!("main report sizes...");
  show!(size_of::<Main>());
  show!(size_of::<input::keyboard::Keycode>());
  println!("...main report sizes");
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

impl Main {
  pub fn create() -> Self {
    let client_config = &crate::STATIC_CONFIG;
    let physical_size = {
      let width  = client_config.resolution_x as f64;
      let height = client_config.resolution_y as f64;
      glutin::dpi::PhysicalSize::new (width, height)
    };
    // create the events loop and gl window to be passed to Av thread
    let event_loop      = glutin::event_loop::EventLoop::new();
    let primary_monitor = event_loop.primary_monitor();
    // NOTE: WindowBuilder provides a method to build a fullscreen window, but
    // as of glutin 0.18 (winit 0.16), calling either grab_cursor or hide_cursor
    // on windows will cause the application to hang, so fullscreen is set
    // after grabbing and hiding cursor
    // TODO: now on glutin 0.24, need to check if the above note is still valid
    let windowed_context = {
      let window_builder = glutin::window::WindowBuilder::new()
        .with_title         ("DFDM")
        .with_inner_size    (physical_size)
        .with_decorations   (false)
        .with_always_on_top (true)
        .with_resizable     (true);
      glutin::ContextBuilder::new()
        .with_gl (glutin::GlRequest::Specific (glutin::Api::OpenGl, (3,3)))
        .with_gl_profile    (glutin::GlProfile::Core)
        .with_vsync         (client_config.vsync)
        .with_multisampling (0)  // 0 = multisampling disabled
        .build_windowed     (window_builder, &event_loop).unwrap()
    };

    // window setup
    {
      let window = windowed_context.window();
      // mouse cursor
      match window.set_cursor_grab (true) {
        Ok  (_)   => {}
        Err (err) => {
          log::warn!("could not grab cursor: {:?}", err);
        }
      }
      window.set_cursor_visible (false);
      if crate::STATIC_CONFIG.fullscreen {
        // TODO: can we use exclusive fullscreen mode instead of borderless?
        window.set_fullscreen (
          Some (glutin::window::Fullscreen::Borderless (primary_monitor)));
      }
      let cursor_position     = glutin::dpi::PhysicalPosition::new (
        0.5 * client_config.resolution_x as f64,
        0.5 * client_config.resolution_y as f64
      );
      window.set_cursor_position (cursor_position).unwrap();
    }

    // pass windowed context to the av thread through a mutex
    {
      let mut windowed_context_lock = thread::WINDOWED_CONTEXT.lock().unwrap();
      debug_assert!(windowed_context_lock.is_none());
      *windowed_context_lock = Some (windowed_context);
    } {
      let mut event_loop_proxy_lock = thread::EVENT_LOOP_PROXY.lock().unwrap();
      debug_assert!(event_loop_proxy_lock.is_none());
      *event_loop_proxy_lock = Some (event_loop.create_proxy());
    }

    Main {
      input: Input::create (event_loop),
      gui:   Gui::create()
    }
  }
}
