// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

//
//  impl_keycode!
//
macro_rules! impl_keycode {
  ($($keycode : ident),+) => {
    #[derive(Clone,Copy,Debug,Eq,PartialEq,Hash,Serialize,Deserialize)]
    pub enum Keycode {
      $($keycode),+
    }

    impl From <glutin::event::VirtualKeyCode> for Keycode {
      fn from (keycode : glutin::event::VirtualKeyCode) -> Self {
        match keycode {
          $(glutin::event::VirtualKeyCode::$keycode => Keycode::$keycode),+
        }
      }
    }
  };
}
