// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use {std, glium::glutin};

pub mod keyboard;

pub struct Input {
  pub event_loop : Option <glutin::event_loop::EventLoop <()>>
}

impl Input {
  pub fn create (event_loop : glutin::event_loop::EventLoop <()>) -> Self {
    let event_loop = Some (event_loop);
    Input { event_loop }
  }
}
impl std::fmt::Debug for Input {
  fn fmt (&self, f : &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "Input")
  }
}
