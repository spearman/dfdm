// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use {cgmath, collision, image, num_traits};
use vec_map::VecMap;

use mat;

use crate::{mode, thread, STATIC_CONFIG};

////////////////////////////////////////////////////////////////////////////////
//  structs
////////////////////////////////////////////////////////////////////////////////

#[derive(Debug)]
pub struct Gui {
  pub screen_resolution : cgmath::Vector2 <u16>,
  pub mouse_position    : cgmath::Point2 <u16>,
  pub scale             : u8,
  pub windows           : VecMap <Window>,
  pub focused_window    : Option <usize>,
  // title main menu
  pub main_menu_active_option     : mode::title_menu::MainMenuOption,
  pub main_menu_button_dimensions : cgmath::Vector2 <u16>,
  pub main_menu_button_bounds     : [collision::Aabb2 <i16>; 4]
}

#[derive(Debug)]
pub struct Window {
  pub pixel_coords : cgmath::Vector2 <u16>,
  pub glyphs       : mat::Mat <Glyph>
}

// TODO
/*
pub struct ColorScheme {
  pub fg_color : [f32; 4],
  pub bg_color : [f32; 4],
  pub hi_color : [f32; 4],
  pub lo_color : [f32; 4]
}
*/

#[derive(Clone,Copy,Debug,PartialEq)]
pub struct Glyph {
  pub ch       : u8,
  pub fg_color : [u8; 4],
  pub bg_color : [u8; 4]
}

////////////////////////////////////////////////////////////////////////////////
//  impls
////////////////////////////////////////////////////////////////////////////////

impl Gui {
  pub fn create() -> Self {
    use cgmath::{ElementWise, EuclideanSpace};
    use image::GenericImageView;

    let screen_resolution = cgmath::Vector2::<u16>::new (
      STATIC_CONFIG.resolution_x,
      STATIC_CONFIG.resolution_y
    );
    // position mouse in screen center
    let mouse_position =
      cgmath::Point2::from_vec (screen_resolution.div_element_wise (2));
    let scale = STATIC_CONFIG.thread_config.main_config.gui_scale;
    let main_menu_active_option =
      mode::title_menu::MainMenuOption::Practice;
    let main_menu_button_dimensions = image::load_from_memory (
      thread::av::render::MAIN_MENU_BUTTON_PRACTICE_PNG_BYTES
    ).unwrap().dimensions();
    // make sure the other buttons are the same size
    debug_assert_eq!(image::load_from_memory (
      thread::av::render::MAIN_MENU_BUTTON_MULTIPLAYER_PNG_BYTES
    ).unwrap().dimensions(), main_menu_button_dimensions);
    debug_assert_eq!(image::load_from_memory (
      thread::av::render::MAIN_MENU_BUTTON_SETTINGS_PNG_BYTES
    ).unwrap().dimensions(), main_menu_button_dimensions);
    debug_assert_eq!(image::load_from_memory (
      thread::av::render::MAIN_MENU_BUTTON_EXIT_PNG_BYTES
    ).unwrap().dimensions(), main_menu_button_dimensions);
    let main_menu_button_dimensions =
      cgmath::Vector2::<u32>::from (main_menu_button_dimensions).cast().unwrap();
    let main_menu_button_bounds = mode::title_menu::main_menu_button_bounds (
      screen_resolution,
      scale,
      main_menu_button_dimensions);

    Gui {
      screen_resolution,
      mouse_position,
      scale,
      main_menu_active_option,
      main_menu_button_dimensions,
      main_menu_button_bounds,
      windows:        Default::default(),
      focused_window: Default::default()
    }
  }

  pub fn set_screen_resolution (&mut self, width : u16, height : u16) {
    self.screen_resolution = (width, height).into();
    // make sure mouse pointer stays within screen bounds
    thread::clamp_mouse_to_screen (
      &self.screen_resolution, &mut self.mouse_position);
    self.main_menu_button_bounds = mode::title_menu::main_menu_button_bounds (
      self.screen_resolution,
      self.scale,
      self.main_menu_button_dimensions);
  }

  pub fn set_mouse_position (&mut self, x : u16, y : u16) {
    self.mouse_position = (x,y).into();
    // make sure mouse pointer stays within screen boundaries
    thread::clamp_mouse_to_screen (
      &self.screen_resolution, &mut self.mouse_position);
  }

  pub fn main_menu_next (&mut self)
    -> Option <mode::title_menu::MainMenuOption>
  {
    let next_option = self.main_menu_active_option.next();
    if next_option != self.main_menu_active_option {
      self.main_menu_active_option = next_option.clone();
      Some (next_option)
    } else {
      None
    }
  }

  pub fn main_menu_prev (&mut self)
    -> Option <mode::title_menu::MainMenuOption>
  {
    let prev_option = self.main_menu_active_option.prev();
    if prev_option != self.main_menu_active_option {
      self.main_menu_active_option = prev_option.clone();
      Some (prev_option)
    } else {
      None
    }
  }

  pub fn main_menu_first (&mut self)
    -> Option <mode::title_menu::MainMenuOption>
  {
    use num_traits::Bounded;
    let first_option = mode::title_menu::MainMenuOption::min_value();
    if first_option != self.main_menu_active_option {
      self.main_menu_active_option = first_option.clone();
      Some (first_option)
    } else {
      None
    }
  }

  pub fn main_menu_last (&mut self)
    -> Option <mode::title_menu::MainMenuOption>
  {
    use num_traits::Bounded;
    let last_option = mode::title_menu::MainMenuOption::max_value();
    if last_option != self.main_menu_active_option {
      self.main_menu_active_option = last_option.clone();
      Some (last_option)
    } else {
      None
    }
  }
}
