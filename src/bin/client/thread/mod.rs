// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use std::sync;
use {cgmath, collision, glium::glutin};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};

use dfdm::{server, Simulation};

pub mod av;
pub mod main;
pub mod net;
pub mod sim;

pub use self::av::Av;
pub use self::main::Main;
pub use self::net::Net;

////////////////////////////////////////////////////////////////////////////////
//  statics                                                                   //
////////////////////////////////////////////////////////////////////////////////

lazy_static!{
  /// Used to transfer GL window from the main thread
  static ref WINDOWED_CONTEXT
    : sync::Mutex <Option <glutin::WindowedContext <glutin::NotCurrent>>>
    = sync::Mutex::new (None);
  /// Used to send 'wake up' events to the input (main) thread
  static ref EVENT_LOOP_PROXY
    : sync::Mutex <Option <glutin::event_loop::EventLoopProxy <()>>>
    = sync::Mutex::new (None);
  /// Used for initial simulation when starting multiplayer
  pub (crate) static ref SIMULATION_INIT
    : sync::Mutex <Option <(std::time::Instant, Simulation)>>
    = sync::Mutex::new (None);
  pub (crate) static ref CLIENT_ID : sync::RwLock <Option <server::ClientId>>
    = sync::RwLock::new (None);
}

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  pub av_config   : av::StaticConfig,
  pub main_config : main::StaticConfig,
  pub net_config  : net::StaticConfig
}

////////////////////////////////////////////////////////////////////////////////
//  enums                                                                     //
////////////////////////////////////////////////////////////////////////////////

/// Default messages from main thread to av thread.
#[derive(Debug)]
pub enum MainToAvMsg {
  MousePosition {
    x : u16,
    y : u16
  },
  WindowSize {
    x : u16,
    y : u16
  },
  Screenshot,
  Quit {
    reset_audio_on_term : bool
  }
}

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

/// Returns `true` if mouse position was modified.
pub fn clamp_mouse_to_screen (
  screen_resolution : &cgmath::Vector2 <u16>,
  mouse_position    : &mut cgmath::Point2 <u16>
) -> bool {
  use cgmath::EuclideanSpace;
  use collision::Contains;
  let screen_aabb = collision::Aabb2::<u16>::new (
    (0,0).into(),
    cgmath::Point2::from_vec (*screen_resolution));

  if !screen_aabb.contains (mouse_position) {
    use std::cmp::{max, min};
    if cfg!(debug_assertions) {
      log::warn!("client::thread::av::Render: {}", format!(
        "mouse position ({:?}) outside screen dimensions ({:?}",
        mouse_position, screen_resolution));
    }
    // clamp
    *mouse_position = [
      max (min (screen_aabb.max.x, mouse_position.x), screen_aabb.min.x),
      max (min (screen_aabb.max.y, mouse_position.y), screen_aabb.min.y)
    ].into();
    true
  } else {
    false
  }
}
