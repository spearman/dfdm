// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use {std, glium::glutin, rfmod};
use lazy_static::lazy_static;
use rand_xorshift::XorShiftRng;
use serde::{Deserialize, Serialize};

use rs_utils::show;
use fmod_utils::rfmod_ok;

use df;

use crate::{thread, ARENA2_PATH};

pub mod render;
pub mod audio;

pub use self::render::Render;
pub use self::audio::Audio;

////////////////////////////////////////////////////////////////////////////////
//  config                                                                    //
////////////////////////////////////////////////////////////////////////////////

#[derive(Serialize, Deserialize)]
pub struct StaticConfig {
  pub midi_dls_path         : String,
  pub debug_frames          : u32,
  // render config
  pub fps_max               : u32,
  pub fov                   : f32,
  pub debug_grid_size       : u16,
  pub debug_grid_color      : (u8, u8, u8, u8),
  pub debug_sphere_color    : (u8, u8, u8, u8),
  pub debug_cylinder_color  : (u8, u8, u8, u8),
  pub debug_capsule_color   : (u8, u8, u8, u8)
}

////////////////////////////////////////////////////////////////////////////////
//  statics                                                                   //
////////////////////////////////////////////////////////////////////////////////

lazy_static! {
  pub static ref STATIC_CONFIG : &'static StaticConfig =
    &crate::STATIC_CONFIG.thread_config.av_config;
  /// Minimum frame time calculated from `STATIC_CONFIG.fps_max`.
  static ref MIN_FRAME_DURATION : std::time::Duration =
    std::time::Duration::from_micros (1_000_000
      / STATIC_CONFIG.fps_max as u64);
}

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

pub struct Av {
  pub render            : Render,
  pub audio             : Audio,
  pub event_loop_proxy  : glutin::event_loop::EventLoopProxy <()>,
  pub rng               : XorShiftRng,
  pub frame             : u64,
  pub frame_fps         : u64,
  pub frame_debug       : u64,
  pub fps               : u16,
  pub t_frame           : std::time::SystemTime,
  pub t_fps             : std::time::SystemTime,
}

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

pub fn report_sizes() {
  use std::mem::size_of;
  println!("av report sizes...");
  show!(size_of::<Av>());
  println!("...av report sizes");
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

impl Av {

  pub fn create() -> Self {
    use rand::SeedableRng;
    let event_loop_proxy = thread::EVENT_LOOP_PROXY.lock().unwrap().take()
      .unwrap();
    log::info!("thread::Av loading ARENA2_PATH: {:?}", *ARENA2_PATH);
    let df::Resources { graphics, audio } =
      df::Resources::load (Some (ARENA2_PATH.clone()));
    Av {
      event_loop_proxy,
      render:       Render::create (graphics),
      audio:        Audio::create  (audio),
      rng:          XorShiftRng::seed_from_u64 (0),
      frame:        0,
      frame_fps:    0,
      frame_debug:  0,
      fps:          0,
      t_frame:      std::time::SystemTime::now(),
      t_fps:        std::time::SystemTime::now()
    }
  }

  pub fn do_frame (&mut self) {
    log::trace!("thread::Av::do_frame...");

    // set listener position
    let listener_position = rfmod::Vector {
      x: self.render.view_position.x,
      y: self.render.view_position.y,
      z: self.render.view_position.z
    };
    // TODO: listener velocity
    let listener_velocity = rfmod::Vector { x: 0.0, y: 0.0, z: 0.0 };
    let forward = rfmod::Vector {
      x: self.render.view_orientation.as_ref()[1].x,
      y: self.render.view_orientation.as_ref()[1].y,
      z: self.render.view_orientation.as_ref()[1].z
    };
    let up = rfmod::Vector {
      x: self.render.view_orientation.as_ref()[2].x,
      y: self.render.view_orientation.as_ref()[2].y,
      z: self.render.view_orientation.as_ref()[2].z
    };
    rfmod_ok!(self.audio.rfmod_sys.set_3D_listener_attributes (
      0, &listener_position, &listener_velocity, &forward, &up
    ));
    // fmod sys update
    rfmod_ok!(self.audio.rfmod_sys.update());

    let t_now = std::time::SystemTime::now();
    if t_now.duration_since (self.t_frame).unwrap() < *MIN_FRAME_DURATION {
      std::thread::sleep (
        (self.t_frame + *MIN_FRAME_DURATION).duration_since (t_now).unwrap());
    }
    self.t_frame = std::time::SystemTime::now();
    self.frame += 1;
    // calculate FPS once per second
    if std::time::Duration::from_secs (1)
      < self.t_frame.duration_since (self.t_fps).unwrap()
    {
      debug_assert!(self.frame_fps < self.frame);
      debug_assert!((self.frame - self.frame_fps) <= std::u16::MAX as u64);
      self.fps       = (self.frame - self.frame_fps) as u16;
      self.frame_fps = self.frame;
      self.t_fps     = std::time::SystemTime::now();
    }
    // debug frame
    if self.frame == self.frame_debug + STATIC_CONFIG.debug_frames as u64 {
      self.frame_debug = self.frame;
      /*FIXME:debug*/
      log::info!("thread::Av: FPS: {}", self.fps);
    }
    log::trace!("...thread::Av::do_frame");
  }

}
