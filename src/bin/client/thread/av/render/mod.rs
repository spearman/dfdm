// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use std::{self, time};
use {cgmath, image, glium, num_traits};
use {rs_utils::{self, /*show*/}, gl_utils};
use vec_map::VecMap;

use enum_unitary::enum_unitary;
use gl_utils::color;

use df;

use dfdm::simulation::world::{player, Player};

use crate::{mode, thread};

pub mod mesh;
pub mod shader;

////////////////////////////////////////////////////////////////////////////////
//  constants                                                                 //
////////////////////////////////////////////////////////////////////////////////

// simulation
// TODO: make configurable ?
pub const PLAYER_COUNT       : usize          = 16;
// TODO: set to 5 for blood effect, how to handle effects with other frame
// counts?
pub const EFFECT_FRAMES      : usize          = 5;
/// Show player wounded frame for this duration when triggered
pub const PLAYER_WOUNDED_DUR : time::Duration =
  time::Duration::from_millis (300);

// gui
pub const MOUSE_POINTER_PNG_BYTES                : &'static [u8; 211]  =
  include_bytes!("../../../../../../include/mouse-pointer.png");
pub const MAIN_MENU_BUTTON_PRACTICE_PNG_BYTES    : &'static [u8; 629]  =
  include_bytes!("../../../../../../include/main-menu-practice.png");
pub const MAIN_MENU_BUTTON_MULTIPLAYER_PNG_BYTES : &'static [u8; 629]  =
  include_bytes!("../../../../../../include/main-menu-multiplayer.png");
pub const MAIN_MENU_BUTTON_SETTINGS_PNG_BYTES    : &'static [u8; 659]  =
  include_bytes!("../../../../../../include/main-menu-settings.png");
pub const MAIN_MENU_BUTTON_EXIT_PNG_BYTES        : &'static [u8; 661]  =
  include_bytes!("../../../../../../include/main-menu-exit.png");
pub const MULTI_SETUP_BUTTON_CONNECT_PNG_BYTES   : &'static [u8; 1086] =
  include_bytes!("../../../../../../include/multi-setup-connect.png");
pub const TILESET_DOS_FALL_6X9_PNG_BYTES         : &'static [u8; 2776] =
  include_bytes!("../../../../../../include/dos_fall_6x9.png");

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

pub struct Render {
  // context
  pub glium_display                 : glium::Display,
  pub shader_programs               : VecMap <glium::Program>,
  pub resolution                    : cgmath::Vector2 <u16>,
  pub frame_time                    : time::Instant,
  pub frame_time_last               : time::Instant,
  pub frame_duration                : time::Duration,

  // 2d rendering
  pub projection_mat_ortho          : cgmath::Matrix4 <f32>,
  // a screen quad for fading colors over the screen
  pub screen_quad_vertex_buffer     : glium::VertexBuffer <shader::Vert2dColor>,
  // 3d rendering
  pub projection_mat_perspective    : cgmath::Matrix4 <f32>,
  pub transform_mat_view            : cgmath::Matrix4 <f32>,
  pub view_position                 : cgmath::Point3  <f32>,
  pub view_orientation              : cgmath::Basis3  <f32>,
  /// Player ID, active attack kind, last known health, and hurt alpha fade
  /// percentage
  pub view_player                   :
    Option <(player::Id, player::AttackKind, u16, f32)>,

  // debug resources
  pub origin_vertex                 : glium::VertexBuffer
    <shader::Vert3dOrientation>,
  // grid mesh
  pub mesh_grid_vertex_buffer       : glium::VertexBuffer
    <shader::Vert3dInstanced>,
  pub mesh_grid_index_buffer        : glium::IndexBuffer <u32>,
  pub mesh_grid_per_instance_vertex_buffer : glium::VertexBuffer
    <shader::Vert3dScale>,
  // sphere mesh
  pub mesh_sphere_vertex_buffer     : glium::VertexBuffer
    <shader::Vert3dInstanced>,
  pub mesh_sphere_index_buffer      : glium::IndexBuffer <u32>,
  // cylinder mesh
  pub mesh_cylinder_vertex_buffer   : glium::VertexBuffer
    <shader::Vert3dInstanced>,
  pub mesh_cylinder_index_buffer    : glium::IndexBuffer <u32>,
  // capsule mesh
  pub mesh_capsule_vertex_buffer    : glium::VertexBuffer
    <shader::Vert3dInstanced>,
  pub mesh_capsule_index_buffer     : glium::IndexBuffer <u32>,

  // simulation resources
  pub player_sprite_textures        : glium::texture::Texture2dArray,
  pub player_animation_state        : VecMap <PlayerAnimationState>,
  pub player_vertex_data            : VecMap
    <shader::Vert3dOrientationScaleSpriteArrayFrames>,
  pub player_vertex_data_packed     : Vec
    <shader::Vert3dOrientationScaleSpriteArrayFrames>,
  pub player_vertex_buffer          : glium::VertexBuffer
    <shader::Vert3dOrientationScaleSpriteArrayFrames>,
  pub weapon_graphic_textures       : glium::texture::Texture2dArray,
  pub weapon_graphic_vertex_buffer  : glium::VertexBuffer
    <shader::Vert2dSpriteArrayFrames>,
  pub corpse_sprite_texture         : glium::Texture2d,
  pub corpse_vertex_data            : Vec <shader::Vert3d>,
  pub corpse_vertex_buffer          : glium::VertexBuffer <shader::Vert3d>,
  /// 64x64
  pub effect_sprite_textures        : glium::texture::Texture2dArray,
  pub effect_animation_state        : Vec <EffectAnimationState>,
  pub effect_vertex_data            : Vec <shader::Vert3dSpriteArrayFrames>,
  pub effect_vertex_buffer          : glium::VertexBuffer
    <shader::Vert3dSpriteArrayFrames>,

  // gui
  pub gui_scale                     : u8,
  pub tilesets                      : VecMap <Tileset>,
  pub mouse_position                : cgmath::Point2 <u16>,
  pub last_mouse_position           : cgmath::Point2 <u16>,
  pub mouse_pointer_texture         : glium::Texture2d,
  pub mouse_pointer_vertex_buffer   : glium::VertexBuffer <shader::Vert2dSprite>,
  pub bg_textures                   : glium::texture::Texture2dArray,
  pub bg_vertex_buffer              : glium::VertexBuffer
    <shader::Vert2dSpriteArray>,

  // title menu mode resources
  pub main_menu_cursor_texture       : glium::Texture2d,
  pub main_menu_cursor_vertex_buffer : glium::VertexBuffer
    <shader::Vert2dSprite>,
  pub main_menu_button_textures      : glium::texture::Texture2dArray,
  pub main_menu_button_vertex_buffer : glium::VertexBuffer
    <shader::Vert2dSpriteArrayColorize>,
  pub main_menu_active_option        : mode::title_menu::MainMenuOption,
  // multi setup resources
  pub connect_to_tile_vertex_buffer  : glium::VertexBuffer <shader::Vert2dTile>,
  pub connect_to_box_texture         : glium::texture::Texture2d,
  pub connect_to_box_vertex_buffer   : glium::VertexBuffer <shader::Vert2dSprite>
}

pub struct Tileset {
  pub dimensions      : cgmath::Vector2 <u16>,
  pub tile_dimensions : cgmath::Vector2 <u8>,
  pub texture         : glium::texture::Texture2d
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct PlayerAnimationState {
  pub walk_cycle : Option <f64>,
  pub wounded    : Option <time::Instant>
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct EffectAnimationState {
  /// Normalized frame cycle position 0.0-1.0
  pub cycle         : f64,
  /// Length of one cycle in seconds
  pub cycle_seconds : f64,
  pub cycle_loop    : bool
}

////////////////////////////////////////////////////////////////////////////////
//  enums                                                                     //
////////////////////////////////////////////////////////////////////////////////

// indexes into background array texture
enum_unitary!{
  pub enum BackgroundId {
    Court,
    Sky,
    SkySepia
  }
}

// indexes into tileset structure
enum_unitary!{
  #[derive(Copy, Debug, Eq, PartialEq)]
  pub enum TilesetId {
    Default
  }
}

// animation frame numbers representing a row in the player sprite sheets
enum_unitary!{
  pub enum PlayerSpriteFrame {
    Standing, //  0
    Walk1,    //  1
    Walk2,    //  2
    Walk3,    //  3
    Walk4,    //  4
    Melee1,   //  5
    Melee2,   //  6
    Melee3,   //  7
    Melee4,   //  8
    Melee5,   //  9
    Melee6,   // 10
    Hurt      // 11
  }
}

// used as index into effect sprites array texture (64x64)
enum_unitary!{
  pub enum EffectId {
    Blood, // 0
  }
}

////////////////////////////////////////////////////////////////////////////////
//  functions                                                                 //
////////////////////////////////////////////////////////////////////////////////

pub fn player_vertex (
  step            : u64,
  frame_time      : time::Instant,
  player          : &Player,
  animation_state : &mut PlayerAnimationState
) -> shader::Vert3dOrientationScaleSpriteArrayFrames {
  use cgmath::InnerSpace;
  let class_data = player.class.data();
  const WALK_SPEED_THRESHOLD : f64 = 0.01;
  const WALK_BASE_FRAME      : u32 = 1;    // first frame of the walk cycle
  const WALK_CYCLE_LEN       : f64 = 4.0;  // four frames
  const WALK_CYCLE_FRAME_LEN : f64 = 1.0/WALK_CYCLE_LEN;  // 0.25
  // TODO: make this take into account step length ?
  const WALK_CYCLE_SPEED_SQ  : f64 = 0.4;  // distance squared travelled per frame
  let speed_sq = player.velocity.magnitude2();
  // check timeout for wounded state
  if let Some (t) = animation_state.wounded {
    if frame_time.duration_since (t) > PLAYER_WOUNDED_DUR {
      animation_state.wounded = None;
    }
  }
  if !player.wounds.is_empty() {
    animation_state.wounded = Some (frame_time);
  }
  // determine animation frame
  let frame = if animation_state.wounded.is_some() {
    PlayerSpriteFrame::Hurt
  } else {
    match player.attack_state.as_ref() {
      None => { // not attacking
        if speed_sq > WALK_SPEED_THRESHOLD {
          match player.jump_state {
            player::JumpState::Standing => {
              if let Some (mut cycle_position) = animation_state.walk_cycle.clone() {
                use num_traits::FromPrimitive;
                cycle_position += (speed_sq / WALK_CYCLE_SPEED_SQ) * WALK_CYCLE_FRAME_LEN;
                cycle_position  = cycle_position - cycle_position.floor();
                animation_state.walk_cycle = Some (cycle_position);
                PlayerSpriteFrame::from_u32 (
                  (cycle_position * WALK_CYCLE_LEN) as u32 + WALK_BASE_FRAME
                ).unwrap()
              } else {
                animation_state.walk_cycle = Some (0.0);
                PlayerSpriteFrame::Walk1
              }
            }
            // TODO: using "jump state" does not seem to be a reliable way to tell if
            // the player is touching the ground or not?
            player::JumpState::Jumping => PlayerSpriteFrame::Walk1
          }
        } else {
          animation_state.walk_cycle = None;
          PlayerSpriteFrame::Standing
        }
      }
      Some (attack_state) => {
        debug_assert!(attack_state.step_start < step);
        debug_assert!(step <= attack_state.step_end);
        match attack_state.attack_kind {
          player::AttackKind::Melee (_) => {
            use num_traits::FromPrimitive;
            const MELEE_BASE_FRAME : u32 = 5;    // first frame of the melee cycle
            // animation is six frames but start and end frames are repeated for
            // a total of 12 frames
            const MELEE_CYCLE_LEN  : f64 = 12.0;
            let melee_attack_steps : f64 =
              (attack_state.step_end - attack_state.step_start) as f64;
            // NB: this step goes from [1..melee_attack_steps], i.e. '0' is
            // never seen, so we safely subtract 1
            let melee_attack_step  : f64 =
              (step - attack_state.step_start - 1) as f64;
            let cycle_position = melee_attack_step / melee_attack_steps;
            let frame = match (cycle_position * MELEE_CYCLE_LEN) as u32 {
              0 | 1 | 2 => 0,
              3 | 4 => 1,
              5 => 2,
              6 => 3,
              7 | 8=> 4,
              9 | 10 | 11 => 5,
              _     => unreachable!()   // TODO: compiler hint ?
            };
            PlayerSpriteFrame::from_u32 (MELEE_BASE_FRAME + frame).unwrap()
          }
          _ => unimplemented!("TODO: missile, spell attack animations")
        }
      }
    }
  } as u32;
  shader::Vert3dOrientationScaleSpriteArrayFrames {
    in_position:      player.position.cast().unwrap().into(),
    in_orientation:   player.orientation.as_ref().cast().unwrap().into(),
    in_scale:         [
      0.5 * class_data.width  as f32,
      0.5 * class_data.width  as f32,
      0.5 * class_data.height as f32
    ],
    in_texture_index: player.class as u32,
    in_frame:         frame
  }
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

impl Render {
  // TODO: mouse currently starts at 0,0, should it be moved to center of screen?
  pub fn create (graphics : df::Graphics) -> Self {
    use num_traits::Bounded;
    use cgmath::{EuclideanSpace, One};

    let client_config = &crate::STATIC_CONFIG;
    let av_config     = &thread::av::STATIC_CONFIG;
    let main_config   = &thread::main::STATIC_CONFIG;

    let resolution    = cgmath::Vector2::<u16>::new (
      client_config.resolution_x, client_config.resolution_y);
    let gui_scale     = main_config.gui_scale;

    let windowed_context = (*thread::WINDOWED_CONTEXT.lock().unwrap()).take()
      .unwrap();
    debug_assert!(!windowed_context.is_current());
    let windowed_context = unsafe { windowed_context.make_current().unwrap() };
    debug_assert!(windowed_context.is_current());
    let glium_display = glium::Display::from_gl_window (windowed_context)
      .unwrap();

    let frame_time      = time::Instant::now();
    let frame_time_last = frame_time;
    let frame_duration  = time::Duration::from_millis (0);

    // shaders and uniforms
    let shader_programs = shader::build_programs (&glium_display).unwrap();
    let projection_mat_ortho = {
      let left    = 0.0;
      let right   = resolution.x as f32;
      let bottom  = 0.0;
      let top     = resolution.y as f32;
      let near    = -1.0;
      let far     = 1.0;
      cgmath::ortho::<f32> (left, right, bottom, top, near, far)
    };
    // view
    let view_player        = None;
    let view_position      = cgmath::Point3::<f32>::origin();
    let view_orientation   = cgmath::Basis3::<f32>::one();
    let transform_mat_view = gl_utils::camera3d::transform_mat_world_to_view (
      &view_position, &view_orientation);
    let projection_mat_perspective = {
      let fovy    = cgmath::Deg (thread::av::STATIC_CONFIG.fov);
      let aspect  = resolution.x as f32 / resolution.y as f32;
      let near    = 0.1;
      let far     = 1000.0;
      cgmath::perspective::<f32, cgmath::Deg <f32>> (fovy, aspect, near, far)
    };
    // full screen quad for fading colors
    let screen_quad_vertex_buffer = glium::VertexBuffer::dynamic (
      &glium_display,
      &[
        shader::Vert2dColor {
          in_position: [-1.0,  1.0],
          in_color:    [1.0, 0.0, 0.0, 0.0]
        },
        shader::Vert2dColor {
          in_position: [-1.0, -1.0],
          in_color:    [1.0, 0.0, 0.0, 0.0]
        },
        shader::Vert2dColor {
          in_position: [ 1.0,  1.0],
          in_color:    [1.0, 0.0, 1.0, 0.0]
        },
        shader::Vert2dColor {
          in_position: [ 1.0, -1.0],
          in_color:    [1.0, 0.0, 0.0, 0.0]
        }
      ]
    ).unwrap();

    // debug
    let origin_vertex = glium::VertexBuffer::dynamic (
      &glium_display,
      &[shader::Vert3dOrientation {
        in_position:    [0.0, 0.0, 0.0],
        in_orientation: *cgmath::Matrix3::one().as_ref()
      }]
    ).unwrap();

    let mesh_sphere_vertex_buffer = glium::VertexBuffer::immutable (
      &glium_display, mesh::sphere_vertex_data().as_slice()).unwrap();
    let mesh_sphere_index_buffer = glium::IndexBuffer::immutable (
      &glium_display,
      glium::index::PrimitiveType::LinesList,
      mesh::sphere_index_data().as_slice()
    ).unwrap();
    let mesh_cylinder_vertex_buffer = glium::VertexBuffer::immutable (
      &glium_display, mesh::cylinder_vertex_data().as_slice()).unwrap();
    let mesh_cylinder_index_buffer = glium::IndexBuffer::immutable (
      &glium_display,
      glium::index::PrimitiveType::LinesList,
      mesh::cylinder_index_data().as_slice()
    ).unwrap();
    let mesh_capsule_vertex_buffer = glium::VertexBuffer::immutable (
      &glium_display, mesh::capsule_vertex_data().as_slice()).unwrap();
    let mesh_capsule_index_buffer = glium::IndexBuffer::immutable (
      &glium_display,
      glium::index::PrimitiveType::LinesList,
      mesh::capsule_index_data().as_slice()
    ).unwrap();

    //
    // simulation
    //

    // player sprites
    let player_sprite_textures = {
      let raw_images = {
        let mut v = Vec::new();
        let (dimensions, bytes) = graphics.image_player_mage_thief_f;
        let raw_image_2d =
          glium::texture::RawImage2d::from_raw_rgba (bytes, dimensions);
        v.push (raw_image_2d);
        let (dimensions, bytes) = graphics.image_player_fighter_light_m;
        let raw_image_2d =
          glium::texture::RawImage2d::from_raw_rgba (bytes, dimensions);
        v.push (raw_image_2d);
        // TODO: more player sprites
        v
      };
      glium::texture::Texture2dArray::with_mipmaps (
        &glium_display, raw_images, glium::texture::MipmapsOption::NoMipmap
      ).unwrap()
    };
    // TODO: make these capacities configurable
    let player_animation_state    = VecMap::with_capacity (PLAYER_COUNT);
    let player_vertex_data        = VecMap::with_capacity (PLAYER_COUNT);
    let player_vertex_data_packed = Vec::with_capacity    (PLAYER_COUNT);
    let player_vertex_buffer      =
      glium::VertexBuffer::dynamic (&glium_display, &[]).unwrap();

    // weapon graphics
    let weapon_graphic_textures = {
      let raw_images = {
        let mut v = Vec::new();
        let (dimensions, bytes) = graphics.image_weapon_sword;
        let raw_image_2d =
          glium::texture::RawImage2d::from_raw_rgba (bytes, dimensions);
        v.push (raw_image_2d);
        // TODO: more weapon graphics
        v
      };
      glium::texture::Texture2dArray::with_mipmaps (
        &glium_display, raw_images, glium::texture::MipmapsOption::NoMipmap
      ).unwrap()
    };
    // weapon sprite vertex
    let weapon_graphic_vertex_buffer = glium::VertexBuffer::dynamic (
      &glium_display,
      &[shader::Vert2dSpriteArrayFrames {
        in_position:      [0.0, 0.0],
        in_dimensions:    [2.0, 2.0],  // -1.0 to 1.0 in both dimensions
        in_texture_index: 0,  // sword
        in_row:           0,  // cycle
        in_column:        0   // frame
      }]
    ).unwrap();
    // corpse sprite texture
    let corpse_sprite_texture = {
      let (dimensions, bytes) = graphics.image_corpse;
      let raw_image_2d =
        glium::texture::RawImage2d::from_raw_rgba (bytes, dimensions);
      glium::Texture2d::with_mipmaps (
        &glium_display, raw_image_2d, glium::texture::MipmapsOption::NoMipmap
      ).unwrap()
    };
    let corpse_vertex_data     = Vec::new();
    let corpse_vertex_buffer   =
      glium::VertexBuffer::dynamic (&glium_display, &[]).unwrap();
    // 3d effects
    let effect_sprite_textures = {
      let raw_images = {
        let mut v = Vec::new();
        // blood
        let (dimensions, bytes) = graphics.image_blood;
        let raw_image_2d =
          glium::texture::RawImage2d::from_raw_rgba (bytes, dimensions);
        v.push (raw_image_2d);
        v
      };
      glium::texture::Texture2dArray::with_mipmaps (
        &glium_display, raw_images, glium::texture::MipmapsOption::NoMipmap
      ).unwrap()
    };
    let effect_animation_state = Vec::new();
    let effect_vertex_data     = Vec::new();
    let effect_vertex_buffer   =
      glium::VertexBuffer::dynamic (&glium_display, &[]).unwrap();

    // gui
    let mouse_position      = cgmath::Point2::<u16>::new (0, 0);
    let last_mouse_position = cgmath::Point2::<u16>::new (0, 0);

    let tilesets        = {
      use enum_unitary::EnumUnitary;
      let mut v = VecMap::new();
      for tileset_id in TilesetId::iter_variants() {
        assert!{
          v.insert (
            tileset_id as usize,
            Tileset::load (&glium_display, tileset_id.bytes())
          ).is_none()
        }
      }
      v
    };
    // mouse pointer texture
    let mouse_pointer_texture =
      gl_utils::texture::texture2d_with_mipmaps_from_bytes (
        &glium_display,
        MOUSE_POINTER_PNG_BYTES,
        image::ImageFormat::Png,
        glium::texture::MipmapsOption::NoMipmap
      ).unwrap();
    // mouse pointer sprite vertex
    let mouse_pointer_width  : u16  =
      mouse_pointer_texture.width()  as u16 * gui_scale as u16;
    let mouse_pointer_height : u16  =
      mouse_pointer_texture.height() as u16 * gui_scale as u16;
    let mouse_pointer_vertex_buffer = glium::VertexBuffer::dynamic (
      &glium_display,
      &[shader::Vert2dSprite {
        in_position:   [0.0, 0.0],
          //(mouse_position.x + mouse_pointer_width/2) as f32,
          //(resolution.y as i32
          //  - mouse_position.y as i32
          //  - mouse_pointer_height as i32/2) as f32],
        in_dimensions: [
          mouse_pointer_width  as f32,
          mouse_pointer_height as f32]
      }]
    ).unwrap();

    // main menu active option
    let main_menu_active_option =
      mode::title_menu::MainMenuOption::min_value();

    // multi setup resources
    let tiles : Vec <shader::Vert2dTile> = {
      let tile_count = *mode::multi_setup::TILE_COUNT;
      let mut v = Vec::with_capacity (tile_count);
      v.resize (
        tile_count,
        shader::Vert2dTile { in_row: 1, in_column: -10, in_tile: 0x00 });
      v
    };
    let connect_to_tile_vertex_buffer =
      glium::VertexBuffer::dynamic (&glium_display, &tiles[..]).unwrap();
    let connect_to_box_texture =
      gl_utils::texture::texture2d_with_mipmaps_from_bytes (
        &glium_display,
        MULTI_SETUP_BUTTON_CONNECT_PNG_BYTES,
        image::ImageFormat::Png,
        glium::texture::MipmapsOption::NoMipmap
      ).unwrap();
    let connect_to_box_dimensions = cgmath::vec2 (
      connect_to_box_texture.width()  as u16,
      connect_to_box_texture.height() as u16);
    let connect_to_box_vertex = mode::multi_setup::connect_to_box_vertex (
      connect_to_box_dimensions, resolution, gui_scale);
    let connect_to_box_vertex_buffer = glium::VertexBuffer::dynamic (
      &glium_display,
      &[connect_to_box_vertex]
    ).unwrap();

    // background textures
    let bg_textures = {
      let raw_images = {
        let mut v = Vec::new();
        // bg court
        let (dimensions, bytes) = graphics.image_court;
        let raw_image_2d =
          glium::texture::RawImage2d::from_raw_rgba (bytes, dimensions);
        v.push (raw_image_2d);
        // bg sky
        let (dimensions, bytes) = graphics.image_sky;
        let raw_image_2d =
          glium::texture::RawImage2d::from_raw_rgba (bytes.clone(), dimensions);
        v.push (raw_image_2d);
        // bg sky sepia
        let (dimensions, bytes) = graphics.image_sky_sepia;
        let raw_image_2d =
          glium::texture::RawImage2d::from_raw_rgba (bytes.clone(), dimensions);
        v.push (raw_image_2d);
        v
      };
      glium::texture::Texture2dArray::with_mipmaps (
        &glium_display, raw_images, glium::texture::MipmapsOption::NoMipmap
      ).unwrap()
    };

    // background sprite vertex
    let bg_vertex_buffer = glium::VertexBuffer::dynamic (
      &glium_display,
      &[shader::Vert2dSpriteArray {
        in_position:      [0.0, 0.0],
        in_dimensions:    [2.0, 2.0],   // -1.0 to 1.0 in both dimensions
        in_texture_index: std::u32::MAX
      }]
    ).unwrap();

    // main menu button textures
    let main_menu_button_textures =
      gl_utils::texture::texture2darray_with_mipmaps_from_bytes (
        &glium_display,
        vec![
          &MAIN_MENU_BUTTON_PRACTICE_PNG_BYTES[..],
          &MAIN_MENU_BUTTON_MULTIPLAYER_PNG_BYTES[..],
          &MAIN_MENU_BUTTON_SETTINGS_PNG_BYTES[..],
          &MAIN_MENU_BUTTON_EXIT_PNG_BYTES[..]
        ],
        image::ImageFormat::Png,
        glium::texture::MipmapsOption::NoMipmap
      ).unwrap();
    let main_menu_button_width  : u16 =
      main_menu_button_textures.width()  as u16;
    let main_menu_button_height : u16 =
      main_menu_button_textures.height() as u16;
    // main menu button vertices
    let main_menu_button_vertices = mode::title_menu::main_menu_button_vertices (
      (main_menu_button_width, main_menu_button_height).into(),
      resolution,
      gui_scale,
      main_menu_active_option);
    // main menu vertex buffer
    let main_menu_button_vertex_buffer = glium::VertexBuffer::dynamic (
      &glium_display,
      &main_menu_button_vertices
    ).unwrap();

    // main menu cursor texture
    let main_menu_cursor_texture = {
      let (dimensions, bytes) = graphics.image_cursor_sword;
      let raw_image_2d =
        glium::texture::RawImage2d::from_raw_rgba (bytes, dimensions);
      glium::Texture2d::with_mipmaps (
        &glium_display, raw_image_2d, glium::texture::MipmapsOption::NoMipmap
      ).unwrap()
    };
    // main menu cursor vertex buffer
    let main_menu_cursor_width  : u16  = main_menu_cursor_texture.width()
      as u16;
    let main_menu_cursor_height : u16  = main_menu_cursor_texture.height()
      as u16;
    let main_menu_cursor_vertex_buffer = glium::VertexBuffer::dynamic (
      &glium_display,
      &mode::title_menu::main_menu_cursor_vertex (
        (main_menu_cursor_width, main_menu_cursor_height).into(),
        resolution,
        gui_scale,
        main_menu_active_option,
        &main_menu_button_vertices)
    ).unwrap();

    // practice mode debug grid
    let grid_dims = av_config.debug_grid_size;
    let (grid_vertex_data, grid_index_data) =
      mesh::grid_vertex_and_index_data (grid_dims as u32);
    let mesh_grid_vertex_buffer = glium::VertexBuffer::immutable (
      &glium_display, grid_vertex_data.as_slice()
    ).unwrap();
    let mesh_grid_index_buffer  = glium::IndexBuffer::immutable (
      &glium_display,
      glium::index::PrimitiveType::LinesList,
      grid_index_data.as_slice()
    ).unwrap();
    let mesh_grid_per_instance_vertex_buffer = glium::VertexBuffer::dynamic (
      &glium_display,
      &[
        shader::Vert3dScale {
          in_position: [0.0, 0.0, 0.0],
          in_scale:    [1.0, 1.0, 1.0]
        }
      ]
    ).unwrap();

    Render {
      // context
      glium_display,
      shader_programs,
      resolution,
      frame_time,
      frame_time_last,
      frame_duration,
      // camera
      projection_mat_ortho,
      projection_mat_perspective,
      transform_mat_view,
      view_position,
      view_orientation,
      view_player,
      screen_quad_vertex_buffer,
      // debug resources
      origin_vertex,
      gui_scale,
      mesh_sphere_vertex_buffer,
      mesh_sphere_index_buffer,
      mesh_cylinder_vertex_buffer,
      mesh_cylinder_index_buffer,
      mesh_capsule_vertex_buffer,
      mesh_capsule_index_buffer,
      // simulation resources
      player_sprite_textures,
      player_animation_state,
      player_vertex_data,
      player_vertex_data_packed,
      player_vertex_buffer,
      weapon_graphic_textures,
      weapon_graphic_vertex_buffer,
      corpse_sprite_texture,
      corpse_vertex_data,
      corpse_vertex_buffer,
      effect_sprite_textures,
      effect_animation_state,
      effect_vertex_data,
      effect_vertex_buffer,
      // gui resources
      tilesets,
      mouse_position,
      last_mouse_position,
      mouse_pointer_texture,
      mouse_pointer_vertex_buffer,
      bg_textures,
      bg_vertex_buffer,
      // title menu mode resources
      main_menu_cursor_texture,
      main_menu_cursor_vertex_buffer,
      main_menu_button_textures,
      main_menu_button_vertex_buffer,
      main_menu_active_option,
      // multi setup resources
      connect_to_tile_vertex_buffer,
      connect_to_box_texture,
      connect_to_box_vertex_buffer,
      // practice mode resources
      mesh_grid_vertex_buffer,
      mesh_grid_index_buffer,
      mesh_grid_per_instance_vertex_buffer
    }
  }

  pub fn set_resolution (&mut self, width : u16, height : u16) {
    self.resolution           = (width, height).into();
    // make sure mouse pointer stays within screen boundaries
    thread::clamp_mouse_to_screen (&self.resolution, &mut self.mouse_position);
    self.projection_mat_ortho = {
      let left    = 0.0;
      let right   = width as f32;
      let bottom  = 0.0;
      let top     = height as f32;
      let near    = -1.0;
      let far     = 1.0;
      cgmath::ortho::<f32> (left, right, bottom, top, near, far)
    };
    self.projection_mat_perspective = {
      let fovy    = cgmath::Deg (thread::av::STATIC_CONFIG.fov);
      let aspect  = width as f32 / height as f32;
      let near    = 0.1;
      let far     = 1000.0;
      cgmath::perspective::<f32, cgmath::Deg <f32>> (fovy, aspect, near, far)
    };
    self.main_menu_vertex_buffers_update();
    self.multi_setup_vertex_buffers_update();
  }

  pub fn set_mouse_position (&mut self, x : u16, y : u16) {
    self.mouse_position = (x,y).into();
    // make sure mouse pointer stays within screen boundaries
    thread::clamp_mouse_to_screen (&self.resolution, &mut self.mouse_position);
  }

  pub fn main_menu_set_active_option (&mut self,
    option : mode::title_menu::MainMenuOption
  ) {
    self.main_menu_active_option = option;
    self.main_menu_vertex_buffers_update();
  }

  pub fn main_menu_vertex_buffers_update (&self) {
    let button_width  = self.main_menu_button_textures.width()  as u16;
    let button_height = self.main_menu_button_textures.height() as u16;
    let main_menu_button_vertices = mode::title_menu::main_menu_button_vertices (
      (button_width, button_height).into(),
      self.resolution,
      self.gui_scale,
      self.main_menu_active_option
    );
    // buttons
    self.main_menu_button_vertex_buffer.write (&main_menu_button_vertices);
    // cursor
    let cursor_width  = self.main_menu_cursor_texture.width()  as u16;
    let cursor_height = self.main_menu_cursor_texture.height() as u16;
    self.main_menu_cursor_vertex_buffer.write (
      &mode::title_menu::main_menu_cursor_vertex (
        (cursor_width, cursor_height).into(),
        self.resolution,
        self.gui_scale,
        self.main_menu_active_option,
        &main_menu_button_vertices
      )
    );
  }

  pub fn multi_setup_vertex_buffers_update (&self) {
    let connect_to_box_dimensions = cgmath::vec2 (
      self.connect_to_box_texture.width()  as u16,
      self.connect_to_box_texture.height() as u16);
    let connect_to_box_vertex = mode::multi_setup::connect_to_box_vertex (
      connect_to_box_dimensions, self.resolution, self.gui_scale);
    self.connect_to_box_vertex_buffer.write (&[connect_to_box_vertex]);
  }

  pub fn screenshot (&self) {
    let raw : glium::texture::RawImage2d <u8> =
      self.glium_display.read_front_buffer().unwrap();
    let mut image_buffer = image::ImageBuffer::from_raw (
      raw.width, raw.height, raw.data.into_owned()).unwrap();
    image_buffer = image::imageops::flip_vertical (&image_buffer);
    let image    = image::DynamicImage::ImageRgba8 (image_buffer);
    // create a file incrementally named 'screenshot-N.png'
    let filepath = rs_utils::file::file_path_incremental_with_extension (
      std::path::Path::new ("screenshot.png")
    ).unwrap();
    image.save (filepath).unwrap();
  }

  /// Causes the current player vertex data to be packed and uploaded to player
  /// vertex buffer.
  ///
  /// Also set on-screen rendering graphic for the local player if `view_player`
  /// is 'Some'.
  pub fn update_players (&mut self) {
    // update player vertex buffer
    self.player_vertex_data_packed.clear();
    for (player_id, player_vertex) in self.player_vertex_data.iter() {
      if let Some ((ref id, _, _, _)) = self.view_player {
        if player_id == usize::from (id.clone()) {
          // NB: comment this 'continue' out to draw the player debug capsule
          continue
        }
      }
      self.player_vertex_data_packed.push (*player_vertex);
    }
    self.player_vertex_buffer = glium::VertexBuffer::dynamic (
      &self.glium_display,
      &self.player_vertex_data_packed.as_slice()
    ).unwrap();

    // local player
    if let Some ((player_id, attack_kind, _, hurt_fade)) =
      self.view_player.as_mut()
    {
      use num_traits::FromPrimitive;
      if *hurt_fade > 0.0 {
        *hurt_fade -= 0.02;
        *hurt_fade = f32::max (*hurt_fade, 0.0);
      }
      let in_color = {
        let mut blood_red = color::rgba_u8_to_rgba_f32 (&color::BLOOD_RED);
        blood_red[3] = *hurt_fade;
        blood_red
      };
      self.screen_quad_vertex_buffer.write (&[
        shader::Vert2dColor {
          in_position: [-1.0,  1.0],
          in_color
        },
        shader::Vert2dColor {
          in_position: [-1.0, -1.0],
          in_color
        },
        shader::Vert2dColor {
          in_position: [ 1.0,  1.0],
          in_color
        },
        shader::Vert2dColor {
          in_position: [ 1.0, -1.0],
          in_color
        }
      ]);
      if let Some (player_vertex) = self.player_vertex_data
        .get (player_id.0 as usize)
      {
        // draw onscreen graphics
        // NOTE: the player sprite attack animation is six frames, but the
        // on-screen melee graphics animation is 5 frames
        let /*mut*/ in_texture_index = 0; // sword TODO: more weapons
        let frame = PlayerSpriteFrame::from_u32 (player_vertex.in_frame).unwrap();
        // if attacking, choose row based on attack kind
        let in_row = match frame {
          PlayerSpriteFrame::Melee1 |
          PlayerSpriteFrame::Melee2 |
          PlayerSpriteFrame::Melee3 |
          PlayerSpriteFrame::Melee4 |
          PlayerSpriteFrame::Melee5 => match attack_kind {
            player::AttackKind::Melee (melee_kind) => match melee_kind {
              // this is the layout of frames in the melee weapon sheets
              player::MeleeKind::Chop           => 1,
              player::MeleeKind::SlashLowLeft   => 2,
              player::MeleeKind::SlashHighLeft  => 3,
              player::MeleeKind::SlashHighRight => 4,
              player::MeleeKind::SlashLowRight  => 5,
              player::MeleeKind::Stab           => 6,
            }
            _ => unimplemented!("TODO: missile and spell attacks")
          },
          // NOTE: Melee6 frame is mapped back to 'not attacking'
          _ => 0  // default frame: not attacking
        };
        let in_column = match frame {
          PlayerSpriteFrame::Melee1 => 0,
          PlayerSpriteFrame::Melee2 => 1,
          PlayerSpriteFrame::Melee3 => 2,
          PlayerSpriteFrame::Melee4 => 3,
          PlayerSpriteFrame::Melee5 => 4,
          // return to 'not attacking' on the last frame
          //PlayerSpriteFrame::Melee6 => 4,
          _ => 0
        };
        self.weapon_graphic_vertex_buffer.write (&[
          shader::Vert2dSpriteArrayFrames {
            in_position:      [0.0, 0.0],
            in_dimensions:    [2.0, 2.0],  // -1.0 to 1.0 in both dimensions
            in_texture_index,   // weapon
            in_row,             // cycle
            in_column           // frame
          }
        ]);
      } else {
        log::warn!("Render view player vertex not present");
      }
    }

  } // end update players


  /// Recomputes the view transformation matrix from view position and
  /// orientation, updating them from player vertex data if the
  /// `view_player_id` is `Some` and the player is alive
  #[inline]
  pub fn update_view_transform_matrix (&mut self) {
    if let Some ((ref player_id, _, _, _)) = self.view_player {
      if let Some (player_data) =
        self.player_vertex_data.get (player_id.0 as usize)
      {
        use num_traits::FromPrimitive;
        use cgmath::{Rotation};
        let class = player::Class::from_u32 (player_data.in_texture_index)
          .unwrap();
        self.view_position    =
          player::eye_f32 (player_data.in_position.into(), class);
        // NB: we invert the `look_at` basis formed by the given orientation
        // to construct the world-to-view matrix
        self.view_orientation = cgmath::Basis3::<f32>::look_at (
          player_data.in_orientation[2].into(),
          player_data.in_orientation[1].into()).invert();
        self.transform_mat_view =
          gl_utils::camera3d::transform_mat_world_to_view (
            &self.view_position, &self.view_orientation);
      }
    }
  }

  /// Update the corpse vertex buffer.
  pub fn update_corpses (&mut self) {
    self.corpse_vertex_buffer = glium::VertexBuffer::dynamic (
      &self.glium_display,
      &self.corpse_vertex_data.as_slice()
    ).unwrap();
  }

  pub fn update_effects (&mut self) {
    debug_assert_eq!(
      self.effect_vertex_data.len(),
      self.effect_animation_state.len());
    let frame_secs = self.frame_duration.as_secs_f64();
    let mut i = 0;
    while i < self.effect_vertex_data.len() {
      let destroy = {
        let anim_state = &mut self.effect_animation_state[i];
        anim_state.cycle += frame_secs / anim_state.cycle_seconds;
        if anim_state.cycle > 1.0 {
          if anim_state.cycle_loop {
            anim_state.cycle = anim_state.cycle.fract();
            false
          } else {
            true
          }
        } else {
          false
        }
      };
      if destroy {
        self.effect_vertex_data.remove (i);
        self.effect_animation_state.remove (i);
      } else {
        let cycle = self.effect_animation_state[i].cycle;
        let frame = (cycle * EFFECT_FRAMES as f64) as u32;
        self.effect_vertex_data[i].in_frame = frame;
        i += 1;
      }
    }
    self.effect_vertex_buffer = glium::VertexBuffer::dynamic (
      &self.glium_display,
      &self.effect_vertex_data.as_slice()
    ).unwrap();

    debug_assert_eq!(
      self.effect_vertex_data.len(),
      self.effect_animation_state.len());
  }

} // end impl Render
impl std::fmt::Debug for Render {
  fn fmt (&self, f : &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "Render {{ ... }}")
  }
}

impl Tileset {
  fn load (facade : &glium::Display, bytes : &[u8]) -> Tileset {
    let image = image::load_from_memory_with_format (
      bytes, image::ImageFormat::Png
    ).unwrap().to_rgba();
    let dimensions      = image.dimensions();
    let tile_dimensions = cgmath::vec2 (
      (dimensions.0/16) as u8,
      (dimensions.1/16) as u8);

    log::debug!("tileset dimensions: {:?}", dimensions);
    log::debug!("tile dimensions:    {:?}", tile_dimensions);

    let raw     = glium::texture::RawImage2d::from_raw_rgba_reversed (
      image.into_raw().as_slice(),
      dimensions);
    let texture = glium::texture::Texture2d::new (facade, raw).unwrap();

    Tileset {
      dimensions: cgmath::vec2 (dimensions.0 as u16, dimensions.1 as u16),
      tile_dimensions,
      texture
    }
  } // end fn load
}

impl TilesetId {
  fn bytes (&self) -> &[u8] {
    match *self {
      TilesetId::Default => &TILESET_DOS_FALL_6X9_PNG_BYTES[..]
    }
  }
}

impl EffectAnimationState {
  pub fn from_id (effect_id : EffectId) -> Self {
    let (cycle_seconds, cycle_loop) = match effect_id {
      EffectId::Blood => (0.5, false)
    };
    EffectAnimationState {
      cycle: 0.0,
      cycle_seconds, cycle_loop
    }
  }
}
