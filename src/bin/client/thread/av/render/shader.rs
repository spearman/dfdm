// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

//! Vertex specification and shader programs.
//!
//! **Shader naming scheme**
//!
//! Since we are targeting OpenGL 3.3 core profile, there are no tessellation
//! shader stages.
//!
//! ```text
//! <shader-name>             := <vertex-processing>|<fragment-shader>
//! <vertex-processing>       := (Vert|Geom)<space>[<space>]<attributes-and-uniforms>[<instanced>]
//! <fragment-shader>         := Frag{<sampler>}<attributes-and-uniforms>
//! <attributes-and-uniforms> := {<attribute>}[Flat{<attribute>}][Uni{<uniform>}]
//! <space>                   := (Model|World|View|Clip)Space(2D|3D)
//! <instanced>               := Instanced<uniforms-and-attributes>
//! <attribute>               := Color | Orientation | Size | ... MORE
//! <uniform>                 := Color | ... MORE
//! <sampler>                 := Tex2d | TexArray2d | ... MORE
//! ```
//!
//! Where [ ] indicates an optional item that appears zero or once and { }
//! indicates an optional item that may be repeated zero or more times.
//!
//! The class of attributes and uniforms are open-ended-- they are usually
//! things like `Color` or `Scale` or `Orientation`, etc.
//!
//! A `<vertex-processing>` shader is always assumed to input and output a
//! vertex *position* and any further attributes should be given explicitly.
//! Obviously this can lead to some overly long names, so groups of attributes
//! or uniforms may be indicated by special names.
//!
//! Attributes input to the fragment shader are implicitly varying unless
//! otherwise specified as `Flat` attributes and needs to match with the
//! previous stage output. Note that texture samplers are technically uniforms,
//! but they are listed separately from the uniforms in a fragment shader name.
//!
//! By convention the camel case names here will be rendered in snake case in
//! GLSL file names, followed by the `.glsl` file extension.
//!
//! In the case of a vertex or geometry shader that outputs 4D homogenous clip
//! coordinates (the final result of vertex processing), no output space is
//! specified, so a shader with a name such as `VertWorldSpace2d` is a shader
//! that accepts 2D world space points as input and applies a world-to-view
//! transform to get view space coordinates followed by an orthographic
//! projection to give clip space coordinates.

use glium;

use gl_utils;

////////////////////////////////////////////////////////////////////////////////
//  vertex specification                                                      //
////////////////////////////////////////////////////////////////////////////////

//
//  struct Vert2d
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert2d {
  pub in_position : [f32; 2]
}
glium::implement_vertex!(Vert2d, in_position);

//
//  struct Vert2dColor
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert2dColor {
  pub in_position : [f32; 2],
  pub in_color    : [f32; 4]
}
glium::implement_vertex!(Vert2dColor, in_position, in_color);

//
//  struct Vert2dSprite
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert2dSprite {
  pub in_position   : [f32; 2],
  pub in_dimensions : [f32; 2]
}
glium::implement_vertex! { Vert2dSprite, in_position, in_dimensions }

//
//  struct Vert2dSpriteArray
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert2dSpriteArray {
  pub in_position      : [f32; 2],
  pub in_dimensions    : [f32; 2],
  pub in_texture_index : u32
}
glium::implement_vertex! {
  Vert2dSpriteArray, in_position, in_dimensions, in_texture_index
}

//
//  struct Vert2dSpriteArrayColorize
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert2dSpriteArrayColorize {
  pub in_position       : [f32; 2],
  pub in_dimensions     : [f32; 2],
  pub in_texture_index  : u32,
  pub in_colorize_flag  : u32,
  pub in_colorize_color : [f32; 3]
}
glium::implement_vertex! {
  Vert2dSpriteArrayColorize,
    in_position,
    in_dimensions,
    in_texture_index,
    in_colorize_flag,
    in_colorize_color
}

//
//  struct Vert2dSpriteArrayFrames
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert2dSpriteArrayFrames {
  pub in_position      : [f32; 2],
  pub in_dimensions    : [f32; 2],
  pub in_texture_index : u32,
  pub in_row           : u32,
  pub in_column        : u32
}
glium::implement_vertex! {
  Vert2dSpriteArrayFrames,
    in_position,
    in_dimensions,
    in_texture_index,
    in_row,
    in_column
}

//
//  struct Vert2dTile
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert2dTile {
  pub in_row    : i32,
  pub in_column : i32,
  pub in_tile   : u8
}
glium::implement_vertex!(Vert2dTile, in_row, in_column, in_tile);

//
//  struct Vert3d
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert3d {
  pub in_position : [f32; 3]
}
glium::implement_vertex!(Vert3d, in_position);

//
//  struct Vert3dInstanced
//
/// To be used per-vertex in instanced rendering (positions in model space).
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert3dInstanced {
  pub inst_position : [f32; 3]
}
glium::implement_vertex!(Vert3dInstanced, inst_position);

//
//  struct Vert3dColor
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert3dColor {
  pub in_position : [f32; 3],
  pub in_color    : [f32; 4]
}
glium::implement_vertex!(Vert3dColor, in_position, in_color);

//
//  struct Vert3dColorSize
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert3dColorSize {
  pub in_position : [f32; 3],
  pub in_color    : [f32; 4],
  pub in_size     : f32
}
glium::implement_vertex!(Vert3dColorSize, in_position, in_color, in_size);

//
//  struct Vert3dScale
//
#[derive(Clone,Copy,Debug)]
pub struct Vert3dScale {
  pub in_position : [f32; 3],
  pub in_scale    : [f32; 3]
}
glium::implement_vertex!(Vert3dScale, in_position, in_scale);

//
//  struct Vert3dSpriteArray
//
#[derive(Clone,Copy,Debug)]
pub struct Vert3dSpriteArray {
  pub in_position      : [f32; 3],
  pub in_texture_index : u32
}
glium::implement_vertex!(Vert3dSpriteArray, in_position, in_texture_index);

//
//  struct Vert3dSpriteArrayFrames
//
#[derive(Clone,Copy,Debug)]
pub struct Vert3dSpriteArrayFrames {
  pub in_position      : [f32; 3],
  pub in_texture_index : u32,
  pub in_frame         : u32
}
glium::implement_vertex!(Vert3dSpriteArrayFrames,
  in_position, in_texture_index, in_frame);

//
//  struct Vert3dOrientation
//
#[derive(Clone,Copy,Debug)]
pub struct Vert3dOrientation {
  pub in_position     : [f32; 3],
  pub in_orientation  : [[f32; 3]; 3]
}
glium::implement_vertex!(Vert3dOrientation, in_position, in_orientation);

//
//  struct Vert3dOrientationScaleSpriteArray
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert3dOrientationScaleSpriteArray {
  pub in_position      : [f32; 3],
  pub in_orientation   : [[f32; 3]; 3],
  pub in_scale         : [f32; 3],
  pub in_texture_index : u32
}
glium::implement_vertex! {
  Vert3dOrientationScaleSpriteArray,
    in_position, in_orientation, in_scale, in_texture_index
}

//
//  struct Vert3dOrientationScaleSpriteArrayFrames
//
#[derive(Clone,Copy,Debug,Default)]
pub struct Vert3dOrientationScaleSpriteArrayFrames {
  pub in_position      : [f32; 3],
  pub in_orientation   : [[f32; 3]; 3],
  pub in_scale         : [f32; 3],
  pub in_texture_index : u32,
  pub in_frame         : u32
}
glium::implement_vertex! {
  Vert3dOrientationScaleSpriteArrayFrames,
    in_position, in_orientation, in_scale, in_texture_index, in_frame
}

////////////////////////////////////////////////////////////////////////////////
//  shaders and shader programs                                               //
////////////////////////////////////////////////////////////////////////////////

gl_utils::def_programs! {
  SHADERS [
    VertPassthru2d,
      "data/shaders/vert_passthru_2d.glsl",
    VertPassthru2dColor,
      "data/shaders/vert_passthru_2d_color.glsl",
    VertPassthru2dTile,
      "data/shaders/vert_passthru_2d_tile.glsl",
    VertPassthru2dSprite,
      "data/shaders/vert_passthru_2d_sprite.glsl",
    VertPassthru2dSpriteArray,
      "data/shaders/vert_passthru_2d_sprite_array.glsl",
    VertPassthru2dSpriteArrayColorize,
      "data/shaders/vert_passthru_2d_sprite_array_colorize.glsl",
    VertPassthru2dSpriteArrayFrames,
      "data/shaders/vert_passthru_2d_sprite_array_frames.glsl",
    VertPassthru3d,
      "data/shaders/vert_passthru_3d.glsl",
    VertPassthru3dColorSize,
      "data/shaders/vert_passthru_3d_color_size.glsl",
    VertPassthru3dPointSpriteArrayFrames,
      "data/shaders/vert_passthru_3d_point_sprite_array_frames.glsl",
    VertPassthru3dOrientation,
      "data/shaders/vert_passthru_3d_orientation.glsl",
    VertPassthru3dOrientationSpriteArray,
      "data/shaders/vert_passthru_3d_orientation_sprite_array.glsl",
    VertPassthru3dOrientationSpriteArrayFrames,
      "data/shaders/vert_passthru_3d_orientation_sprite_array_frames.glsl",
    VertWorldSpace3dClipSpace3dColor,
      "data/shaders/vert_world_space_3d_clip_space_3d_color.glsl",
    VertModelSpace3dClipSpace3dInstancedScale,
      "data/shaders/vert_model_space_3d_clip_space_3d_instanced_scale.glsl",
    VertModelSpace3dClipSpace3dInstancedCapsule,
      "data/shaders/vert_model_space_3d_clip_space_3d_instanced_capsule.glsl",
    GeomClipSpace2dSprite,
      "data/shaders/geom_clip_space_2d_sprite.glsl",
    GeomClipSpace2dSpriteArray,
      "data/shaders/geom_clip_space_2d_sprite_array.glsl",
    GeomClipSpace2dSpriteArrayColorize,
      "data/shaders/geom_clip_space_2d_sprite_array_colorize.glsl",
    GeomClipSpace2dSpriteArrayFrames,
      "data/shaders/geom_clip_space_2d_sprite_array_frames.glsl",
    GeomScreenSpace2dTile,
      "data/shaders/geom_screen_space_2d_tile.glsl",
    GeomScreenSpace2dSprite,
      "data/shaders/geom_screen_space_2d_sprite.glsl",
    GeomWorldSpace3dBasis,
      "data/shaders/geom_world_space_3d_basis.glsl",
    GeomWorldSpace3dOrientationBasis,
      "data/shaders/geom_world_space_3d_orientation_basis.glsl",
    GeomWorldSpace3dOrientationSpriteArray,
      "data/shaders/geom_world_space_3d_orientation_sprite_array.glsl",
    GeomWorldSpace3dOrientationSpriteArrayFrames,
      "data/shaders/geom_world_space_3d_orientation_sprite_array_frames.glsl",
    GeomWorldSpace3dPointSprite,
      "data/shaders/geom_world_space_3d_point_sprite.glsl",
    GeomWorldSpace3dPointSpriteArrayFrames,
      "data/shaders/geom_world_space_3d_point_sprite_array_frames.glsl",
    FragSolidYellow,
      "data/shaders/frag_solid_yellow.glsl",
    FragColor,
      "data/shaders/frag_color.glsl",
    FragUniformColor,
      "data/shaders/frag_uniform_color.glsl",
    FragTexture2d,
      "data/shaders/frag_texture_2d.glsl",
    FragTexture2dArray,
      "data/shaders/frag_texture_2d_array.glsl",
    FragTexture2dArrayColorize,
      "data/shaders/frag_texture_2d_array_colorize.glsl"
  ]

  PROGRAMS [
    program ClipSpace2dSolidYellow {
      vertex_shader:   VertPassthru2d
      fragment_shader: FragSolidYellow
    }
    program ClipSpace2dColor {
      vertex_shader:   VertPassthru2dColor
      fragment_shader: FragColor
    }
    program ClipSpace2dSprite {
      vertex_shader:   VertPassthru2dSprite
      geometry_shader: GeomClipSpace2dSprite
      fragment_shader: FragTexture2d
    }
    program ClipSpace2dSpriteArray {
      vertex_shader:   VertPassthru2dSpriteArray
      geometry_shader: GeomClipSpace2dSpriteArray
      fragment_shader: FragTexture2dArray
    }
    program ClipSpace2dSpriteArrayColorize {
      vertex_shader:   VertPassthru2dSpriteArrayColorize
      geometry_shader: GeomClipSpace2dSpriteArrayColorize
      fragment_shader: FragTexture2dArrayColorize
    }
    program ClipSpace2dSpriteArrayFrames {
      vertex_shader:   VertPassthru2dSpriteArrayFrames
      geometry_shader: GeomClipSpace2dSpriteArrayFrames
      fragment_shader: FragTexture2dArray
    }
    program ScreenSpace2dTile {
      vertex_shader:   VertPassthru2dTile
      geometry_shader: GeomScreenSpace2dTile
      fragment_shader: FragTexture2d
    }
    program ScreenSpace2dSprite {
      vertex_shader:   VertPassthru2dSprite
      geometry_shader: GeomScreenSpace2dSprite
      fragment_shader: FragTexture2d
    }
    program WorldSpace3dColor {
      vertex_shader:   VertWorldSpace3dClipSpace3dColor
      fragment_shader: FragColor
    }
    program WorldSpace3dPointSprite {
      vertex_shader:   VertPassthru3d
      geometry_shader: GeomWorldSpace3dPointSprite
      fragment_shader: FragTexture2d
    }
    program WorldSpace3dPointSpriteArrayFrames {
      vertex_shader:   VertPassthru3dPointSpriteArrayFrames
      geometry_shader: GeomWorldSpace3dPointSpriteArrayFrames
      fragment_shader: FragTexture2dArray
    }
    program WorldSpace3dOrientationBasis {
      vertex_shader:   VertPassthru3dOrientation
      geometry_shader: GeomWorldSpace3dOrientationBasis
      fragment_shader: FragColor
    }
    program WorldSpace3dOrientationSpriteArray {
      vertex_shader:   VertPassthru3dOrientationSpriteArray
      geometry_shader: GeomWorldSpace3dOrientationSpriteArray
      fragment_shader: FragTexture2dArray
    }
    program WorldSpace3dOrientationSpriteArrayFrames {
      vertex_shader:   VertPassthru3dOrientationSpriteArrayFrames
      geometry_shader: GeomWorldSpace3dOrientationSpriteArrayFrames
      fragment_shader: FragTexture2dArray
    }
    program ModelSpace3dInstancedScaleUniformColor {
      vertex_shader:   VertModelSpace3dClipSpace3dInstancedScale
      fragment_shader: FragUniformColor
    }
    program ModelSpace3dInstancedCapsuleUniformColor {
      vertex_shader:   VertModelSpace3dClipSpace3dInstancedCapsule
      fragment_shader: FragUniformColor
    }
  ]
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

impl Default for Vert3dOrientation {
  fn default() -> Self {
    Vert3dOrientation {
      in_position: [0.0, 0.0, 0.0],
      in_orientation: [
        [1.0, 0.0, 0.0],
        [0.0, 1.0, 0.0],
        [0.0, 0.0, 1.0]
      ]
    }
  }
}
