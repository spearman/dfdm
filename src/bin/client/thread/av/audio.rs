// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use {std, rfmod};
use fmod_utils::rfmod_ok;

use df;

use crate::{thread};

pub struct Audio {
  pub rfmod_sys                : rfmod::Sys,
  pub dsp_echo                 : rfmod::Dsp,
  pub sound_ambient_fire       : rfmod::Sound,
  pub sound_sfx_gui_tick       : rfmod::Sound,
  pub sound_sfx_footstep_soft1 : rfmod::Sound,
  pub sound_sfx_footstep_soft2 : rfmod::Sound,
  pub sound_sfx_swing_medium   : rfmod::Sound,
  pub sound_sfx_hit_melee1     : rfmod::Sound,
  pub sound_sfx_hit_melee2     : rfmod::Sound,
  pub sound_sfx_hit_melee3     : rfmod::Sound,
  pub sound_sfx_corpse_fall    : rfmod::Sound,
  pub sound_music_title        : rfmod::Sound,
  pub sound_music_deathmatch   : rfmod::Sound
}

impl Audio {
  pub fn create (audio : df::Audio) -> Self {
    let av_config    = &thread::av::STATIC_CONFIG;
    // init rfmod sys
    let rfmod_sys    = rfmod::Sys::new().unwrap();
    let max_channels = 256;  // TODO: configure number of channels
    rfmod_ok!(rfmod_sys.init_with_parameters (
      max_channels, rfmod::InitFlag (rfmod::INIT_3D_RIGHTHANDED)));
    // setup listener
    let listener_position = rfmod::Vector { x: 0.0, y: 0.0, z: 0.0 };
    let listener_velocity = rfmod::Vector { x: 0.0, y: 0.0, z: 0.0 };
    let forward           = rfmod::Vector { x: 0.0, y: 0.0, z: -1.0 };
    let up                = rfmod::Vector { x: 0.0, y: 1.0, z: 0.0 };
    rfmod_ok!(rfmod_sys.set_3D_listener_attributes (
      0, &listener_position, &listener_velocity, &forward, &up));
    // echo dsp
    let dsp_echo = rfmod_sys.create_DSP_by_type (rfmod::DspType::Echo).unwrap();
    rfmod_ok!(dsp_echo.set_parameter (rfmod::DspTypeEcho::Delay as i32, 80.0));
    rfmod_ok!(dsp_echo.set_parameter (
      rfmod::DspTypeEcho::DecayRatio as i32, 0.2));
    // ambient
    let sound_ambient_fire = rfmod_sys.create_sound_openmemory (
      &audio.sound_fire,
      Some (rfmod::Mode (rfmod::_3D | rfmod::SOFTWARE)),
      None  // no soundex info
    ).unwrap();
    sound_ambient_fire.set_mode (rfmod::Mode (rfmod::LOOP_NORMAL));
    // sfx
    let sound_sfx_gui_tick = rfmod_sys.create_sound_openmemory (
      &audio.sound_gui_tick,
      Some (rfmod::Mode (rfmod::_3D | rfmod::SOFTWARE)),
      None  // no soundex info
    ).unwrap();
    let sound_sfx_footstep_soft1 = rfmod_sys.create_sound_openmemory (
      &audio.sound_footstep_soft1,
      Some (rfmod::Mode (rfmod::_3D | rfmod::SOFTWARE)),
      None  // no soundex info
    ).unwrap();
    let sound_sfx_footstep_soft2 = rfmod_sys.create_sound_openmemory (
      &audio.sound_footstep_soft2,
      Some (rfmod::Mode (rfmod::_3D | rfmod::SOFTWARE)),
      None  // no soundex info
    ).unwrap();
    let sound_sfx_swing_medium = rfmod_sys.create_sound_openmemory (
      &audio.sound_swing_med,
      Some (rfmod::Mode (rfmod::_3D | rfmod::SOFTWARE)),
      None  // no soundex info
    ).unwrap();
    let sound_sfx_hit_melee1 = rfmod_sys.create_sound_openmemory (
      &audio.sound_hit_melee1,
      Some (rfmod::Mode (rfmod::_3D | rfmod::SOFTWARE)),
      None  // no soundex info
    ).unwrap();
    let sound_sfx_hit_melee2 = rfmod_sys.create_sound_openmemory (
      &audio.sound_hit_melee2,
      Some (rfmod::Mode (rfmod::_3D | rfmod::SOFTWARE)),
      None  // no soundex info
    ).unwrap();
    let sound_sfx_hit_melee3 = rfmod_sys.create_sound_openmemory (
      &audio.sound_hit_melee3,
      Some (rfmod::Mode (rfmod::_3D | rfmod::SOFTWARE)),
      None  // no soundex info
    ).unwrap();
    let sound_sfx_corpse_fall = rfmod_sys.create_sound_openmemory (
      &audio.sound_corpse_fall,
      Some (rfmod::Mode (rfmod::_3D | rfmod::SOFTWARE)),
      None  // no soundex info
    ).unwrap();
    // load midi
    let mut create_soundex_info_midi  = rfmod::CreateSoundexInfo::default();
    // set a dls path (required for Linux, otherwise may be empty for
    // Windows or macOS to use default system GM .dls files)
    create_soundex_info_midi.dls_name = av_config.midi_dls_path.clone();
    let sound_music_title  = rfmod_sys.create_sound_openmemory (
      &audio.song_title_menu,
      Some (rfmod::Mode (rfmod::DEFAULT)),
      Some (&mut create_soundex_info_midi)
    ).unwrap();
    let sound_music_deathmatch = rfmod_sys.create_sound_openmemory (
      &audio.song_deathmatch,
      Some (rfmod::Mode (rfmod::DEFAULT)),
      Some (&mut create_soundex_info_midi)
    ).unwrap();
    rfmod_ok!(rfmod_sys.update());
    Audio {
      rfmod_sys,
      dsp_echo,
      sound_ambient_fire,
      sound_sfx_gui_tick,
      sound_sfx_footstep_soft1,
      sound_sfx_footstep_soft2,
      sound_sfx_swing_medium,
      sound_sfx_hit_melee1,
      sound_sfx_hit_melee2,
      sound_sfx_hit_melee3,
      sound_sfx_corpse_fall,
      sound_music_title,
      sound_music_deathmatch
    }
  }
}

impl std::fmt::Debug for Audio {
  fn fmt (&self, f : &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "Audio {{ ... }}")
  }
}
