// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

#![recursion_limit="128"]
#![warn(unused_extern_crates)]
#![feature(const_fn)]
#![feature(core_intrinsics)]
#![feature(integer_atomics)]
#![feature(vec_resize_default)]
#![feature(nll)]

use lazy_static::lazy_static;
use log;

use serde_yaml;
use simplelog;

use apis;
use geometry_utils as geometry;
use macro_machines;

use dfdm;

#[macro_use] mod macro_def;
mod config;
mod mode;
mod program;
mod thread;

use program::*;

lazy_static!{
  pub static ref STATIC_CONFIG : config::StaticConfig = {
    let config_file = std::fs::File::open (config::STATIC_CONFIG_YAML_PATH)
      .unwrap();
    serde_yaml::from_reader::<std::fs::File, config::StaticConfig> (config_file)
      .unwrap()
  };
  pub static ref CMDLINE_OPTIONS  : getopts::Options = {
    let mut opts = getopts::Options::new();
    opts.optopt ("l", "log_level", "Log level filter", "LOG_LEVEL");
    opts.optopt ("p", "arena2_path", "ARENA2 path", "ARENA2_PATH");
    opts
  };
  pub static ref CMDLINE_OPTION_MATCHES : getopts::Matches = {
    let args : Vec <String> = std::env::args().collect();
    CMDLINE_OPTIONS.parse (&args[1..]).unwrap()
  };

  //  Off, Error, Warn, Info, Debug, Trace
  pub static ref LOG_LEVEL_FILTER : simplelog::LevelFilter = {
    use std::str::FromStr;
    let log_level = CMDLINE_OPTION_MATCHES.opt_str ("l")
      .unwrap_or (STATIC_CONFIG.log_level.clone());
    simplelog::LevelFilter::from_str (log_level.as_str()).unwrap()
  };

  pub static ref ARENA2_PATH : std::path::PathBuf = {
    let path_string = CMDLINE_OPTION_MATCHES.opt_str ("p")
      .unwrap_or (STATIC_CONFIG.arena2_path.to_str().unwrap().to_string());
    let mut path = std::path::PathBuf::from (path_string.clone());
    path.canonicalize().or_else (|e| {
      if path.ends_with ("ARENA2") {
        path.set_file_name ("arena2");
        path.canonicalize()
      } else if path.ends_with ("arena2") {
        path.set_file_name ("ARENA2");
        path.canonicalize()
      } else {
        Err(e)
      }
    }).unwrap_or_else (|e| {
      log::error!("{}", format!("ARENA2_PATH={:?}: {}", path_string, e));
      panic!("dfdm client: fatal error")
    })
  };
}

pub fn report_sizes() {
  println!("client report sizes...");
  Client::report_sizes();
  thread::main::report_sizes();
  thread::av::report_sizes();
  thread::net::report_sizes();
  dfdm::simulation::report_sizes();
  println!("...client report sizes");
}

///////////////////////////////////////////////////////////////////////////////
//  main                                                                     //
///////////////////////////////////////////////////////////////////////////////

fn main() {
  use std::{fs::File, io::Write};
  use colored::Colorize;
  use macro_machines::MachineDotfile;
  use apis::{Program, session::Context};

  let current_exe = rs_utils::app::exe_name();
  println!("{}", format!("{:?} main...", current_exe).green().bold());

  rs_utils::app::init_simple_termlogger (*LOG_LEVEL_FILTER);

  // uncomment this to generate dotfile output
  if std::env::var ("DFDM_DOTFILES").is_ok() {
    // create a dotfile for the program state machine
    let mut f = File::create ("client.dot").unwrap();
    f.write_all (Client::dotfile().as_bytes()).unwrap();
    drop (f);
    // create session dotfiles for the program modes
    // splash
    let mut f = File::create ("splash.dot").unwrap();
    f.write_all (mode::Splash::def().unwrap().dotfile().as_bytes()).unwrap();
    drop (f);
    // title menu
    let mut f = File::create ("title_menu.dot").unwrap();
    f.write_all (mode::TitleMenu::def().unwrap().dotfile().as_bytes()).unwrap();
    drop (f);
    // multi setup
    let mut f = File::create ("multi_setup.dot").unwrap();
    f.write_all (mode::MultiSetup::def().unwrap().dotfile().as_bytes()).unwrap();
    drop (f);
    // multi run
    let mut f = File::create ("multi_run.dot").unwrap();
    f.write_all (mode::MultiRun::def().unwrap().dotfile().as_bytes()).unwrap();
    drop (f);
    // practice
    let mut f = File::create ("practice.dot").unwrap();
    f.write_all (mode::Practice::def().unwrap().dotfile().as_bytes()).unwrap();
    drop (f);
  }

  // show some size information about the program
  report_sizes();
  dfdm::simulation::report_sizes();
  // debug: show the static configuration
  log::debug!("client::STATIC_CONFIG: {}",
    serde_yaml::to_string (&*STATIC_CONFIG).unwrap());

  // create a program in the initial mode
  let mut client_program = Client::initial();
  //log::debug!("client_program: {:#?}", client_program);
  // run to completion
  client_program.run();

  println!("{}", format!("...{:?} main", current_exe).green().bold());
}
