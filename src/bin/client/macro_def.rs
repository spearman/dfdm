// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

//
//  impl_input_fun!
//
/// Defines a serializable enum `InputFun` that dereferences to a pointer to a
/// function that takes a mutable process context and returns an
/// `InputFunResult`.
macro_rules! impl_input_fun {
  ( $process : path {
      $($function_path : path, $variant : ident)*
    }
  ) => (

    /// Serializable function names that may be mapped to keycodes.
    #[derive(Copy,Clone,Debug,Serialize,Deserialize)]
    pub enum InputFun {
      $($variant),*
    }

    #[derive(Default)]
    pub struct InputFunResult {
      pub control_flow      : Option <apis::process::ControlFlow>,
      pub input_fun_keyup   : Option <InputFun>
      // TODO:
      //pub input_map : Option <vec_map::VecMap <InputFun>>,
      // TODO: do we really want this?
      /*
      /// Continuation.
      pub input_fun : Option <InputFun>
      */
    }

    impl std::ops::Deref for InputFun {
      type Target = fn (&mut $process) -> InputFunResult;
      fn deref (&self) -> &fn (&mut $process) -> InputFunResult {
        match *self {
        $(
          InputFun::$variant => {
            static F : fn (&mut $process) -> InputFunResult = $function_path;
            &F
          }
        )*
        }
      }
    }
  );
}
