// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

#![recursion_limit="128"]
#![warn(unused_extern_crates)]
#![feature(const_fn)]
#![feature(core_intrinsics)]
#![feature(pattern)]
#![feature(vec_remove_item)]
#![feature(nll)]
#![feature(integer_atomics)]

pub mod client;
pub mod server;
pub mod simulation;

pub use self::simulation::Simulation;

pub const VERSION : [u16; 3] = [0, 0, 4];
