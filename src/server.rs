// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

use crate::{simulation, Simulation};

#[derive(Serialize, Deserialize, Copy, Clone, Debug, Eq, PartialEq)]
pub enum ClientId {
  Player    (simulation::world::player::Id),
  /// Should be < SPECTATORS_MAX
  Spectator (u16)
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum Message {
  ConnectionEstablished {
    ping_rtt_window_size : u16,
    version              : [u16; 3]
  },
  ConnectionRefused     (ConnectionRefused),
  Pong {
    /// Sequence number of the original ping
    sequence : u64,
    /// Server step when ping was received
    step     : u64
  },
  ConnectionAccepted (ClientId, Simulation),
  Kicked      (Kicked),
  /// (step,command)
  PeerCommand (u64, simulation::Command),
  /// (step,command)
  Rejected    (u64, simulation::Command)
}

#[derive(Serialize, Deserialize, Clone, Debug, Eq, PartialEq)]
pub enum ConnectionRefused {
  ServerFull,
  /// Pair of (ping, max ping) milliseconds
  PingTooHigh (u16, u16)
}

#[derive(Serialize, Deserialize, Clone, Debug, Eq, PartialEq)]
pub enum Kicked {
  /// Pair of (ping, max ping) milliseconds
  PingTooHigh (u16, u16)
}

impl ClientId {
  #[inline]
  pub fn player_id (&self) -> Option <simulation::world::player::Id> {
    match self {
      ClientId::Player    (id) => Some (*id),
      ClientId::Spectator (_)  => None
    }
  }
  #[inline]
  pub fn spectator_id (&self) -> Option <u16> {
    match self {
      ClientId::Player    (_)  => None,
      ClientId::Spectator (id) => Some (*id)
    }
  }
}
