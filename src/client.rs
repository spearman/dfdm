// Copyright 2018, 2019 Shane Pearman
//
// This file is part of DFDM.
//
// DFDM is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DFDM is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DFDM.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

use crate::simulation;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum Message {
  /// Ping with timestamp
  Ping {
    /// Sequence number
    sequence            : u64,
    /// Client step when the ping was sent
    step                : u64,
    /// Client timestamp
    ping_timestamp      : u64,
    /// Client timestamp at the time of the last pong received
    last_pong_timestamp : u64
  },
  /// Schedule event on the given step
  ScheduleCommand (u64, simulation::Command)
}
